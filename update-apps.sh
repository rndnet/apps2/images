OPENCL_APPS=$(pwd)/opencl/  # folder whith opencl projects
RAYTRACER=$(pwd)/ray-tracer/Dockerfiles  # folder whith raytracer project

USE_SCRATCH=no   #Use scratch if posible 


MULTI=( $(pwd)/multi/ $OPENCL_APPS)  #folder whith multi projects
ONE=( $RAYTRACER )

echo "--------------------------"
echo "Update multi projects: ${MULTI[*]}"
echo "--------------------------"
echo 

for i in "${MULTI[@]}"
do
  cd $i
  echo $i 
  for D in `find . -mindepth 1 -maxdepth 1 -type d`
  do   
     cd $i/$D
     pwd
     
     if [ $USE_SCRATCH = yes ]; then     
        if [ -f  build-upload-from-scratch.sh ]; then
           echo Scratch mode on
           bash build-upload-from-scratch.sh
        else
           echo Scratch mode not exist. Scratch mode off
           bash build-upload.sh
        fi
     else
           echo Scratch mode off
           bash build-upload.sh
     fi
     echo
  done

done

echo 
echo "--------------------------"
echo "Update one projects: ${ONE[*]}"
echo "--------------------------"
echo 

for i in "${ONE[@]}"
do
     cd $i     
     pwd

     if [ $USE_SCRATCH = yes ]; then     
        if [ -f  build-upload-from-scratch.sh ]; then
           echo Scratch mode on
           bash build-upload-from-scratch.sh
        else
           echo Scratch mode not exist. Scratch mode off
           bash build-upload.sh
        fi
     else
           echo Scratch mode off
           bash build-upload.sh
     fi

     echo
done
