#!/bin/bash
. common

echo
echo "Build ....."
#podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}  --no-cache -f Dockerfile  #use git clone git: ... for microsesm library
podman build -t ${REP}:${TAG}  --no-cache -f Dockerfile  #use git clone git: ... for microsesm library

echo
echo Upload images
podman login docker.io
#podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}

