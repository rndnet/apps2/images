#!/bin/bash
. common

echo
echo "Build ....."
#podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}  --no-cache -f Dockerfile  
podman build  -t ${REP}:${TAG}  -t ${REP_LOCAL}:${TAG}  --no-cache --format docker -f Dockerfile

echo
echo Upload images

echo push ${REP_LOCAL}
podman push ${REP_LOCAL}:${TAG}

echo push ${REP}
#podman login docker.io
#podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}
#podman logout docker.io

