#!/bin/bash
. common
#set -eou

mkdir -p files

if [ ! -f files/${AMD_DRV} ]; then
    echo
    echo "Download AMD drivers: ${AMD_DRV}"
    referer="https://www.amd.com/en/support/kb/release-notes/rn-rad-lin-20-20-unified"
    download="https://drivers.amd.com/drivers/linux/${AMD_DRV}"
    wget ${download} --referer ${referer}  -O build/files/${AMD_DRV}
fi 

#cd build
#echo
#echo "Remove FROM command from origin Dockerfile"
#sed '/^FROM /d' ../Dockerfile > Dockerfile_origin
#echo
#echo "Get base image Dockerfile"
#wget https://raw.githubusercontent.com/rndnet/opencl-bfj/master/Dockerfile -O $DF   
#echo
#echo "Create new Dockerfile: append two Dockerfiles"
#cat Dockerfile_origin  >> $DF
#cat $DF

echo
echo "Build ....."
podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG} -v ${HOME}/.ssh:/root/.ssh --format docker --no-cache -f Dockerfile  #use git clone git: ... for microsesm library
#podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}                            --no-cache -f Dockerfile  #use git clone https: ... for microsesm library

#rm -rv $DF Dockerfile_origin

echo
echo Upload images
podman login docker.io
podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}

