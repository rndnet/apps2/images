#!/bin/bash
. common

mkdir -p files

if [ ! -f ./files/${AMD_DRV} ]; then
    echo
    echo "Download AMD drivers: ${AMD_DRV}"
    referer="https://www.amd.com/en/support/kb/release-notes/rn-rad-lin-20-20-unified"
    download="https://drivers.amd.com/drivers/linux/${AMD_DRV}"
    wget ${download} --referer ${referer}  -O files/${AMD_DRV}
fi

echo
echo "Build ....."
#podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}  --no-cache -f Dockerfile  
#podman build  -t ${REP}:${TAG}  -t ${REP_LOCAL}:${TAG}  --no-cache --format docker -f Dockerfile
#podman build  -t ${REP}:${TAG}   --no-cache --format docker -f Dockerfile
podman build  -t ${REP}:${TAG}  --format docker --no-cache -f Dockerfile

echo
echo Upload images

#echo push ${REP_LOCAL}
#podman push ${REP_LOCAL}:${TAG}

echo push ${REP}
#podman login docker.io
#podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}
#podman logout docker.io

