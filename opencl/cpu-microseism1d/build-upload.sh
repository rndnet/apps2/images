#!/bin/bash
. common

set -eou

if [ $# -eq 0 ]; then
  CM=podman
else
  CM=$1
fi

echo "Use $CM as container manager"

echo
echo "Build ....."
$CM build -t ${REP}:${TAG}   -v ${HOME}/.ssh:/root/.ssh --no-cache -f Dockerfile  --format $format
#$CM build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}   -v ${HOME}/.ssh:/root/.ssh --no-cache -f Dockerfile  --format $format

echo
echo Upload images
$CM login docker.io
#$CM push ${REP}:${TAG}-${VER} 
$CM push ${REP}:${TAG}

