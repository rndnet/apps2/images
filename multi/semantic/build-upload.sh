#!/bin/bash
. common
#VER=`date +"%Y-%m-%d_%H-%M-%S"`
TAG=latest
REP=$REG_LOCAL/apps/semantic
#VER=`date +"%Y-%m-%d"`

echo
echo "Build ....."
#podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}  --no-cache -f Dockerfile  
podman build  -t ${REP}:${TAG}  --no-cache -f Dockerfile  

echo
echo Upload images
#podman login docker.io
#podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}

