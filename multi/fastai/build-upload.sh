#!/bin/bash
. common
##VER=`date +"%Y-%m-%d_%H-%M-%S"`
TAG=latest
REP=$REG/rndnet/fastai-job.py
REP_LOCAL=${REG_LOCAL}/rndnet/fastai-job.py
##VER=`date +"%Y-%m-%d"`

echo
echo "Build ....."
##podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}  --no-cache -f Dockerfile  
#podman build  -t ${REP}:${TAG}  -t ${REP_LOCAL}:${TAG}  --no-cache --format docker -f Dockerfile

#podman build  -t ${REP}:${TAG}  -t ${REP_LOCAL}:${TAG}  --no-cache --format docker -f Dockerfile
podman build  -t ${REP}:${TAG}  -t ${REP_LOCAL}:${TAG}   -f Dockerfile

#podman build  -t ${REP}:${TAG}  -f Dockerfile
#podman build  -t ${REP}:${TAG}  --format docker -f Dockerfile

echo
echo Upload images

echo push ${REP_LOCAL}
podman push ${REP_LOCAL}:${TAG}

echo push ${REP}
##podman login docker.io
##podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}
##podman logout docker.io

