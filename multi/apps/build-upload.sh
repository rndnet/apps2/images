#!/bin/bash
. common
#VER=`date +"%Y-%m-%d_%H-%M-%S"`
TAG=latest
REP=$REG/rndnet/job-apps.py
REP_LOCAL=${REG_LOCAL}/rndnet/job-apps.py
#VER=`date +"%Y-%m-%d"`

echo
echo "Build ....."
#podman build -t ${REP}:${TAG}-${VER} -t ${REP}:${TAG}  --no-cache -f Dockerfile  
podman build  -t ${REP}:${TAG}  -t ${REP_LOCAL}:${TAG}  --no-cache --format docker -f Dockerfile

echo
echo Upload images

echo push ${REP_LOCAL}
podman push ${REP_LOCAL}:${TAG}

echo push ${REP}
#podman login docker.io
#podman push ${REP}:${TAG}-${VER} 
podman push ${REP}:${TAG}
#podman logout docker.io

