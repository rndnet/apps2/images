#include <iostream>
//#include "pybind11/include/pybind11/pybind11.h"
#include "pybind11/pybind11.h"
#include "pybind11/numpy.h"
#include <vector>
#include <exception>
#include <stdexcept>
#include <functional>

namespace py = pybind11;
template<class T>
void printType(T a){
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

template<class T, class S>
void print(S s, std::vector<T> a){
    std::cout << s << " [";
    for (T t : a){
        std::cout << t << " ";
    }
    std::cout << "]" << std::endl;
}

template<class T, class F>
std::vector<T> zip(F f, std::vector<T> a, std::vector<T> b){
    std::vector<T> o;
    for (int i = 0; i < a.size(); ++i){
        o.push_back(f(a[i], b[i]));
    }
    return o;
}

template<class T>
std::vector<T> vectorIndexing(const std::vector<T>& a, const std::vector<int>& indexes){
    std::vector<T> o;
    for (int i : indexes)
        o.push_back(a[i]);
    return o;
}

template<template<class, long> class T, class V>
V tupleIndexing(const T<V, 3>& a, const std::vector<int>& indexes){
    return a(indexes[0], indexes[1], indexes[2]);
}

template<class T>
T dot(const std::vector<T>& a, const std::vector<T>& b){
     return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

template<class T>
std::vector<T> scalarProduct(T c, const std::vector<T>& a){
    //std::transform(a.begin(), a.end(), [c](T a){return a+c;});
    return {c*a[0], c*a[1], c*a[2]};
}

template<class T>
T norm(const std::vector<T>& v){
    return sqrt(std::pow(v[0],2) + std::pow(v[1],2) + std::pow(v[2],2));
}

template<class T>
T normalize(std::vector<T>& v){
    double normCoeff = 1/norm(v);
    v[0] /= normCoeff;
    v[1] /= normCoeff;
    v[2] /= normCoeff;
}

template<class T>
std::vector<T> plus(std::vector<T> a, std::vector<T> b){
     return {a[0]+b[0], a[1]+b[1], a[2]+b[2]};
}

template<class T>
std::vector<T> negate(std::vector<T> a){
     return {-a[0], -a[1], -a[2]};
}

double signF(double x){
    return x/std::abs(x);
}

struct RayTracer{
    RayTracer(
        py::array_t<double> raysStore
       ,py::array_t<double> distancesStore
       ,py::array_t<double> vpStore
       ,py::array_t<double> acousticImpedanceStore
       ,std::vector<double> mostNegativeCube
       ,std::vector<double> nCubes
       ,int rCube
       ) : raysStore(raysStore)
          ,distancesStore(distancesStore)
          ,vpStore(vpStore)
          ,acousticImpedanceStore(acousticImpedanceStore)
          ,mostNegativeCube(mostNegativeCube)
          ,nCubes(nCubes)
          ,rCube(rCube)
          ,raysMutable(raysStore.mutable_unchecked<3>())
          ,rays(raysStore.unchecked<3>())
          ,distancesMutable(distancesStore.mutable_unchecked<2>())
          ,distances(distancesStore.unchecked<2>())
          ,vp(vpStore.unchecked<3>())
          ,acousticImpedance(acousticImpedanceStore.unchecked<3>())
       {
       }

    std::vector<double> location(int iRay, int jRay) const{
        return {rays(iRay, jRay, 1), rays(iRay, jRay, 2), rays(iRay, jRay, 3)};
    }

    void setLocation(
        int iRay, int jRay, const std::vector<double>& updLocation){
        raysMutable(iRay, jRay, 1) = updLocation[0];
        raysMutable(iRay, jRay, 2) = updLocation[1];
        raysMutable(iRay, jRay, 3) = updLocation[2];
    }

    void setDirection(
        int iRay, int jRay, const std::vector<double>& updDirection){
        raysMutable(iRay, jRay, 4) = updDirection[0];
        raysMutable(iRay, jRay, 5) = updDirection[1];
        raysMutable(iRay, jRay, 6) = updDirection[2];
    }

    std::vector<double> direction(int iRay, int jRay) const{
        return {rays(iRay, jRay, 4), rays(iRay, jRay, 5), rays(iRay, jRay, 6)};
    }

    void addDistance(int iRay, int jRay, double distance){
         distancesMutable(iRay, jRay) += distance;
    }

    double scaleAmplitude(int iRay, int jRay, double scale){
        raysMutable(iRay, jRay, 8) *= scale;
        return rays(iRay, jRay, 8);
    }

    bool isInsideGrid(int iRay, int jRay) const{
        return isUnderZSurface(iRay, jRay)
              && isInsideXGrid(iRay, jRay)
              && isInsideYGrid(iRay, jRay);
    }

    bool isInsideXGrid(int iRay, int jRay) const{
        double x = location(iRay, jRay)[0];
        double xBorder = mostNegativeCube[0];
        return x >= xBorder && x < xBorder + nCubes[0]*rCube;
    }

    bool isInsideYGrid(int iRay, int jRay) const{
        double y = location(iRay, jRay)[1];
        double yBorder = mostNegativeCube[1];
        return y >= yBorder && y < yBorder + nCubes[1]*rCube;
    }

    bool isUnderZSurface(int iRay, int jRay) const{
        double zSurface = mostNegativeCube[2] + nCubes[2]*rCube;
        return location(iRay, jRay)[2] < zSurface;
    }

    bool isLost(int iRay, int jRay) const{
        return rays(iRay, jRay, 7) == 1;
        //return false;
    }

    void rayLost(int iRay, int jRay){
        raysMutable(iRay, jRay, 7) = 1;
    }

    void addTime(int iRay, int jRay, double timeDelta){
        raysMutable(iRay, jRay, 0) += timeDelta;
    }

    void closestPlanes(std::vector<double>& result, const std::vector<double>& loc){
        std::vector<double> dists = plus(loc, negate(mostNegativeCube));
        std::vector<double> shifts = vectorIndexing(mostNegativeCube, RayTracer::idxsD);
        result[4] = ((static_cast<int>(dists[2]) / rCube) + 1) * rCube + shifts[4];
        result[0] = ((static_cast<int>(dists[0]) / rCube)    ) * rCube + shifts[0];
        result[1] = ((static_cast<int>(dists[0]) / rCube) + 1) * rCube + shifts[1];
        result[2] = ((static_cast<int>(dists[1]) / rCube)    ) * rCube + shifts[2];
        result[3] = ((static_cast<int>(dists[1]) / rCube) + 1) * rCube + shifts[3];
    }

    void cubeIndex(std::vector<int>& result, const std::vector<double>& loc){
        result[0] = static_cast<int>(loc[0] - mostNegativeCube[0]) / rCube;
        result[1] = static_cast<int>(loc[1] - mostNegativeCube[1]) / rCube;
        result[2] = static_cast<int>(-mostNegativeCube[2] - (loc[2] - mostNegativeCube[2])) / rCube;
    }

    py::array_t<double>& raysStore;
    py::array_t<double>& distancesStore;
    py::array_t<double>& vpStore;
    py::array_t<double>& acousticImpedanceStore;
    std::vector<double> mostNegativeCube;
    std::vector<double> nCubes;
    int rCube;
    py::detail::unchecked_mutable_reference<double, 3l> raysMutable;
    py::detail::unchecked_reference<double, 3l> rays;
    py::detail::unchecked_mutable_reference<double, 2l> distancesMutable;
    py::detail::unchecked_reference<double, 2l> distances;
    py::detail::unchecked_reference<double, 3l> vp;
    py::detail::unchecked_reference<double, 3l> acousticImpedance;

    static const std::vector<int> idxsD;
    static const std::vector<std::vector<double>> normals;
    static const double eps;
};

void refractionsDirection(
        std::vector<double>& updDirection
       ,double refractionCos
       ,const std::vector<double>& direction
       ,int iIntersectPlane
        ){

    //std::cout << "RRR" << std::endl;
    if (std::abs(direction[2] - 1) < RayTracer::eps && iIntersectPlane == 4){
        updDirection[0] = direction[0];
        updDirection[1] = direction[1];
        updDirection[2] = direction[2];
        //std::cout << "rrr" << std::endl;
        //print("direction:", direction);
        return;
    }

    int sign = RayTracer::normals[iIntersectPlane][RayTracer::idxsD[iIntersectPlane]];
    double refractionX = 0;
    double refractionY = 0;
    double refractionZ = 0;

    if (iIntersectPlane < 2){
        refractionX = sign*refractionCos;
        if (std::abs(direction[1]) < RayTracer::eps){
            refractionY = 0;
            refractionZ = signF(direction[2]) * std::sqrt(1 - std::pow(refractionX,2));
        }else if (std::abs(direction[2]) < RayTracer::eps){
            refractionZ = 0;
            refractionY = signF(direction[1]) * std::sqrt(1 - std::pow(refractionX,2));
        }else{
            double refractionYZrel = direction[2]/direction[1];
            refractionY = signF(direction[1])
                * std::sqrt((1 - std::pow(refractionX,2)) / (1 + std::pow(refractionYZrel,2)));
            refractionZ = refractionY * refractionYZrel;
        }
    }else if (iIntersectPlane < 4){
        refractionY = sign*refractionCos;
        if (std::abs(direction[0]) < RayTracer::eps){
            refractionX = 0;
            refractionZ = signF(direction[2]) * std::sqrt(1 - std::pow(refractionY,2));
        }else if (std::abs(direction[2]) < RayTracer::eps){
            refractionZ = 0;
            refractionX = signF(direction[0]) * std::sqrt(1 - std::pow(refractionY,2));
        }else{
            double refractionZXrel = direction[0]/direction[2];
            refractionZ = signF(direction[2])
                * std::sqrt((1 - std::pow(refractionY,2)) / (1 + std::pow(refractionZXrel,2)));
            refractionX = refractionZ * refractionZXrel;
        }
    } else{
        refractionZ = sign*refractionCos;
        if (std::abs(direction[0]) < RayTracer::eps){
            refractionX = 0;
            refractionY = signF(direction[1]) * std::sqrt(1 - std::pow(refractionZ,2));
        }else if (std::abs(direction[1]) < RayTracer::eps){
            refractionY = 0;
            refractionX = signF(direction[0]) * std::sqrt(1 - std::pow(refractionZ,2));
        }else{
            double refractionYXrel = direction[0]/direction[1];
            refractionY = signF(direction[1])
                * std::sqrt((1 - std::pow(refractionZ,2)) / (1 + std::pow(refractionYXrel,2)));
            refractionX = refractionYXrel * refractionY;
        }
    }
        //std::cout << "r" << refractionX << " " << refractionY  << " " << refractionZ << std::endl;
        updDirection[0] = refractionX;
        updDirection[1] = refractionY;
        updDirection[2] = refractionZ;

        normalize(updDirection);
}

const std::vector<int> RayTracer::idxsD = {0,0,1,1,2};

const std::vector<std::vector<double>> RayTracer::normals = {
            {-1, 0, 0}, { 1, 0, 0}, { 0,-1, 0}, { 0, 1, 0}, { 0, 0, 1}
    };

const double RayTracer::eps = 1e-9;

py::array_t<double> gridTrace(
        py::array_t<double> raysStore
       ,py::array_t<double> distances
       ,py::array_t<double> vpStore
       ,py::array_t<double> acousticImpedanceStore
       ,py::array_t<double> mostNegativeCube
       ,py::array_t<double> nCubes
       ,int rCube
       ,int nTheta
       ,int nPhi
       ){
    std::vector<double> a = {0,10,20,30,40};
    std::vector<double> b = {0,10,20,30,40};
    std::cout << "Start" << std::endl;

    std::vector<double> nCubesV(nCubes.data(), nCubes.data()+3);
    std::vector<double> mostNegativeCubeV(mostNegativeCube.data(), mostNegativeCube.data()+3);
    RayTracer rays = RayTracer(
            raysStore, distances, vpStore, acousticImpedanceStore, mostNegativeCubeV, nCubesV, rCube);

    std::vector<double> planes(5);
    std::vector<int> cubeIndexPrevLocation(3);
    std::vector<int> cubeIndexUpdLocation(3);
    double distanceToPlane        = 1e100;
    int iIntersectPlane           = 0;
    double lengthNormalToPlane    = 0;
    double curDistanceToPlane     = 0;
    double curLengthNormalToPlane = 0;
    double cosRayPlane            = 0;
    double stepsToIntersect       = 0;
    double timeOfSegment          = 0;
    double refractionIndexFrom    = 0;
    double refractionIndexTo      = 0;
    double incidenceSin           = 0;
    double incidenceCos           = 0;
    double refractionSin          = 0;
    double refractionCos          = 0;
    double fresnelCoeff           = 0;
    double updAmplitude           = 0;

    std::vector<double> updLocation(3);
    std::vector<double> updDirection(3);
    std::vector<double> prevLocation(3);
    int printsC = 0;
    std::cout << "Start cycle" << std::endl;
    std::cout << "Changed" << std::endl;
    for (int iRay = 0; iRay < 2*nTheta-1; ++iRay){
        for (int jRay = 0; jRay < 2*nPhi-1; ++jRay){
            int edgeI = 0;
            std::cout << "Ray: " << iRay << " " << jRay << std::endl;
            while (rays.isInsideGrid(iRay, jRay) && !rays.isLost(iRay, jRay) && edgeI < 300000){
                std::cout << edgeI << ": ";
                print("Location: ", rays.location(iRay,jRay));
                edgeI += 1;
                //std::cout << "CC---------------------------------------------------C:" << std::endl << printsC++ << " " ;
                //print("location:", rays.location(0,0));
                //print("direction:", rays.direction(0,0));
                if ((dot(rays.direction(iRay, jRay), RayTracer::normals[4]) <= 0) or edgeI > 200000){
                    //std::cout << "rayLost!" << std::endl;
                    rays.rayLost(iRay, jRay);
                }

                ///Trace ray through heterogeneous environment
                rays.closestPlanes(planes, rays.location(iRay, jRay));
                distanceToPlane        = 1e100;
                for (int iNormal = 0; iNormal < 5; ++iNormal){
                    if (dot(rays.direction(iRay, jRay), RayTracer::normals[iNormal]) > 0){
                        curLengthNormalToPlane = std::abs(
                            rays.location(iRay, jRay)[RayTracer::idxsD[iNormal]] - planes[iNormal]);
                        cosRayPlane = dot(rays.direction(iRay, jRay), RayTracer::normals[iNormal]);
                        curDistanceToPlane = curLengthNormalToPlane/cosRayPlane;
                        if (curDistanceToPlane < distanceToPlane){
                            //std::cout << "iNormal: " << iNormal << std::endl;
                            //std::cout << "curLengthNormalToPlane" << curLengthNormalToPlane << std::endl;
                            //std::cout << "RayTracer::idxsD[iNormal]" << RayTracer::idxsD[iNormal] << std::endl;
                            //std::cout << "planes[iNormal]" << planes[iNormal] << std::endl;
                            //print("planes: ", planes);
                            distanceToPlane = curDistanceToPlane;
                            iIntersectPlane = iNormal;
                            lengthNormalToPlane = curLengthNormalToPlane;
                        }
                    }
                }

                stepsToIntersect =
                    std::abs(lengthNormalToPlane /
                            rays.direction(iRay, jRay)[RayTracer::idxsD[iIntersectPlane]]);
                //std::cout << "stepsToIntersect: " << stepsToIntersect << std::endl;
                updLocation  =
                    plus(rays.location(iRay, jRay),
                            scalarProduct(stepsToIntersect + 1e-7, rays.direction(iRay, jRay)));
                prevLocation = rays.location(iRay, jRay);
                rays.setLocation(iRay, jRay, updLocation);
                rays.addDistance(iRay, jRay, norm(plus(prevLocation, negate(updLocation))));
                //print("location: ", rays.location(0,0));

                ///Time update
                rays.cubeIndex(cubeIndexPrevLocation, prevLocation);
                timeOfSegment = norm(plus(updLocation, negate(prevLocation)))/
                    tupleIndexing(rays.vp, cubeIndexPrevLocation);
                rays.addTime(iRay, jRay, timeOfSegment);

                ///Snell's law
                if (rays.isInsideGrid(iRay, jRay)){
                    //std::cout << ("SNELL'S") << std::endl;
                    rays.cubeIndex(cubeIndexUpdLocation, updLocation);
                    refractionIndexFrom = tupleIndexing(rays.acousticImpedance, cubeIndexPrevLocation);
                    refractionIndexTo   = tupleIndexing(rays.acousticImpedance, cubeIndexUpdLocation);
                    incidenceSin = std::sqrt(1 - std::pow(
                            dot(rays.direction(iRay, jRay), RayTracer::normals[iIntersectPlane])
                                ,2));
                    if (refractionIndexTo*incidenceSin > refractionIndexFrom){
                        //std::cout << "rayLost!!!" << std::endl;
                        rays.rayLost(iRay, jRay);
                    }else{
                        refractionSin = refractionIndexTo/refractionIndexFrom*incidenceSin;
                        refractionCos = std::sqrt(1 - std::pow(refractionSin,2));
                        //std::cout << "refractionsDirection:" << " "
                        //    << refractionCos << " " << iIntersectPlane << " ";
                        //print("", rays.direction(iRay, jRay));
                        refractionsDirection(updDirection,
                            refractionCos, rays.direction(iRay, jRay), iIntersectPlane);
                        //print("updDirection:", updDirection);
                        rays.setDirection(iRay, jRay, updDirection);

                        ///Fresnel's law
                        incidenceCos = std::sqrt(1 - std::pow(incidenceSin,2));
                        fresnelCoeff = 2 * refractionIndexTo * incidenceCos
                            / (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos);
                        updAmplitude = rays.scaleAmplitude(iRay, jRay, fresnelCoeff);
                    }
                }
            }
        }
    }
    std::cout << "End cycle" << std::endl;
    return raysStore;
}

PYBIND11_MODULE(rayTracer, m) {
     m.def("gridTrace", &gridTrace, "t");
}


