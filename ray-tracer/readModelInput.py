import numpy as np
import h5py
import os
import rayTracer
from readModellingData import *
import time
from visualize import *

path = "data/data/model3D_1300_pow1"
path = "data/data1"
nTensorPath  = lambda i: path + "/"
geometryPath = lambda i: nTensorPath(i) + "geometry.txt"
powerPath    = lambda i: nTensorPath(i) + "power.txt"
propertyPath = lambda i: nTensorPath(i) + "property.txt"

with open("field.txt","r") as f:
    lines = f.readlines()
    print(len(lines))
    signalNames     = []
    signalCoords    = np.zeros((len(lines), 4))
    for (i,line) in zip(range(0,99999,1),lines):
       line  = ''.join(list(filter(lambda x: x!='\n', line)))
       cells = line.split(" ")
       cells = list(filter(lambda x: x, cells))
       signalNames.append(np.string_(cells[0]))
       #print("lime:", i, "cell:", cells)
       signalCoords[i,:] = cells[1:]
    signalNames = np.array(signalNames)

receiversCoordinates = (signalCoords[:,:2] - 100) * 25
geometryFile = open(geometryPath(1),"r")

nXNodes, nYNodes, nZNodes = list(map(int, geometryFile.readline().split(" "))) 

startXNodes, nodeXSize = list(map(int, geometryFile.readline().split(" ")[:2]))
startYNodes, nodeYSize = list(map(int, geometryFile.readline().split(" ")[:2]))
startZNodes, nodeZSize = list(map(int, geometryFile.readline().split(" ")[:2]))

geometryFile.close()

powerFile = open(powerPath(1), "r")
powerFile.readline()

xPower, yPower, zPower = list(map(int,powerFile.readline().split(" ")[:3]))

powerFile.close()

propertyFile = open(propertyPath(1), "r")

print("startReadProperty")
startTime = time.time()

startsCubes = []
endsCubes   = []
youngs      = []
poissons    = []
densities   = []
for line in propertyFile.readlines():
    #print("line:", line)
    #print("line:", line.split(" "))
    startX, startY, startZ, \
    endX  , endY  , endZ  , \
       = list(map(int,list(filter(lambda x: len(x) > 0, line.split(" ")))[:6]))
    young, poisson, density = list(map(float,list(filter(lambda x: len(x) > 0, line.split(" ")))[6:9]))

    startsCubes.append([startX, startY, startZ])
    endsCubes.append([endX, endY, endZ])
    youngs.append(young)
    poissons.append(poisson)
    densities.append(density)

print(time.time()-startTime, "s")
    
startCubes = np.array(startsCubes) 
finishCubes   = np.array(endsCubes  )
youngs      = np.array(youngs     )
poissons    = np.array(poissons   )
densities   = np.array(densities  )

shiftModule = youngs/(2*(1+poissons))
lame        = poissons*youngs/(1-poissons-2*poissons**2)
vps         = ((lame + 2*shiftModule) / densities)**(0.5)

startBounds  = np.amin(startsCubes,axis=0)
finishBounds = np.amax(endsCubes,axis=0)
rCube  = nodeXSize
nCubesShape = np.round(finishBounds - startBounds)
###nCubes = np.zeros((nCubesShape[0], nCubesShape[1], nCubesShape[2]))
###
###xPower = xPower - startBounds[0]
###yPower = yPower - startBounds[1]
#vp = np.zeros(nCubes.shape)
#density = np.zeros(nCubes.shape)
#
#print(time.time()-startTime, "s")
#print("startBounds", startBounds)
#for i in range(startCubes.shape[0]):
#    sx, sy, sz = startCubes[i,:] - startBounds
#    fx, fy, fz = finishCubes[i,:]
#    vp[sx:fx,sy:fy,sz:fz]      = vps[i]
#    density[sx:fx,sy:fy,sz:fz] = densities[i]
#print(time.time()-startTime, "s")
#
#vpf = h5py.File("vp.h5", "w")
#vpf["vp"] = vp
#vpf.close()
#
#densityf = h5py.File("density.h5", "w")
#densityf["density"] = density
#densityf.close()

vpf      = h5py.File("vp.h5"     , "r")
densityf = h5py.File("density.h5", "r")

vp      = vpf["vp"][()]
density = densityf["density"][()]

#vp[:] = 1000
#density[:] = 1

mostNegativeCube = np.array([0, 0, -nCubesShape[2]*rCube])
print("power:", xPower, yPower, zPower)
startRaysLocation = np.array([xPower*rCube, yPower*rCube, -zPower*rCube])
startRaysLocation = np.array([3150, 3625, -800])
startRaysLocation = np.array([111, 115, -40]) * 25
initialAmplitude = 1

#print(time.time()-startTime, "s")
#coords, s = readModellingData(200, np.array([xPower, yPower, zPower]), np.array([25,25,25]), range(1,3))
#print(time.time()-startTime, "s")

#coords = (coords[0] - startBounds[0], coords[1] - startBounds[1], coords[2] - startBounds[2])

nRays = 111
startAngle = 0.05*np.pi
iTrace = [0]
jTrace = [8]

ecs0 = (np.arange(150,202,4) - 100) * 25
ecs0 = (np.arange(150,202,4) - 100) * 25
ecs0 = (np.arange(190,242,4) - 100) * 25
ecs0 = (np.arange(190,242,4) - 100) * 25
ecs = np.zeros((ecs0.shape[0]*ecs0.shape[0], 2))
for i in range(ecs0.shape[0]):
   for j in range(ecs0.shape[0]):
       ecs[i*ecs0.shape[0] + j,:] = [ecs0[i], ecs0[j]]

#ecs0 = (np.arange(190,242,4) - 100) * 25

###ecs0 = (np.arange(220,242,2) - 100) * 25
###ecs1 = (np.arange(225,247,2) - 100) * 25
###
###ecs = np.zeros((ecs0.shape[0]*ecs0.shape[0], 2))
###for i in range(ecs0.shape[0]):
###   for j in range(ecs1.shape[0]):
###       ecs[i*ecs0.shape[0] + j,:] = [ecs0[i], ecs1[j]]

#nRecs                = ecs.shape[0]
#receiversCoordinates = np.zeros((nRecs,2))
#receiversCoordinates = ecs
#
receiversCoordinates = receiversCoordinates[:,:]
f = h5py.File("receiversCoordinates.h5", "w")
f["receiversCoordinates"] = receiversCoordinates
f.close()
print(time.time()-startTime, "s")
print("receiversCoordinatesFIRST",receiversCoordinates)
print("startRaysLocation",startRaysLocation)
print("mostNegativeCube:", mostNegativeCube)
#raise ValueError()

#receivedSignals, distancesToRays, rays, path, pathAmps, pathTimes, nPath, pathGradientDescent, pathGDIsLost = \
#   rayTracer.rayTracerDirect(receiversCoordinates, 200, 8, 5, nCubesShape, rCube, vp, density, startRaysLocation, initialAmplitude,mostNegativeCube,30,3,np.array(iTrace))
receivedSignals, distancesToRays, rays, tensorRays, path, pathAmps, pathTimes, nPath, initialArrangement = \
    rayTracer.rayTracer1(receiversCoordinates, 200, 8, 5, nCubesShape, rCube, vp, density, startRaysLocation, initialAmplitude,mostNegativeCube,nRays,nRays,startAngle,iTrace, jTrace)
print("ray tracer:", time.time()-startTime, "s")


rsf = h5py.File("S47.h5", "w")
rsf["S"] = receivedSignals
rsf["distancesToRays"] = distancesToRays
rsf["coord"] = receiversCoordinates
rsf["startRaysLocation"] = startRaysLocation
rsf["rays"] = rays
rsf["tensorRays"] = tensorRays
#rsf["pathGradientDescent"] = pathGradientDescent
#rsf["pathGDIsLost"] = pathGDIsLost
rsf.close()
print("receivedSignals.shape",receivedSignals.shape)
#plotPath(nCubesShape,25,mostNegativeCube,path,nPath,c=['xz','yz','xy'])

#l = path[0,nPath[0]-1,:]
#iRr = rayTracer.nearestReceiver(l[:2], coords[0]*25, coords[1]*25, coords[2]*25)

#Amplitude different receivers comparison
#plt.plot(np.arange(receivedSignals.shape[3])/200,receivedSignals[0,0,0,:], label="Ray Tracer 1")
#plt.plot(np.arange(receivedSignals.shape[3])/200,receivedSignals[1,0,0,:], label="Ray Tracer 2")
#plt.xlabel("Sec")
#plt.legend()
#plt.show()
#plt.plot(np.arange(s[0].shape[1])/200,s[0][0,:,cdI[0]]*1e18, label="Model 1")
#plt.plot(np.arange(s[0].shape[1])/200,s[0][0,:,cdI[1]]*1e18, label="Model 2")
#plt.xlabel("Sec")
#plt.legend()
#plt.show()

#Amplitude comparison
#plt.plot(np.arange(receivedSignals.shape[3])/200,receivedSignals[0,0,0,:], label="Ray Tracer xx")
#plt.plot(np.arange(receivedSignals.shape[3])/200,receivedSignals[0,1,0,:], label="Ray Tracer yy")
#plt.xlabel("Sec")
#plt.legend()
#plt.show()
#
#plt.plot(np.arange(s[0].shape[1])/200,s[0][0,:,cdI[0]]*1.5e17, label="Model xx")
#plt.plot(np.arange(s[0].shape[1])/200,s[0][1,:,cdI[0]]*1.5e17, label="Model yy")
#plt.xlabel("Sec")
#plt.legend()
#plt.show()

#Time comparison
#plt.plot(np.arange(receivedSignals.shape[3])/200,receivedSignals[0,0,0,:], label="Ray Tracer")
#plt.plot(np.arange(s[0].shape[1])/200,s[0][0,:,cdI[0]]*1e9,  label="Model")
#plt.xlabel("Sec")
#plt.legend()
#plt.show()
#for i in cdI:
#    plt.plot(receivedSignals[i,0,0,:])
#    plt.plot(s[0][0,:,i]*1e21)
#    plt.show()
