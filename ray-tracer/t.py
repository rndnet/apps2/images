import numpy as np
import itertools

signalCoords = np.random.randn(7,2)
print(signalCoords)

foldFunctions = [np.amax, np.amin]
foldAxes      = [0,1]
extremum = lambda f: lambda axis: f(signalCoords[:,axis])

m = min(map(lambda x: extremum(x[0])(x[1]),
        itertools.product(foldFunctions, foldAxes)))

print(m)
   
