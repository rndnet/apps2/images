import numpy as np
import numpy.linalg as npl
import scipy.stats as scs
import math, random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sympy import primefactors

from visualize import plotPath, plotMediumPath

import h5py

A homotopy is a continuous interpolation between two loops. More precisely, a homotopy between two loops $\gamma, \gamma' : [0,1] \to X$ (based at the same point $x_0$) is a continuous map
:$h : [0, 1] \times [0, 1] \to X,$
such that
:$h(0, t) = x_0$ for all $t \in [0,1],$ that is, the starting point of the homotopy is $x_0$ for all ''t'' (which is often thought of as a time parameter). 
:$h(1, t) = x_0$ for all $t \in [0,1],$ that is, similarly the end point stays at $x_0$ for all ''t''.
:$h(r, 0) = \gamma(r), h(r, 1) = \gamma'(r)$ for all $r \in [0,1].$

#xs = [1,3,5,7,8.7]
#ys = [3,5,6,4,3.3]
#plt.plot(xs, ys, marker='x', mec='red', mew=3)
#
##xs = [1,3.7]
##ys = [3,3]
##plt.plot(xs, ys, marker='x', mec='red', mew=3)
#plt.show()
#
#fd = 200
#length = 2000
#waveComing = 7
#fdPWave = 8
#sigma = 1/(7*fdPWave)
#gaussWavelet = lambda mean, var: lambda x:\
#          -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
#gaussWavelet2 = lambda mean, var: lambda x:\
#          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
#
#gwt = gaussWavelet2(waveComing,sigma)(np.arange(length)/fd)

#plt.plot(gwt)
#plt.show()
#plt.plot(np.arange(1000)/10,np.abs(np.fft.fft(gwt))[:length//2])
#plt.show()

f = h5py.File("receiversCoordinates.h5", "r")
receiversCoordinates = f["receiversCoordinates"][()]

f = h5py.File("S47.h5", "r")
S = f["S"][()]
rays = f["rays"][()]
tensorRays = f["tensorRays"][()]
locations = []
times = []
amplitudes = []
for i in range(rays.shape[0]):
    for j in range(rays.shape[1]):
        if rays[i,j,7] == 0 and rays[i,j,3] >= 0:
            locations.append(rays[i,j,1:3]) 
            times.append(rays[i,j,0])
            amplitudes.append(rays[i,j,8]*tensorRays[i,j,2,3])
            #amplitudes.append(rays[i,j,8])
            if rays[i,j,3] < 0:
                print("STRANGE", rays[i,j,1:4])

locations = np.array(locations) 
times = np.array(times)
amplitudes = np.array(amplitudes)

#plt.scatter(locations[:,0], locations[:,1], s=3, c=times, cmap='nipy_spectral')
plt.scatter(locations[:,0], locations[:,1], s=3, c=amplitudes, cmap='nipy_spectral')
plt.colorbar()
plt.scatter([2775], [2875], marker='x', c='red', s=73)
plt.scatter(receiversCoordinates[:,0], receiversCoordinates[:,1], c='orange', s=47, marker='*')
plt.legend(['Income rays on Surface', 'Source of Signal', 'Sensors'], loc='upper right')
#plt.legend(['Income rays on Surface (color for amplitudes)', 'Source of Signal', 'Sensors'], loc='upper right')
plt.title('Depth = 1000, Minimum Angle = 0.05*pi. Z. Tensor: XY')
plt.show()


coords = f["coord"]
distancesToRays = f["distancesToRays"]
startRaysLocation = f["startRaysLocation"]
pgd = f["pathGradientDescent"]
pgl = f["pathGDIsLost"][()]
    	
print(coords.shape)
print(np.where(pgl[0,:]==0)[0])
#plt.scatter(startRaysLocation[0], startRaysLocation[1], s=173, c='red', marker="x")
#plt.scatter(coords[:,0], coords[:,1], s=73, c=distancesToRays, cmap="nipy_spectral")
#plt.colorbar()
#plt.show()

#plt.scatter(pgd[0,np.where(pgl[0,:]==1)[0],0], pgd[0,np.where(pgl[0,:]==1)[0],1], s=1, c='red')
#plt.scatter(pgd[0,np.where(pgl[0,:]==0)[0],0], pgd[0,np.where(pgl[0,:]==0)[0],1], s=1, c='blue')

plt.scatter(pgd[2,np.where(pgl[2,:]==1)[0],0], pgd[2,np.where(pgl[2,:]==1)[0],1], s=1, c='red')
plt.scatter(pgd[2,np.where(pgl[2,:]==0)[0],0], pgd[2,np.where(pgl[2,:]==0)[0],1], s=7, c='blue')
plt.scatter(startRaysLocation[0], startRaysLocation[1], s=173, c='red', marker="x")
plt.scatter(coords[[2],0], coords[[2],1], s=73)
plt.show()

print(np.mean(np.abs(S-S0)), np.mean(np.abs(S)), np.mean(np.abs(S0)))
print("C:",coords[()])
print(np.mean(distancesToRays))
print(np.var(distancesToRays))
print(np.amax(distancesToRays))
print(coords[np.argmax(distancesToRays),:])
print(np.argmax(distancesToRays))
#plt.plot(distancesToRays)
#plt.show()
#plt.hist(distancesToRays,100)
#plt.show()

ecs0 = (np.arange(190,242,4) - 100) * 25

ecs = np.zeros((ecs0.shape[0]*ecs0.shape[0], 2))
for i in range(ecs0.shape[0]):
   for j in range(ecs0.shape[0]):
       ecs[i*ecs0.shape[0] + j,:] = [ecs0[i], ecs0[j]]

#plt.imshow(S[49:53,0,0,:777], aspect='auto', cmap='jet')
#plt.imshow(S[:,0,0,:], aspect='auto', cmap='jet')
#plt.colorbar()
#plt.show()
#plt.imshow(S0[:,0,0,:], aspect='auto', cmap='jet')
#plt.colorbar()
#plt.show()
#plt.plot(ecs[:,0], ecs[:,1])
#plt.show()

