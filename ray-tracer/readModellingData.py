import numpy as np
import matplotlib.pyplot as plt
import os 
import h5py

def readModellingData(fd, strikeCoords, cellLengths, tensors=range(1,7)):
    print("startReadModelling")
    cellLengthX, cellLengthY, cellLengthZ = cellLengths
    toInt = lambda x: int(x)
    toFloat = lambda x: float(x)
    isLikeNumber = lambda x: x and x[0].isdigit()
    toFloats = lambda line: list(map(toFloat, filter(isLikeNumber, line)))
    filepath = "data/model3D_1300_pow"
    nPatches = 4 
    
    px = strikeCoords[0]
    py = strikeCoords[1]
    pz = strikeCoords[2]
    
    nCoords = np.zeros(nPatches, dtype=np.int64)
    for iPatch in range(nPatches):
       path = filepath + "/"
       f = h5py.File(path + "micros0-"+str(iPatch)+".h5", "r")
       nCoords[iPatch] = f["pcoords"][()].shape[0]
    print(nCoords)
    
    f = h5py.File(path + "micros0-0.h5", "r")
    nModelTicks = len(list(filter(lambda x: x[7:].isdigit(), list(f.keys()))))
    print(nModelTicks)
    
    arrayCoords = np.zeros((np.sum(nCoords),3))
    sx = np.zeros((6,nModelTicks,np.sum(nCoords)))
    sy = np.zeros((6,nModelTicks,np.sum(nCoords)))
    sz = np.zeros((6,nModelTicks,np.sum(nCoords)))
    
    for iTensor in tensors:
       print("iTensor:", iTensor)
       for iPatch in range(nPatches):
          print("iPatch:", iPatch)
          f = h5py.File(filepath + str(iTensor) + "/" + "micros0-" + str(iPatch) + ".h5", "r")
          pcoords = f["pcoords"][()] 
          s = int(np.sum(nCoords[:iPatch  ]))
          e = int(np.sum(nCoords[:iPatch+1]))
          arrayCoords[s:e,:] = pcoords
          for t in range(nModelTicks):
             sx[iTensor-1, t, s:e] = f["points-"+str(t+1)][()][:,0]
             sy[iTensor-1, t, s:e] = f["points-"+str(t+1)][()][:,1]
             sz[iTensor-1, t, s:e] = f["points-"+str(t+1)][()][:,2]  
          print("t=", t)
    
    xCoords     = arrayCoords[:,0]/cellLengthX
    yCoords     = arrayCoords[:,1]/cellLengthY
    zCoords     = arrayCoords[:,2]/cellLengthZ
    
    depth_m = pz*cellLengthZ
    data = dict(
            sx      = sx
           ,sy      = sy
           ,sz      = sz
           ,xCoords = xCoords
           ,yCoords = yCoords
           ,zCoords = zCoords)
    meta = dict(
            fd = fd
           ,depth_m = depth_m 
           ,px = px
           ,py = py
           ,pz = pz
           )
    return (xCoords, yCoords, zCoords), (sx, sy, sz)
