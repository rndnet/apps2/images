import math
import numpy as np
import pandas as pd
from datetime import datetime
from sklearn.preprocessing import OneHotEncoder
from sklearn.decomposition import PCA
from functools import reduce 
from itertools import count
from enum import Enum
import matplotlib.pyplot as plt
import matplotlib        as mpl
import numpy.linalg as npl
import scipy.linalg as scl
from sklearn import mixture
import h5py 

eSc = h5py.File('data/e/signalCoords.h5')['signalCoords'][()][:,2:]
rSc = h5py.File('data/r/signalCoords.h5')['signalCoords'][()][:,2:]

eSn = h5py.File('data/e/signalNames.h5')['signalNames'][()]
rSn = h5py.File('data/r/signalNames.h5')['signalNames'][()]

eS = h5py.File('data/e/S.h5', 'r')['S'][()]
rS = h5py.File('data/r/S.h5', 'r')['S'][()]

centeredESc = eSc - np.mean(eSc, axis=0)
centeredRSc = rSc - np.mean(rSc, axis=0)
print(eS.shape)
print(rS.shape)

rotationMatrix   = np.array([[0  , -1]
                            ,[1  ,  0]])
reflectionMatrix = np.array([[1  ,  0]
                            ,[0  , -1]])

transformationMatrix = np.dot(reflectionMatrix, rotationMatrix)
print('transform:')
print(transformationMatrix)

mSc = np.dot(eSc, rotationMatrix)
mSc = np.dot(transformationMatrix, eSc.T).T

plt.scatter(eSc[:,0], eSc[:,1])
for i,name in enumerate(eSn):
    plt.text(eSc[i,0], eSc[i,1], name)
plt.title('e')
plt.figure()

plt.scatter(rSc[:,0], rSc[:,1])
for i,name in enumerate(rSn):
    plt.text(rSc[i,0], rSc[i,1], name)
plt.title('r')
plt.figure()

plt.scatter(mSc[:,0], mSc[:,1])
for i,name in enumerate(eSn):
    plt.text(mSc[i,0], mSc[i,1], name)
plt.title('m')
plt.show()


eS = eS.reshape(3, -1, 1000)[:2,:]
rS = rS.reshape(3, -1, 1000)[:2,:]

plt.plot(eS[1,np.where(eSn==np.string_('49'))[0][0]*6+4,:])
plt.plot(rS[1,np.where(rSn==np.string_('1'))[0][0]*6+4,:])
plt.show()

print(np.sum(np.abs(eS-rS)))

mS = np.zeros(eS.shape)

for s in range(eS.shape[1]):
    mS[:,s,:] = np.dot(transformationMatrix, eS[:,s,:])
print(np.sum(np.abs(rS-mS)))

def getOriginAmps(S):
    def getOriginAmp(S):
        return S[np.argmax(np.abs(S))]
    out = np.zeros((S.shape[0], S.shape[1]))
    for i in range(S.shape[0]):
        for j in range(S.shape[1]):
            out[i,j] = getOriginAmp(S[i,j,:])
    return out


#originEs = getOriginAmps(eS)[:2,np.array(range(eS.shape[1]//6))*6]
#originRs = getOriginAmps(rS)[:2,np.array(range(eS.shape[1]//6))*6]
originEs = getOriginAmps(eS)[:2,:]
originRs = getOriginAmps(rS)[:2,:]
print(np.sum(np.abs(originEs-originRs)))

originEs = np.dot(transformationMatrix, originEs)
print(np.sum(np.abs(originEs-originRs)))
        

def rta(curDirection):
    normals = np.array([[-1,0], [1,0], [0,-1], [0,1], [0,0]])
    xCos = np.dot(curDirection, normals[1])
    yCos = np.dot(curDirection, normals[3])
    o = np.zeros(2)
    o1 = np.zeros(2)
    o[0] =       6*xCos**3          - 3*xCos
    o[1] =       6*(xCos**2)*yCos   -   yCos
    o1[0] =      6*xCos*(yCos**2)   -   xCos
    o1[1] =      6*(yCos**3)        - 3*yCos
    return o, o1

def printTensorLaws(T, x=np.array([-100,-1,1])):
    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    def getTensorExpansion(curDirection):
        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])

        tensors = np.zeros((3,7))
        tensors[0,0] =  6*xCos**3          - 3*xCos
        tensors[1,0] =  6*(xCos**2)*yCos   -   yCos
        tensors[2,0] =  6*(xCos**2)*zCos   -   zCos

        tensors[0,1] =  6*xCos*(yCos**2)   -   xCos
        tensors[1,1] =  6*(yCos**3)        - 3*yCos
        tensors[2,1] =  6*(yCos**2)*zCos   -   zCos

        tensors[0,2] =  6*xCos*(zCos**2)   -   xCos
        tensors[1,2] =  6*(yCos)*(zCos**2) -   yCos
        tensors[2,2] =  6*(zCos**3)        - 3*zCos

        tensors[0,3] =  6*(xCos**2)*(yCos) -   yCos
        tensors[1,3] =  6*(yCos**2)*(xCos) -   xCos
        tensors[2,3] =  6*xCos*yCos*(zCos)

        tensors[0,4] = (6*(xCos**2)*(zCos) -   zCos)
        tensors[1,4] = (6*xCos*yCos*(zCos)         )
        tensors[2,4] = (6*(zCos**2)*xCos   -   xCos)

        tensors[0,5] = (6*xCos*yCos*(zCos)         ) 
        tensors[1,5] = (6*(yCos**2)*(zCos)  -  zCos)
        tensors[2,5] = (6*(zCos**2)*yCos    -  yCos)

        return tensors
    
    ExpX  = getTensorExpansion(x) 
    Tx = np.dot(T,x)
    ExpTx = getTensorExpansion(np.dot(T,x))
    tensors = ['XX', 'YY', 'ZZ', 'XY', 'XZ', 'YZ']
    for i in range(len(tensors)):
        print(tensors[i])
        print(ExpX[:,i])
        print(ExpTx[:,i])
        print()

def shiftToSensors(coords, S):
    for i in range(coords.shape[0]):
        S[:,i*6:(i+1)*6] = coords[i,:].reshape(2,1)+S[:,i:i+6]
    return S


#print(rta(np.array([1,7])))
#print(rta(np.dot(transformationMatrix,np.array([1,7]))))
#print(originEs[:2,3*6+0])
#print(originRs[:2,3*6+1])
#print(printTensorLaws(transformationMatrix))
transformationMatrix3D = np.eye(3)
transformationMatrix3D[:2,:2] = transformationMatrix
print(printTensorLaws(transformationMatrix3D))
#print(printTensorLaws(np.eye(3)))

#originEs = shiftToSensors(eSc, originEs)
#originRs = shiftToSensors(rSc, originRs)

transformed = np.dot(transformationMatrix, originEs)
#for i in range(transformed.shape[1]//6):
#    #if i%6 == 0:
#    #    print('============================================')
#    index = i*6
#    print(rSn[i], eSn[i])
#    print(centeredESc[i], centeredRSc[i])
#    print('XX:', originEs[:,index], transformed[:,index], originRs[:,index])
#    print('YY:', originEs[:,index+1], transformed[:,index+1], originRs[:,index+1])
#    print('ZZ:', originEs[:,index+2], transformed[:,index+2], originRs[:,index+2])
#    print('XY:', originEs[:,index+3], transformed[:,index+3], originRs[:,index+3])
#    print('XZ:', originEs[:,index+4], transformed[:,index+4], originRs[:,index+4])
#    print('YZ:', originEs[:,index+5], transformed[:,index+5], originRs[:,index+5])
##    print('================')
#print(originEs[:,0], originRs[:,1])
#plt.show() #print(transformationMatrix)
#print(np.dot(transformationMatrix, originEs[:2,7]))
#print(eSn)
#print(rSn)
#plt.scatter(originEs[0,np.array(range(eS.shape[1]//6))*6], originEs[1,np.array(range(eS.shape[1]//6))*6], s=71, c=range(eS.shape[1]//6), marker='o', cmap='jet')
#plt.scatter(originRs[0,np.array(range(eS.shape[1]//6))*6], originRs[1,np.array(range(eS.shape[1]//6))*6], s=71 , c=range(eS.shape[1]//6), marker='x', cmap='jet')
#plt.scatter(originEs[0,np.array(range(eS.shape[1]//6))[:17]*6], originEs[1,np.array(range(eS.shape[1]//6))[:17]*6], s=71,cmap='jet')
#plt.scatter(originRs[0,np.array(range(eS.shape[1]//6))[:17]*6], originRs[1,np.array(range(eS.shape[1]//6))[:17]*6], s=71,cmap='jet')
#plt.show()

#S[iChannel*(nSens*6) + iSen*6 + iTensor , : ]
