. common
#!/bin/bash
set -eou

if [ $# -eq 0 ]; then
  CM=podman
else
  CM=$1
fi

echo "Use $CM as container manager"

echo
echo "Build ....."
$CM build -t ${REP}:${TAG}  --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)" --no-cache --format docker -f Dockerfile .
#$CM build -t ${REP}:${VER} -t ${REP}:${TAG}  --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)" --no-cache -f Dockerfile .

echo
echo Upload images
$CM login docker.io
#$CM push ${REP}:${VER} 
$CM push ${REP}:${TAG}

