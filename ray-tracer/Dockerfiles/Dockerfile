FROM docker.io/ubuntu:18.04 as safe

ARG ssh_prv_key
ARG ssh_pub_key

RUN apt-get update
RUN apt-get install -y git

# Authorize SSH Host
RUN mkdir -p /root/.ssh
RUN chmod 0700 /root/.ssh
RUN ssh-keyscan gitlab.com > /root/.ssh/known_hosts

RUN echo "${ssh_prv_key}" > /root/.ssh/id_rsa
RUN echo "${ssh_pub_key}" > /root/.ssh/id_rsa.pub
RUN chmod 600 /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa.pub

RUN git clone --recursive git@gitlab.com:modernseismic/rnd/ray-tracer.git /raytracer-src

RUN rm /root/.ssh/id_rsa


FROM docker.io/ubuntu:18.04

ENV PYTHONPATH=/raytracer-src

COPY --from=safe /raytracer-src /raytracer-src

RUN apt-get update
RUN apt-get install -y git g++ make python3 python3-pip openssh-server
RUN apt-get install -y libhdf5-serial-dev curl moreutils

RUN pip3 install numpy scipy matplotlib sympy pybind11

RUN cd /raytracer-src && make 
RUN cd /opt/ && curl https://gitlab.com/rndnet/apps2/job.py/-/archive/master/job.py-master.tar | tar -x && pip3 install -r job.py-master/requirements.txt && pip3 install ./job.py-master && rm -rf job.py-master
