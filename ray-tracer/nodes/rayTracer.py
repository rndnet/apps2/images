#!/usr/bin/env python3
import numpy as np
import time
import numpy.linalg as npl
import scipy.stats as scs
import math, random
from sympy import primefactors

import bfj

def fibonacciSphere(samples=1,randomize=True):
    rnd = 1.
    if randomize:
        rnd = random.random() * samples

    points = []
    offset = 2./samples
    increment = math.pi * (3. - math.sqrt(5.));

    for i in range(samples):
        y = ((i * offset) - 1) + (offset / 2);
        r = math.sqrt(1 - pow(y,2))

        phi = ((i + rnd) % samples) * increment

        x = math.cos(phi) * r
        z = math.sin(phi) * r

        points.append([x,y,z])

    return np.array(points)

def rayTracer(
        receiversCoordinates # (number of receivers, 2)
       ,fd
       ,fdPWave
       ,timeLength
       ,nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,startRaysLocation
       ,initialAmplitude
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,nTheta           # number of theta steps in quarter
       ,nPhi
       ,startAngle = np.pi/10
       ,iTrace   = 0
       ,jTrace   = 0
       ):
    print("Start Ray Tracer")

    ###Initial setup
    eps = 1e-9
    startSphereRadius = rCube/1000
    #rays = (theta, phi, time, x, y, z, dirX, dirY, dirZ, isLost, amplitude)
    rays = np.zeros((2*nTheta-1,2*nPhi-1,9))
    distances = np.zeros((2*nTheta-1,2*nPhi-1))
    nPars = rays.shape[2]

    ###Utils definition
    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]
    initialLocation = lambda iTheta, iPhi: initialArrangement2D[iTheta,iPhi,:]

    def addDistance(iTheta, iPhi, distance):
        distances[iTheta,iPhi] += distance
    distance = lambda iTheta, iPhi: distances[iTheta, iPhi] 

    def shiftAllLocations(v):
        rays[:,:,1:4] += np.array(v)

    def setDirection(iTheta, iPhi, direction):
        rays[iTheta,iPhi,4:7] = direction
    direction = lambda iTheta, iPhi: rays[iTheta,iPhi,4:7]
    initialDirection = lambda iTheta, iPhi: initialDirection2D[iTheta,iPhi,:]

    def scaleLocation(s):
        rays[:,:,1:4] *= s
    def setLocToDir():
        rays[:,:,4:7] = rays[:,:,1:4]

    def rayLost(iTheta, iPhi):
        rays[iTheta, iPhi, 7] = 1
    isLost = lambda iTheta, iPhi: rays[iTheta, iPhi, 7] == 1

    def cubeIndex(loc):
        x = int((loc[0] - mostNegativeCube[0])) // rCube
        y = int((loc[1] - mostNegativeCube[1])) // rCube
        z = int((loc[2] - mostNegativeCube[2])) // rCube
        #if x < 0 or y < 0 or z < 0:
        #    raise ValueError("mostNegativeCube: ", mostNegativeCube, " Location of power: ", loc )
        return x,y,z

    def setAmplitude(amp):
        rays[:,:,8] = amp
    def scaleAmplitude(iTheta, iPhi, scale):
        if scale < 0:
            raise ValueError("TRYING TO REVERSE AMPLITUDE!")
        rays[iTheta,iPhi,8] *= scale
        return rays[iTheta,iPhi,8]
    amplitude = lambda iTheta, iPhi: rays[iTheta, iPhi, 8]

    def addTime(iTheta, iPhi, t):
        rays[iTheta,iPhi,0] += t
        return rays[iTheta,iPhi,0]
    rayTime = lambda iTheta, iPhi: rays[iTheta,iPhi,0]

    times = lambda rays: rays[:,:,0]

    ###

    #Initialize rays
    setAmplitude(initialAmplitude)

    thetaSpace = np.linspace(startAngle,np.pi/2,nTheta)
    phiSpace   = np.linspace(startAngle,np.pi/2,nPhi  )

    for iTheta in range(nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta, iPhi, np.array([np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)]) )

    rays[nTheta:2*nTheta-1,:,:] = np.copy(rays[:nTheta-1,:,:])
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = np.copy(rays[:,:nPhi-1,:])
    rays[:,nPhi:2*nPhi-1,2] *= -1

    setLocToDir()

    scaleLocation(startSphereRadius)

    shiftAllLocations(startRaysLocation)

    initialArrangement   = np.copy(rays.reshape(-1,nPars)[:,1:4])
    initialArrangement2D = np.copy(rays[:,:,1:4])
    initialDirection2D = np.copy(rays[:,:,4:7])

    ###Trace debug setup
    iTrace = np.array(iTrace)
    jTrace = np.array(jTrace)
    traces = iTrace.shape[0]
    path     = np.zeros((traces,10000,3))
    pathAmps = np.zeros((traces,10000))
    pathTimes = np.zeros((traces))
    for i in range(traces):
        path[i,0,:] = location(iTrace[i],jTrace[i])
        pathAmps[i,0] = 1
    nPath = np.ones(traces, dtype=np.int)

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])
    def refractionsDirection(refractionCos, direction, iIntersectPlane):
        #According to Snell's law
        updDirection = np.zeros(3)
        if np.abs(direction[2] - 1) < eps and iIntersectPlane == 4:
            return direction
        sign = normals[iIntersectPlane][idxsD[iIntersectPlane]]
        refractionX = 0
        refractionY = 0
        refractionZ = 0
        signF = lambda x: x/np.abs(x)
        if iIntersectPlane < 2:
            refractionX = sign*refractionCos
            if np.abs(direction[1]) < eps:
                refractionY = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionX**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionX**2)
            else:
                refractionYZrel = direction[2]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionX**2) / (1 + refractionYZrel**2))
                refractionZ = refractionY*refractionYZrel
        elif iIntersectPlane < 4:
            refractionY = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionY**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionY**2)
            else:
                refractionZXrel = direction[0]/direction[2]
                refractionZ = signF(direction[2]) * np.sqrt((1 - refractionY**2) / (1 + refractionZXrel**2))
                refractionX = refractionZ * refractionZXrel
        else:
            refractionZ = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionZ**2)
            elif np.abs(direction[1]) < eps:
                refractionY = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionZ**2)
            else:
                refractionYXrel = direction[0]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionZ**2) / (1 + refractionYXrel**2))
                refractionX = refractionYXrel * refractionY

        updDirection = np.array([refractionX, refractionY, refractionZ])
        return updDirection/npl.norm(updDirection)


    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    def triangleAreaBy3Sides(a, b, c):
        halfPerimeter = (a+b+c)/2
        return np.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c))

    def triangleAreaBy3Points(A, B, C):
        a = npl.norm(A-B)
        b = npl.norm(C-B)
        c = npl.norm(C-A)
        return triangleAreaBy3Sides(a, b, c)

    def triangleAmplitudeNorm(iCurRay, jCurRay):
        quarter1 = (None, 1e111)
        quarter2 = (None, 1e111)
        quarter3 = (None, 1e111)
        quarter4 = (None, 1e111)

        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isUnderZSurface(iRay, jRay) and not isLost(iRay,jRay):
                    distV = location(iCurRay, jCurRay) - location(iRay, jRay)
                    distance = npl.norm(distV)
                    if distance > 1e-7:
                        if distV[0] >= 0 and distV[1]  < 0 and distance < quarter1[1]:
                            quarter1 = ((iRay, jRay), distance)
                        if distV[0] >= 0 and distV[1] >= 0 and distance < quarter2[1]:
                            quarter2 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1]  < 0 and distance < quarter3[1]:
                            quarter3 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1] >= 0 and distance < quarter4[1]:
                            quarter4 = ((iRay, jRay), distance)

        closestRays = list(filter(lambda x: x[0] != None, [quarter1, quarter2, quarter3, quarter4]))

        if len(closestRays) < 1:
            raise ValueError("There is no triangles are found!")

        finishAreas = []
        startAreas  = []
        for i in range(len(closestRays)):
            finishDist = npl.norm(location(*closestRays[i-1][0]) - location(*closestRays[i][0]))
            finishAreas.append(triangleAreaBy3Sides(closestRays[i-1][1], closestRays[i][1], finishDist))
            startAreas.append(
                triangleAreaBy3Points(
                    initialLocation(iCurRay,jCurRay), 
                    initialLocation(*closestRays[i-1][0]), 
                    initialLocation(*closestRays[i][0])
                    ))

    #Triangle norm where we take triangles on the start unit sphere and then try to find their images
    def triangleAmplitudeNormReverse(iCurRay, jCurRay):

        finishAreas = []
        startAreas  = []
        startAreasAppend = lambda iR0, jR0, iR1, jR1: \
            startAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(initialLocation(iR0,jR0) - initialLocation(iR1,jR0))
                 ,npl.norm(initialLocation(iR1,jR0) - initialLocation(iR0,jR1))
                 ,npl.norm(initialLocation(iR0,jR0) - initialLocation(iR0,jR1))
                 ))
        finishAreasAppend = lambda iR0, jR0, iR1, jR1: \
            finishAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(location(iR0,jR0) - location(iR1,jR0))
                 ,npl.norm(location(iR1,jR0) - location(iR0,jR1))
                 ,npl.norm(location(iR0,jR0) - location(iR0,jR1))
                 ))
        areasAppend = lambda iR, jR, iR1, jR1: \
             startAreasAppend(iR, jR, iR1, jR1) == \
             finishAreasAppend(iR, jR, iR1, jR1)
        
        isLost0 = lambda iR, jR: isLost(iR,jR) and (not isUnderZSurface(iR,jR))

        isNotLost = lambda iR, jR, iR0, jR0: \
                not (isLost0(iR,jR) or isLost0(iR0,jR0) or isLost0(iR,jR0) or isLost0(iR0,jR))

        if iCurRay+1 < 2*nTheta-1 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay-1)
        if iCurRay+1 < 2*nTheta-1 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay-1)

        if len(startAreas) < 3:
            return False, None
        #print("Areas:", np.array(finishAreas), " ", np.array(startAreas))
        
        return True, np.sqrt(np.median(np.array(finishAreas)) / np.median(np.array(startAreas)))

    def approxParamsByClosestRay(point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
        minD = dists[curMinimumIdx]
        minId = curMinimumIdx

        outTime = rayTime(*minId)
        outAmp  = amplitude(*minId)
        outDirection = initialDirection(*minId)

        return outTime, outAmp, outDirection, minD

    def distanceNorm(iCurRay, jCurRay):
        return True, distance(iCurRay, jCurRay)

    def approxParamsByClosestRays(n, point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        mins    = []
        minsD   = np.zeros(n)
        weights = np.zeros(n)
        for i in range(n):
            curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
            minsD[i] = dists[curMinimumIdx]
            mins.append(curMinimumIdx)
            dists[curMinimumIdx] = 1e111
            weights[i] = scs.multivariate_normal.pdf(point, location(*curMinimumIdx)[:2], np.eye(2)*minsD[0])


        outTime = 0
        outAmp  = 0
        outDirection = np.zeros(3)
        for i in range(n):
            outTime += (weights[i]/np.sum(weights)) * rayTime(*mins[i])
            outAmp  += (weights[i]/np.sum(weights)) * amplitude(*mins[i])
            outDirection += (weights[i]/np.sum(weights)) * direction(*mins[i])

        return outTime, outAmp, outDirection


    acousticImpedance = vp * density

    zSurface = mostNegativeCube[2] + nCubes[2]*rCube
    isUnderZSurface = lambda iRay, jRay: location(iRay, jRay)[2] < zSurface
    isInsideXGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[0] >= mostNegativeCube[0] and\
        location(iRay, jRay)[0] < mostNegativeCube[0] + nCubes[0]*rCube
    isInsideYGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[1] >= mostNegativeCube[1] and\
        location(iRay, jRay)[1] < mostNegativeCube[1] + nCubes[1]*rCube
    isInsideGrid = lambda iRay, jRay:\
        isUnderZSurface(iRay, jRay) and isInsideXGrid(iRay, jRay) and isInsideYGrid(iRay, jRay)
    #print("direction(28, 72)=",direction(28, 72))
    #raise ValueError()
    for iRay in range(2*nTheta-1):
        #print("Ray ", iRay*(2*nPhi-1), " from ", (2*nTheta-1))
        for jRay in range(2*nPhi-1):
            #print(iRay, jRay, " Ray ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            
            while isInsideGrid(iRay, jRay) and not isLost(iRay,jRay):
                #print("direction(iRay, jRay)=",direction(iRay, jRay))

                if np.dot(direction(iRay,jRay), normals[4]) <= 0:
                    #print("RayLost: Full reflection!")
                    rayLost(iRay, jRay)

                #Trace ray through heterogeneous environment
                planes = closestPlanes(location(iRay,jRay))
                distanceToPlane = 1e100
                iIntersectPlane = 0
                lengthNormalToPlane = 0
                for iNormal in range(normals.shape[0]):
                    if np.dot(direction(iRay,jRay), normals[iNormal]) > 0:
                        curLengthNormalToPlane = np.abs(location(iRay,jRay)[idxsD[iNormal]] - planes[iNormal])
                        cosRayPlane = np.dot(direction(iRay,jRay), normals[iNormal])
                        curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                        if curDistanceToPlane < distanceToPlane:
                            distanceToPlane = curDistanceToPlane
                            iIntersectPlane = iNormal
                            lengthNormalToPlane = curLengthNormalToPlane
                stepsToIntersect = np.abs(lengthNormalToPlane / direction(iRay,jRay)[idxsD[iIntersectPlane]])
                #print("stepsToIntersect=",stepsToIntersect)
                #print("direction(iRay, jRay)=",direction(iRay, jRay))
                updLocation  = location(iRay, jRay) + (stepsToIntersect + 1e-7)*direction(iRay, jRay)
                prevLocation = np.copy(location(iRay, jRay))
                setLocation(iRay, jRay, updLocation)
                addDistance(iRay, jRay, npl.norm(prevLocation - updLocation))

                #Time update
                time = npl.norm(updLocation - prevLocation)/vp[cubeIndex(prevLocation)]
                addTime(iRay,jRay, time)

                #Snell's law
                if isInsideGrid(iRay, jRay):
                    refractionIndexFrom = acousticImpedance[cubeIndex(prevLocation)]
                    refractionIndexTo   = acousticImpedance[cubeIndex(updLocation )]
                    incidenceSin  = np.sqrt(1 - np.dot(direction(iRay, jRay), normals[iIntersectPlane])**2)
                    if refractionIndexTo*incidenceSin > refractionIndexFrom:
                        rayLost(iRay, jRay)
                    else:
                        refractionSin = refractionIndexTo/refractionIndexFrom * incidenceSin
                        refractionCos = np.sqrt(1 - refractionSin**2)
                        updDirection = refractionsDirection(refractionCos, direction(iRay, jRay), iIntersectPlane)
                        setDirection(iRay, jRay, updDirection)
                #Fresnel's law
                        incidenceCos = np.sqrt(1 - incidenceSin**2)
                        fresnelCoeff = 2*refractionIndexTo*incidenceCos / \
                            (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos)
                        updAmplitude = scaleAmplitude(iRay,jRay,fresnelCoeff)


                #Debug
                for indTrace in range(traces):
                    if iRay == iTrace[indTrace] and jRay == jTrace[indTrace]:
                        path[indTrace,nPath[indTrace],:] = updLocation
                        pathAmps[indTrace,nPath[indTrace]] = updAmplitude
                        pathTimes[indTrace] += time
                        nPath[indTrace] += 1

    #print("TRIANGLE AMPLITUDE NORM IS DISABLED!")
    triNorms = np.zeros((2*nTheta-1,2*nPhi-1))
    addToLost = []
    for iRay in range(2*nTheta-1):
        for jRay in range(2*nPhi-1):
            #print("Triangle amplitude update ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #Triangle amplitude update
            if not isLost(iRay, jRay) and not isUnderZSurface(iRay, jRay):
                #isNormExist, rayNorm = triangleAmplitudeNormReverse(iRay,jRay)
                isNormExist, rayNorm = distanceNorm(iRay,jRay)
                if isNormExist:
                   scaleAmplitude(iRay,jRay,1/rayNorm)
                   #print("triNorm:", rayNorm)
                   triNorms[iRay, jRay] = rayNorm
                else:
                   addToLost.append((iRay,jRay))  

    for ray in addToLost:
        rayLost(*ray)

    #get signal for receivers
    receiversTimes = np.zeros(receiversCoordinates.shape[0])
    receiversAmps  = np.zeros(receiversCoordinates.shape[0])
    receiversTensorAmps = np.zeros((receiversCoordinates.shape[0],3,6))
    distancesToRays = np.zeros(receiversCoordinates.shape[0])
    for i in range(receiversCoordinates.shape[0]):
        #print("Receiver ", i, " from ", receiversCoordinates.shape[0])
        curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(receiversCoordinates[i,:])
        receiversTimes[i] = curTime
        receiversAmps[i]  = curAmplitude
        distancesToRays[i] = distanceToRay

        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])
        receiversTensorAmps[i,0,0] =       6*xCos**3          - 3*xCos
        receiversTensorAmps[i,1,0] =       6*(xCos**2)*yCos   -   yCos
        receiversTensorAmps[i,2,0] =       6*(xCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,1] =       6*xCos*(yCos**2)   -   xCos
        receiversTensorAmps[i,1,1] =       6*(yCos**3)        - 3*yCos
        receiversTensorAmps[i,2,1] =       6*(yCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,2] =       6*xCos*(zCos**2)   -   xCos
        receiversTensorAmps[i,1,2] =       6*(yCos)*(zCos**2) -   yCos
        receiversTensorAmps[i,2,2] =       6*(zCos**3)        - 3*zCos

        receiversTensorAmps[i,0,3] =       6*(xCos**2)*(yCos) -   yCos
        receiversTensorAmps[i,1,3] =       6*(yCos**2)*(xCos) -   xCos
        receiversTensorAmps[i,2,3] =       6*xCos*yCos*(zCos)

        receiversTensorAmps[i,0,4] = (-1)*(6*(xCos**2)*(zCos) -   zCos)
        receiversTensorAmps[i,1,4] = (-1)*(6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,2,4] = (-1)*(6*(zCos**2)*xCos   -   xCos)

        receiversTensorAmps[i,0,5] = (-1)*(6*xCos*yCos*(zCos)         ) 
        receiversTensorAmps[i,1,5] = (-1)*(6*(yCos**2)*(zCos)  -  zCos)
        receiversTensorAmps[i,2,5] = (-1)*(6*(zCos**2)*yCos    -  yCos)

    gaussWavelet = lambda mean, var: lambda x:\
              -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
    gaussWavelet2 = lambda mean, var: lambda x:\
          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))
                sigma=1/fdPWave/5
                length = receiverSignals.shape[3]

                gwt = gaussWavelet2(waveComing/fd,sigma)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt
                #plt.plot(receiverSignals[iR, iCh, iTensor, :])
                #plt.plot(np.arange(0,gwt.shape[0])*(fd/gwt.shape[0]), np.abs(np.fft.fft(gwt)))
                #plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1

    print("should be second order")
    return receiverSignals, distancesToRays, rays, path, pathAmps, pathTimes,  nPath, initialArrangement

def rayTracerDirect(
        receiversCoordinates # (number of receivers, 2)
       ,fd
       ,fdPWave
       ,timeLength
       ,nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,startRaysLocation
       ,initialAmplitude
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,maxSteps
       ,tolerance
       ,iTrace   = 0
       ):
    print("Start Ray Tracer")

    ###Initial setup
    eps = 1e-9
    #rays = (i, time, x, y, z, dirX, dirY, dirZ, isLost, amplitude, distance, initDirX, initDirY, initDirZ)
    rays = np.zeros((receiversCoordinates.shape[0],13))
    print(rays.shape)
    nPars = rays.shape[1]

    ###Utils definition
    def setLocation(i, location):
        rays[i,1:4] = location
    location = lambda i: rays[i,1:4]

    def addDistance(i,distance):
        rays[i,9] += distance
    distance = lambda i: rays[i,9]
    distances = lambda: rays[:,9]

    def setDirection(i, direction):
        rays[i,4:7] = direction
    direction = lambda i: rays[i,4:7]

    def setInitDirection(i, direction):
        rays[i,10:13] = direction
        rays[i,4:7] = direction
    initDirection = lambda i: rays[i,10:13]

    def scaleLocation(s):
        rays[:,1:4] *= s

    def shiftAllLocations(v):
        rays[:,1:4] += np.array(v)

    def setLocToDir():
        rays[:,4:7] = rays[:,1:4]

    def rayLost(i):
        rays[i,7] = 1
    isLost = lambda i: rays[i, 7] == 1

    def cubeIndex(loc):
        x = int((loc[0] - mostNegativeCube[0])) // rCube
        y = int((loc[1] - mostNegativeCube[1])) // rCube
        z = int((loc[2] - mostNegativeCube[2])) // rCube
        #if x < 0 or y < 0 or z < 0:
        #    raise ValueError("mostNegativeCube: ", mostNegativeCube, " Location of power: ", loc )
        return x,y,z

    def setAmplitudes(amp):
        rays[:,8] = amp

    def setAmplitude(iRay, amp):
        rays[iRay,8] = amp

    def scaleAmplitude(i, scale):
        if scale < 0:
            raise ValueError("TRYING TO REVERSE AMPLITUDE!")
        rays[i,8] *= scale
        return rays[i,8]
    amplitude = lambda i: rays[i, 8]
    amplitudes = lambda: rays[:,8]

    def addTime(i, t):
        rays[i,0] += t
        return rays[i,0]
    rayTime = lambda i: rays[i,0]

    times = lambda: rays[:,0]

    ###Trace debug setup
    iTrace = np.array(iTrace)
    print(iTrace)
    traces = iTrace.shape[0]
    path     = np.zeros((traces,10000,3))
    pathAmps = np.zeros((traces,10000))
    pathTimes = np.zeros((traces))
    for i in range(traces):
        path[i,0,:] = location(iTrace[i])
        pathAmps[i,0] = 1
    nPath = np.ones(traces, dtype=np.int)

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])
    def refractionsDirection(refractionCos, direction, iIntersectPlane):
        #According to Snell's law
        updDirection = np.zeros(3)
        if np.abs(direction[2] - 1) < eps and iIntersectPlane == 4:
            return direction
        sign = normals[iIntersectPlane][idxsD[iIntersectPlane]]
        refractionX = 0
        refractionY = 0
        refractionZ = 0
        signF = lambda x: x/np.abs(x)
        if iIntersectPlane < 2:
            refractionX = sign*refractionCos
            if np.abs(direction[1]) < eps:
                refractionY = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionX**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionX**2)
            else:
                refractionYZrel = direction[2]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionX**2) / (1 + refractionYZrel**2))
                refractionZ = refractionY*refractionYZrel
        elif iIntersectPlane < 4:
            refractionY = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionY**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionY**2)
            else:
                refractionZXrel = direction[0]/direction[2]
                refractionZ = signF(direction[2]) * np.sqrt((1 - refractionY**2) / (1 + refractionZXrel**2))
                refractionX = refractionZ * refractionZXrel
        else:
            refractionZ = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionZ**2)
            elif np.abs(direction[1]) < eps:
                refractionY = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionZ**2)
            else:
                refractionYXrel = direction[0]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionZ**2) / (1 + refractionYXrel**2))
                refractionX = refractionYXrel * refractionY

        updDirection = np.array([refractionX, refractionY, refractionZ])
        return updDirection/npl.norm(updDirection)


    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    def triangleAreaBy3Sides(a, b, c):
        halfPerimeter = (a+b+c)/2
        return np.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c))

    def triangleAreaBy3Points(A, B, C):
        a = npl.norm(A-B)
        b = npl.norm(C-B)
        c = npl.norm(C-A)
        return triangleAreaBy3Sides(a, b, c)

    def triangleAmplitudeNorm(iCurRay, jCurRay):
        quarter1 = (None, 1e111)
        quarter2 = (None, 1e111)
        quarter3 = (None, 1e111)
        quarter4 = (None, 1e111)

        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isUnderZSurface(iRay, jRay) and not isLost(iRay,jRay):
                    distV = location(iCurRay, jCurRay) - location(iRay, jRay)
                    distance = npl.norm(distV)
                    if distance > 1e-7:
                        if distV[0] >= 0 and distV[1]  < 0 and distance < quarter1[1]:
                            quarter1 = ((iRay, jRay), distance)
                        if distV[0] >= 0 and distV[1] >= 0 and distance < quarter2[1]:
                            quarter2 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1]  < 0 and distance < quarter3[1]:
                            quarter3 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1] >= 0 and distance < quarter4[1]:
                            quarter4 = ((iRay, jRay), distance)

        closestRays = list(filter(lambda x: x[0] != None, [quarter1, quarter2, quarter3, quarter4]))

        if len(closestRays) < 1:
            raise ValueError("There is no triangles are found!")

        finishAreas = []
        startAreas  = []
        for i in range(len(closestRays)):
            finishDist = npl.norm(location(*closestRays[i-1][0]) - location(*closestRays[i][0]))
            finishAreas.append(triangleAreaBy3Sides(closestRays[i-1][1], closestRays[i][1], finishDist))
            startAreas.append(
                triangleAreaBy3Points(
                    initialLocation(iCurRay,jCurRay), 
                    initialLocation(*closestRays[i-1][0]), 
                    initialLocation(*closestRays[i][0])
                    ))

    #Triangle norm where we take triangles on the start unit sphere and then try to find their images
    def triangleAmplitudeNormReverse(iCurRay, jCurRay):

        finishAreas = []
        startAreas  = []
        startAreasAppend = lambda iR0, jR0, iR1, jR1: \
            startAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(initialLocation(iR0,jR0) - initialLocation(iR1,jR0))
                 ,npl.norm(initialLocation(iR1,jR0) - initialLocation(iR0,jR1))
                 ,npl.norm(initialLocation(iR0,jR0) - initialLocation(iR0,jR1))
                 ))
        finishAreasAppend = lambda iR0, jR0, iR1, jR1: \
            finishAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(location(iR0,jR0) - location(iR1,jR0))
                 ,npl.norm(location(iR1,jR0) - location(iR0,jR1))
                 ,npl.norm(location(iR0,jR0) - location(iR0,jR1))
                 ))
        areasAppend = lambda iR, jR, iR1, jR1: \
             startAreasAppend(iR, jR, iR1, jR1) == \
             finishAreasAppend(iR, jR, iR1, jR1)
        
        isLost0 = lambda iR, jR: isLost(iR,jR) and (not isUnderZSurface(iR,jR))

        isNotLost = lambda iR, jR, iR0, jR0: \
                not (isLost0(iR,jR) or isLost0(iR0,jR0) or isLost0(iR,jR0) or isLost0(iR0,jR))

        if iCurRay+1 < 2*nTheta-1 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay-1)
        if iCurRay+1 < 2*nTheta-1 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay-1)

        if len(startAreas) < 3:
            return False, None
        #print("Areas:", np.array(finishAreas), " ", np.array(startAreas))
        
        return True, np.sqrt(np.median(np.array(finishAreas)) / np.median(np.array(startAreas)))

    def approxParamsByClosestRay(point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
        minD = dists[curMinimumIdx]
        minId = curMinimumIdx

        outTime = rayTime(*minId)
        outAmp  = amplitude(*minId)
        outDirection = initialDirection(*minId)

        return outTime, outAmp, outDirection, minD

    def distanceNorm(i):
        return True, distance(i)

    def approxParamsByClosestRays(n, point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        mins    = []
        minsD   = np.zeros(n)
        weights = np.zeros(n)
        for i in range(n):
            curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
            minsD[i] = dists[curMinimumIdx]
            mins.append(curMinimumIdx)
            dists[curMinimumIdx] = 1e111
            weights[i] = scs.multivariate_normal.pdf(point, location(*curMinimumIdx)[:2], np.eye(2)*minsD[0])


        outTime = 0
        outAmp  = 0
        outDirection = np.zeros(3)
        for i in range(n):
            outTime += (weights[i]/np.sum(weights)) * rayTime(*mins[i])
            outAmp  += (weights[i]/np.sum(weights)) * amplitude(*mins[i])
            outDirection += (weights[i]/np.sum(weights)) * direction(*mins[i])

        return outTime, outAmp, outDirection


    acousticImpedance = vp * density

    zSurface = mostNegativeCube[2] + nCubes[2]*rCube
    isUnderZSurface = lambda i: location(i)[2] < zSurface
    isInsideXGrid   = lambda i:\
        location(i)[0] >= mostNegativeCube[0] and\
        location(i)[0] < mostNegativeCube[0] + nCubes[0]*rCube
    isInsideYGrid   = lambda i:\
        location(i)[1] >= mostNegativeCube[1] and\
        location(i)[1] < mostNegativeCube[1] + nCubes[1]*rCube
    isInsideGrid = lambda i:\
        isUnderZSurface(i) and isInsideXGrid(i) and isInsideYGrid(i)
    #print("direction(28, 72)=",direction(28, 72))
    #raise ValueError()
    def traceRay(iRay):
        #print("Ray ", iRay*(2*nPhi-1), " from ", (2*nTheta-1))
        #print(iRay, jRay, " Ray ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
        while isInsideGrid(iRay) and not isLost(iRay):
            #print("direction(iRay, jRay)=",direction(iRay, jRay))

            if np.dot(direction(iRay), normals[4]) <= 0:
                print("RayLost: Full reflection!")
                rayLost(iRay)

            #Trace ray through heterogeneous environment
            planes = closestPlanes(location(iRay))
            distanceToPlane = 1e100
            iIntersectPlane = 0
            lengthNormalToPlane = 0
            for iNormal in range(normals.shape[0]):
                if np.dot(direction(iRay), normals[iNormal]) > 0:
                    curLengthNormalToPlane = np.abs(location(iRay)[idxsD[iNormal]] - planes[iNormal])
                    cosRayPlane = np.dot(direction(iRay), normals[iNormal])
                    curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                    if curDistanceToPlane < distanceToPlane:
                        distanceToPlane = curDistanceToPlane
                        iIntersectPlane = iNormal
                        lengthNormalToPlane = curLengthNormalToPlane
            stepsToIntersect = np.abs(lengthNormalToPlane / direction(iRay)[idxsD[iIntersectPlane]])
            #print("stepsToIntersect=",stepsToIntersect)
            #print("direction(iRay, jRay)=",direction(iRay, jRay))
            updLocation  = location(iRay) + (stepsToIntersect + 1e-7)*direction(iRay)
            prevLocation = np.copy(location(iRay))
            setLocation(iRay, updLocation)
            addDistance(iRay, npl.norm(prevLocation - updLocation))

            #Time update
            time = npl.norm(updLocation - prevLocation)/vp[cubeIndex(prevLocation)]
            addTime(iRay, time)

            #Snell's law
            if isInsideGrid(iRay):
                refractionIndexFrom = acousticImpedance[cubeIndex(prevLocation)]
                refractionIndexTo   = acousticImpedance[cubeIndex(updLocation )]
                incidenceSin  = np.sqrt(1 - np.dot(direction(iRay), normals[iIntersectPlane])**2)
                if refractionIndexTo*incidenceSin > refractionIndexFrom:
                    #print("SNELL's LAW LOST")
                    rayLost(iRay)
                    #print("LocationOfLost: ",location(iRay))
                    #print("from:", prevLocation, acousticImpedance[cubeIndex(prevLocation)])
                    #print("to:", updLocation, acousticImpedance[cubeIndex(updLocation)])
                    #print("iIntersectPlane:", normals[iIntersectPlane])
                    #print("Direction:", direction(iRay))
                    #print("incidenceSin:", incidenceSin)
                else:
                    refractionSin = refractionIndexTo/refractionIndexFrom * incidenceSin
                    refractionCos = np.sqrt(1 - refractionSin**2)
                    updDirection = refractionsDirection(refractionCos, direction(iRay), iIntersectPlane)
                    setDirection(iRay, updDirection)
            #Fresnel's law
                    incidenceCos = np.sqrt(1 - incidenceSin**2)
                    fresnelCoeff = 2*refractionIndexTo*incidenceCos / \
                        (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos)
                    updAmplitude = scaleAmplitude(iRay,fresnelCoeff)


            #Debug
        #for indTrace in range(traces):
        #    if iRay == iTrace[indTrace]:
        #        path[indTrace,nPath[indTrace],:] = updLocation
        #        pathAmps[indTrace,nPath[indTrace]] = updAmplitude
        #        pathTimes[indTrace] += time
        #        nPath[indTrace] += 1
        if isUnderZSurface(iRay):
            #print("underZSurface!")
            rayLost(iRay)

        return

    #Initialize rays
    setAmplitudes(initialAmplitude)
    shiftAllLocations(startRaysLocation)
    print("amp", amplitudes()) 

    #for iRay in range(receiversCoordinates.shape[0]):
    #    print("RAY",iRay)
    #    goal = np.zeros(3)
    #    goal[:2] = receiversCoordinates[iRay,:]
    #    print("goal:", goal)
    #    initialVector = goal - location(iRay)
    #    initialDirection = initialVector/npl.norm(initialVector)
    #    setInitDirection(iRay,initialDirection)
    #    print("START: ", receiversCoordinates[iRay,:], startRaysLocation, direction(iRay), location(iRay))

    #    nOfSteps = 0
    #    distanceToReceiver = 1e111
    #    bestRay = np.zeros((10))
    #    while tolerance < distanceToReceiver and maxSteps > nOfSteps:
    #        print(nOfSteps, tolerance, distanceToReceiver)
    #        traceRay(iRay)
    #        if isLost(iRay) and nOfSteps == 0:
    #            print("instant lost")
    #            rays[iRay,:] = 0
    #            setInitDirection(iRay, np.array([0,0,1]))
    #            setLocation(iRay, startRaysLocation)
    #        else:
    #            nOfSteps += 1
    #            curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
    #            print("curDistance", curDistanceToReceiver)
    #            if isLost(iRay) or curDistanceToReceiver >= distanceToReceiver:
    #                if isLost(iRay):
    #                    print("lost")
    #                else:
    #                    print("distance")
    #                randDist = np.random.rand(1)
    #                randAngle = 2*np.pi*np.random.rand(1)
    #                randShift = np.array([np.sin(randAngle), np.cos(randAngle), 0]) * randDist * distanceToReceiver
    #                newDirectionVector = (goal + randShift) - startRaysLocation
    #                rays[iRay,:] = 0
    #                setInitDirection(iRay, newDirectionVector/npl.norm(newDirectionVector))
    #                setLocation(iRay,startRaysLocation)
    #            else:
    #                print("success")
    #                distanceToReceiver = curDistanceToReceiver
    #                bestRay = np.copy(rays[iRay,:])
    #                newGoal = np.zeros(3)
    #                newGoal[:2] = goal[:2] - 0.5*(goal[:2] - location(iRay)[:2])
    #                newDirectionVector = newGoal - startRaysLocation
    #                print("newDirectionVector", isLost(iRay), (newGoal), startRaysLocation, location(iRay))
    #                rays[iRay,:] = 0
    #                newDirection = newDirectionVector/npl.norm(newDirectionVector)
    #                setInitDirection(iRay, newDirection)
    #                setLocation(iRay, startRaysLocation)

    for iRay in range(receiversCoordinates.shape[0]):
        print("RAY",iRay)
        scalingStep = 0.1
        lGoal = np.zeros(3)
        rGoal = np.zeros(3)
        lGoal[:2] = startRaysLocation[:2]
        rGoal[:2] = receiversCoordinates[iRay,:2] 
        rGoal += rGoal - lGoal
        goal = lambda l, r: r + 0.5*(l - r)
        initialVector = goal(lGoal, rGoal) - startRaysLocation
        initialDirection = initialVector/npl.norm(initialVector)
        setInitDirection(iRay,initialDirection)

        nOfSteps = 0
        distanceToReceiver = 1e111
        bestRay = np.zeros((13))
        prevLGoal = lGoal
        print(nOfSteps, distanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
        history = ['L','L','L']
        historyGoals = []
        def historyUpdate(x):
            history[:2] = history[1:3]
            history[2] = x
        while tolerance < distanceToReceiver and maxSteps > nOfSteps:
            traceRay(iRay)
            nOfSteps += 1
            curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
            if history == ["S","F","F"]:
                scalingStep /= 7
                lGoal, rGoal = historyGoals[-1]
            if (isLost(iRay) or curDistanceToReceiver > distanceToReceiver) and history != ["S","F","F"] :
                historyUpdate("F")
                #if isLost(iRay):
                #    print(nOfSteps, "L", curDistanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
                #else:
                #    print(nOfSteps, "F", curDistanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
                lGoal = prevLGoal
                rGoal = rGoal + scalingStep*(lGoal-rGoal)
                initialVector = goal(lGoal, rGoal) - startRaysLocation
                initialDirection = initialVector/npl.norm(initialVector)
                rays[iRay,:] = 0
                setAmplitude(iRay, initialAmplitude)
                setInitDirection(iRay, initialDirection)
                setLocation(iRay, startRaysLocation)
            else:
                historyUpdate("S")
                historyGoals.append((lGoal, rGoal))
                #print("curDistance", curDistanceToReceiver)
                #print("success")
                #print(nOfSteps, "success", curDistanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
                distanceToReceiver = curDistanceToReceiver
                bestRay = np.copy(rays[iRay,:])
                prevLGoal = lGoal
                lGoal = lGoal + scalingStep*(rGoal-lGoal)
                initialVector = goal(lGoal, rGoal) - startRaysLocation
                initialDirection = initialVector/npl.norm(initialVector)
                rays[iRay,:] = 0
                setAmplitude(iRay, initialAmplitude)
                setInitDirection(iRay, initialDirection)
                setLocation(iRay, startRaysLocation)

        rays[iRay,:] = np.copy(bestRay)
        #print("bestRay:", bestRay)
        print("distanceToreceiver", distanceToReceiver)
        print(rayTime(iRay))

        if distanceToReceiver > 100:
            nSteps = 50
            dirVector = receiversCoordinates[iRay,:] - startRaysLocation[:2]
            dirVector = dirVector/nSteps 
            for iStep in range(2*nSteps):
                goal = np.zeros(3)
                goal[:2] = startRaysLocation[:2] + dirVector*iStep
                initialVector = goal - startRaysLocation
                initialDirection = initialVector/npl.norm(initialVector)
                rays[iRay,:] = 0
                setAmplitude(iRay, initialAmplitude)
                setInitDirection(iRay, initialDirection)
                setLocation(iRay, startRaysLocation)
                traceRay(iRay)
                curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
                if (not isLost(iRay)) and curDistanceToReceiver < distanceToReceiver:
                    distanceToReceiver = curDistanceToReceiver
                    bestRay = np.copy(rays[iRay,:])
        rays[iRay,:] = np.copy(bestRay)
        print("distanceToreceiver", distanceToReceiver)
        print(rayTime(iRay))

    #initialArrangement   = np.copy(rays[:,1:4])
    #initialArrangement2D = np.copy(rays[:,1:4])
    #initialDirection2D   = np.copy(rays[:,4:7])

    #print("TRIANGLE AMPLITUDE NORM IS DISABLED!")
    triNorms = np.zeros((receiversCoordinates.shape[0]))
    addToLost = []
    def normByDistance(iRay):
            #print("Triangle amplitude update ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #Triangle amplitude update
            isNormExist, rayNorm = distanceNorm(iRay)
            if isNormExist:
               scaleAmplitude(iRay,1/rayNorm)
               #print("triNorm:", rayNorm)
               triNorms[iRay] = rayNorm

    for iRay in range(receiversCoordinates.shape[0]):
        normByDistance(iRay)

    #get signal for receivers
    receiversTimes = times()
    receiversAmps  = amplitudes()
    receiversTensorAmps = np.zeros((receiversCoordinates.shape[0],3,6))
    distancesToRays = np.zeros(receiversCoordinates.shape[0])
    for i in range(receiversCoordinates.shape[0]):
        #print("Receiver ", i, " from ", receiversCoordinates.shape[0])
        #curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(receiversCoordinates[i,:])
        #receiversTimes[i] = time(iRay)
        #receiversAmps[i]  = curAmplitude
        distancesToRays[i] = npl.norm(receiversCoordinates[i,:] - location(i)[:2])
        curDirection = initDirection(i)

        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])
        receiversTensorAmps[i,0,0] =       6*xCos**3          - 3*xCos
        receiversTensorAmps[i,1,0] =       6*(xCos**2)*yCos   -   yCos
        receiversTensorAmps[i,2,0] =       6*(xCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,1] =       6*xCos*(yCos**2)   -   xCos
        receiversTensorAmps[i,1,1] =       6*(yCos**3)        - 3*yCos
        receiversTensorAmps[i,2,1] =       6*(yCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,2] =       6*xCos*(zCos**2)   -   xCos
        receiversTensorAmps[i,1,2] =       6*(yCos)*(zCos**2) -   yCos
        receiversTensorAmps[i,2,2] =       6*(zCos**3)        - 3*zCos

        receiversTensorAmps[i,0,3] =       6*(xCos**2)*(yCos) -   yCos
        receiversTensorAmps[i,1,3] =       6*(yCos**2)*(xCos) -   xCos
        receiversTensorAmps[i,2,3] =       6*xCos*yCos*(zCos)

        receiversTensorAmps[i,0,4] = (-1)*(6*(xCos**2)*(zCos) -   zCos)
        receiversTensorAmps[i,1,4] = (-1)*(6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,2,4] = (-1)*(6*(zCos**2)*xCos   -   xCos)

        receiversTensorAmps[i,0,5] = (-1)*(6*xCos*yCos*(zCos)         ) 
        receiversTensorAmps[i,1,5] = (-1)*(6*(yCos**2)*(zCos)  -  zCos)
        receiversTensorAmps[i,2,5] = (-1)*(6*(zCos**2)*yCos    -  yCos)

    gaussWavelet = lambda mean, var: lambda x:\
              -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
    gaussWavelet2 = lambda mean, var: lambda x:\
          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))
                sigma=1/fdPWave/5
                length = receiverSignals.shape[3]

                gwt = gaussWavelet2(waveComing/fd,sigma)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt

                #plt.plot(receiverSignals[iR, iCh, iTensor, :])
                #plt.plot(np.arange(0,gwt.shape[0])*(fd/gwt.shape[0]), np.abs(np.fft.fft(gwt)))
                #plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1

    print("should be second order")
    return receiverSignals, distancesToRays, rays, path, pathAmps, pathTimes, nPath

bfj.load_inputs(globals())

startTime = time.time()

nXNodes, nYNodes, nZNodes = nCellsX, nCellsY, nCellsZ
 
nodeXSize = cellLengthX

rCube  = nodeXSize
mostNegativeCube = np.array([0, 0, -nCubesShape[2]*rCube])

xPower = xPower * rCube
yPower = yPower * rCube
startRaysLocation = np.array([xPower, yPower, -depth])

nRays = numberOfRays
startAngle = startAngle*np.pi
iTrace = []
jTrace = []

signalCoords[:,0] -= startBounds[0]
signalCoords[:,1] -= startBounds[1]
receiversCoordinates = signalCoords[:,:2] * rCube

print(time.time()-startTime, "s")
print("power:", xPower, yPower, -depth)
print("startRaysLocation:", startRaysLocation)
print("receiversCoordinatesFIRST",receiversCoordinates)
print("nCubesShape",nCubesShape)
print("rCube",rCube)
print("vp.shape",vp.shape)
print(vp[37,141,23], vp[37,41,23])
print(density[37,41,23])
print("density.shape",density.shape)
print("startRaysLocation",startRaysLocation)
#raise ValueError()
receivedSignals, distancesToRays, rays, path, pathAmps, pathTimes, nPath = \
    rayTracer(receiversCoordinates, fd, pWaveFrequency, timeLength, nCubesShape, rCube, vp, density, startRaysLocation, initialAmplitude,mostNegativeCube,30,3,iTrace)
#receivedSignals, distancesToRays, rays, path, pathAmps, pathTimes, nPath, initialArrangement = \
#    rayTracer(receiversCoordinates, fd, pWaveFrequency, timeLength, nCubesShape, rCube, vp, density, startRaysLocation, initialAmplitude,mostNegativeCube,nRays,nRays,startAngle,iTrace,jTrace)
print("ray tracer:", time.time()-startTime, "s")

print("receivedSignals.shape",receivedSignals.shape)

receivedSignals = receivedSignals.transpose(1,0,2,3).reshape(-1,receivedSignals.shape[3])
data = dict( 
           S = receivedSignals
          ,distancesToRays = distancesToRays 
          ,signalNames     = signalNames
          ,coordsOfMaxDistance = receiversCoordinates[np.argmax(distancesToRays),:]
           )
           
meta = dict(
           apX   = xPower
          ,apY   = yPower
          ,depth = depth
          ,modelFd = fd
          ,maxDistanceToRay  = round(np.amax(distancesToRays)*10)/10
          ,meanDistanceToRay = round(np.mean(distancesToRays)*10)/10
          ,startAngle        = startAngle/np.pi
          ,nRays             = nRays
          )
          
bfj.save_package(label="rayTracersModels",data=data, meta=meta)
