import numpy as np
import numpy.linalg as npl
import scipy.stats as scs
import math, random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time as timeUtils

from sympy import primefactors

from visualize import plotPath, plotMediumPath

def fibonacciSphere(samples=1,randomize=True):
    rnd = 1.
    if randomize:
        rnd = random.random() * samples

    points = []
    offset = 2./samples
    increment = math.pi * (3. - math.sqrt(5.));

    for i in range(samples):
        y = ((i * offset) - 1) + (offset / 2);
        r = math.sqrt(1 - pow(y,2))

        phi = ((i + rnd) % samples) * increment

        x = math.cos(phi) * r
        z = math.sin(phi) * r

        points.append([x,y,z])

    return np.array(points)

def rayTracer(
        receiversCoordinates # (number of receivers, 2)
       ,fd
       ,fdPWave
       ,timeLength
       ,nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,startRaysLocation
       ,initialAmplitude
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,nTheta           # number of theta steps in quarter
       ,nPhi
       ,startAngle = np.pi/10
       ,iTrace   = 0
       ,jTrace   = 0
       ):
    print("Start Ray Tracer")

    ###Initial setup
    eps = 1e-9
    startSphereRadius = rCube/1000
    #rays = (theta, phi, time, x, y, z, dirX, dirY, dirZ, isLost, amplitude)
    rays = np.zeros((2*nTheta-1,2*nPhi-1,9))
    distances = np.zeros((2*nTheta-1,2*nPhi-1))
    nPars = rays.shape[2]

    ###Utils definition
    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]
    initialLocation = lambda iTheta, iPhi: initialArrangement2D[iTheta,iPhi,:]

    def addDistance(iTheta, iPhi, distance):
        distances[iTheta,iPhi] += distance
    distance = lambda iTheta, iPhi: distances[iTheta, iPhi] 

    def shiftAllLocations(v):
        rays[:,:,1:4] += np.array(v)

    def setDirection(iTheta, iPhi, direction):
        rays[iTheta,iPhi,4:7] = direction
    direction = lambda iTheta, iPhi: rays[iTheta,iPhi,4:7]
    initialDirection = lambda iTheta, iPhi: initialDirection2D[iTheta,iPhi,:]

    def scaleLocation(s):
        rays[:,:,1:4] *= s
    def setLocToDir():
        rays[:,:,4:7] = rays[:,:,1:4]

    def rayLost(iTheta, iPhi):
        rays[iTheta, iPhi, 7] = 1
    isLost = lambda iTheta, iPhi: rays[iTheta, iPhi, 7] == 1

    def cubeIndex(loc):
        x = int((loc[0] - mostNegativeCube[0])) // rCube
        y = int((loc[1] - mostNegativeCube[1])) // rCube
        z = int((loc[2] - mostNegativeCube[2])) // rCube
        #if x < 0 or y < 0 or z < 0:
        #    raise ValueError("mostNegativeCube: ", mostNegativeCube, " Location of power: ", loc )
        return x,y,z

    def setAmplitude(amp):
        rays[:,:,8] = amp
    def scaleAmplitude(iTheta, iPhi, scale):
        rays[iTheta,iPhi,8] *= scale
        return rays[iTheta,iPhi,8]
    amplitude = lambda iTheta, iPhi: rays[iTheta, iPhi, 8]

    def addTime(iTheta, iPhi, t):
        rays[iTheta,iPhi,0] += t
        return rays[iTheta,iPhi,0]
    rayTime = lambda iTheta, iPhi: rays[iTheta,iPhi,0]

    times = lambda rays: rays[:,:,0]

    ###

    #Initialize rays
    setAmplitude(initialAmplitude)

    thetaSpace = np.linspace(startAngle,np.pi/2,nTheta)
    phiSpace   = np.linspace(startAngle,np.pi/2,nPhi  )

    for iTheta in range(nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta, iPhi, np.array([np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)]) )

    rays[nTheta:2*nTheta-1,:,:] = np.copy(rays[:nTheta-1,:,:])
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = np.copy(rays[:,:nPhi-1,:])
    rays[:,nPhi:2*nPhi-1,2] *= -1

    setLocToDir()

    scaleLocation(startSphereRadius)

    shiftAllLocations(startRaysLocation)

    initialArrangement   = np.copy(rays.reshape(-1,nPars)[:,1:4])
    initialArrangement2D = np.copy(rays[:,:,1:4])
    initialDirection2D = np.copy(rays[:,:,4:7])

    ###Trace debug setup
    iTrace = np.array(iTrace)
    jTrace = np.array(jTrace)
    traces = iTrace.shape[0]
    path     = np.zeros((traces,10000,3))
    pathAmps = np.zeros((traces,10000))
    pathTimes = np.zeros((traces))
    for i in range(traces):
        path[i,0,:] = location(iTrace[i],jTrace[i])
        pathAmps[i,0] = 1
    nPath = np.ones(traces, dtype=np.int)

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])
    def refractionsDirection(refractionCos, direction, iIntersectPlane):
        #According to Snell's law
        updDirection = np.zeros(3)
        if np.abs(direction[2] - 1) < eps and iIntersectPlane == 4:
            return direction
        sign = normals[iIntersectPlane][idxsD[iIntersectPlane]]
        refractionX = 0
        refractionY = 0
        refractionZ = 0
        signF = lambda x: x/np.abs(x)
        if iIntersectPlane < 2:
            refractionX = sign*refractionCos
            if np.abs(direction[1]) < eps:
                refractionY = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionX**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionX**2)
            else:
                refractionYZrel = direction[2]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionX**2) / (1 + refractionYZrel**2))
                refractionZ = refractionY*refractionYZrel
        elif iIntersectPlane < 4:
            refractionY = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionY**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionY**2)
            else:
                refractionZXrel = direction[0]/direction[2]
                refractionZ = signF(direction[2]) * np.sqrt((1 - refractionY**2) / (1 + refractionZXrel**2))
                refractionX = refractionZ * refractionZXrel
        else:
            refractionZ = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionZ**2)
            elif np.abs(direction[1]) < eps:
                refractionY = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionZ**2)
            else:
                refractionYXrel = direction[0]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionZ**2) / (1 + refractionYXrel**2))
                refractionX = refractionYXrel * refractionY

        updDirection = np.array([refractionX, refractionY, refractionZ])
        return updDirection/npl.norm(updDirection)


    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    def triangleAreaBy3Sides(a, b, c):
        halfPerimeter = (a+b+c)/2
        return np.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c))

    def triangleAreaBy3Points(A, B, C):
        a = npl.norm(A-B)
        b = npl.norm(C-B)
        c = npl.norm(C-A)
        return triangleAreaBy3Sides(a, b, c)

    def triangleAmplitudeNorm(iCurRay, jCurRay):
        quarter1 = (None, 1e111)
        quarter2 = (None, 1e111)
        quarter3 = (None, 1e111)
        quarter4 = (None, 1e111)

        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isUnderZSurface(iRay, jRay) and not isLost(iRay,jRay):
                    distV = location(iCurRay, jCurRay) - location(iRay, jRay)
                    distance = npl.norm(distV)
                    if distance > 1e-7:
                        if distV[0] >= 0 and distV[1]  < 0 and distance < quarter1[1]:
                            quarter1 = ((iRay, jRay), distance)
                        if distV[0] >= 0 and distV[1] >= 0 and distance < quarter2[1]:
                            quarter2 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1]  < 0 and distance < quarter3[1]:
                            quarter3 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1] >= 0 and distance < quarter4[1]:
                            quarter4 = ((iRay, jRay), distance)

        closestRays = list(filter(lambda x: x[0] != None, [quarter1, quarter2, quarter3, quarter4]))

        if len(closestRays) < 1:
            raise ValueError("There is no triangles are found!")

        finishAreas = []
        startAreas  = []
        for i in range(len(closestRays)):
            finishDist = npl.norm(location(*closestRays[i-1][0]) - location(*closestRays[i][0]))
            finishAreas.append(triangleAreaBy3Sides(closestRays[i-1][1], closestRays[i][1], finishDist))
            startAreas.append(
                triangleAreaBy3Points(
                    initialLocation(iCurRay,jCurRay), 
                    initialLocation(*closestRays[i-1][0]), 
                    initialLocation(*closestRays[i][0])
                    ))

    #Triangle norm where we take triangles on the start unit sphere and then try to find their images
    def triangleAmplitudeNormReverse(iCurRay, jCurRay):

        finishAreas = []
        startAreas  = []
        startAreasAppend = lambda iR0, jR0, iR1, jR1: \
            startAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(initialLocation(iR0,jR0) - initialLocation(iR1,jR0))
                 ,npl.norm(initialLocation(iR1,jR0) - initialLocation(iR0,jR1))
                 ,npl.norm(initialLocation(iR0,jR0) - initialLocation(iR0,jR1))
                 ))
        finishAreasAppend = lambda iR0, jR0, iR1, jR1: \
            finishAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(location(iR0,jR0) - location(iR1,jR0))
                 ,npl.norm(location(iR1,jR0) - location(iR0,jR1))
                 ,npl.norm(location(iR0,jR0) - location(iR0,jR1))
                 ))
        areasAppend = lambda iR, jR, iR1, jR1: \
             startAreasAppend(iR, jR, iR1, jR1) == \
             finishAreasAppend(iR, jR, iR1, jR1)
        
        isLost0 = lambda iR, jR: isLost(iR,jR) and (not isUnderZSurface(iR,jR))

        isNotLost = lambda iR, jR, iR0, jR0: \
                not (isLost0(iR,jR) or isLost0(iR0,jR0) or isLost0(iR,jR0) or isLost0(iR0,jR))

        if iCurRay+1 < 2*nTheta-1 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay-1)
        if iCurRay+1 < 2*nTheta-1 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay-1)

        if len(startAreas) < 3:
            return False, None
        #print("Areas:", np.array(finishAreas), " ", np.array(startAreas))
        
        return True, np.sqrt(np.median(np.array(finishAreas)) / np.median(np.array(startAreas)))

    def distanceNorm(iCurRay, jCurRay):
        return True, distance(iCurRay, jCurRay)

    def approxParamsByClosestRay(point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
        minD = dists[curMinimumIdx]
        minId = curMinimumIdx

        outTime = rayTime(*minId)
        outAmp  = amplitude(*minId)
        outDirection = initialDirection(*minId)

        #print(minId)
        #print("triNorm: ", point, " ", triNorms[minId[0], minId[1]] )

        return outTime, outAmp, outDirection, minD

    def approxParamsByClosestRays(n, point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        mins    = []
        minsD   = np.zeros(n)
        weights = np.zeros(n)
        for i in range(n):
            curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
            minsD[i] = dists[curMinimumIdx]
            mins.append(curMinimumIdx)
            dists[curMinimumIdx] = 1e111
            weights[i] = scs.multivariate_normal.pdf(point, location(*curMinimumIdx)[:2], np.eye(2)*minsD[0])


        outTime = 0
        outAmp  = 0
        outDirection = np.zeros(3)
        for i in range(n):
            outTime += (weights[i]/np.sum(weights)) * rayTime(*mins[i])
            outAmp  += (weights[i]/np.sum(weights)) * amplitude(*mins[i])
            outDirection += (weights[i]/np.sum(weights)) * direction(*mins[i])

        return outTime, outAmp, outDirection


    acousticImpedance = vp * density

    zSurface = mostNegativeCube[2] + nCubes[2]*rCube
    isUnderZSurface = lambda iRay, jRay: location(iRay, jRay)[2] < zSurface
    isInsideXGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[0] >= mostNegativeCube[0] and\
        location(iRay, jRay)[0] < mostNegativeCube[0] + nCubes[0]*rCube
    isInsideYGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[1] >= mostNegativeCube[1] and\
        location(iRay, jRay)[1] < mostNegativeCube[1] + nCubes[1]*rCube
    isInsideGrid = lambda iRay, jRay:\
        isUnderZSurface(iRay, jRay) and isInsideXGrid(iRay, jRay) and isInsideYGrid(iRay, jRay)
    #print("direction(28, 72)=",direction(28, 72))
    #raise ValueError()
    for iRay in range(2*nTheta-1):
        if iRay % round(nTheta /3.5) == 7:
            print(iRay," Ray ", iRay, " from ", 2*nTheta-1)
        for jRay in range(2*nPhi-1):
            #print(iRay, jRay, " Ray ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #print("Ray ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            while isInsideGrid(iRay, jRay) and not isLost(iRay,jRay):
                #print("direction(iRay, jRay)=",direction(iRay, jRay))

                if np.dot(direction(iRay,jRay), normals[4]) <= 0:
                    #print("RayLost: Full reflection!")
                    rayLost(iRay, jRay)

                #Trace ray through heterogeneous environment
                planes = closestPlanes(location(iRay,jRay))
                distanceToPlane = 1e100
                iIntersectPlane = 0
                lengthNormalToPlane = 0
                for iNormal in range(normals.shape[0]):
                    if np.dot(direction(iRay,jRay), normals[iNormal]) > 0:
                        curLengthNormalToPlane = np.abs(location(iRay,jRay)[idxsD[iNormal]] - planes[iNormal])
                        cosRayPlane = np.dot(direction(iRay,jRay), normals[iNormal])
                        curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                        if curDistanceToPlane < distanceToPlane:
                            distanceToPlane = curDistanceToPlane
                            iIntersectPlane = iNormal
                            lengthNormalToPlane = curLengthNormalToPlane
                stepsToIntersect = np.abs(lengthNormalToPlane / direction(iRay,jRay)[idxsD[iIntersectPlane]])
                #print("stepsToIntersect=",stepsToIntersect)
                #print("direction(iRay, jRay)=",direction(iRay, jRay))
                updLocation  = location(iRay, jRay) + (stepsToIntersect + 1e-7)*direction(iRay, jRay)
                prevLocation = np.copy(location(iRay, jRay))
                setLocation(iRay, jRay, updLocation)
                addDistance(iRay, jRay, npl.norm(prevLocation - updLocation))

                #Time update
                time = npl.norm(updLocation - prevLocation)/vp[cubeIndex(prevLocation)]
                addTime(iRay,jRay, time)

                #Snell's law
                if isInsideGrid(iRay, jRay):
                    refractionIndexFrom = acousticImpedance[cubeIndex(prevLocation)]
                    refractionIndexTo   = acousticImpedance[cubeIndex(updLocation )]
                    incidenceSin  = np.sqrt(1 - np.dot(direction(iRay, jRay), normals[iIntersectPlane])**2)
                    if refractionIndexTo*incidenceSin > refractionIndexFrom:
                        rayLost(iRay, jRay)
                    else:
                        refractionSin = refractionIndexTo/refractionIndexFrom * incidenceSin
                        refractionCos = np.sqrt(1 - refractionSin**2)
                        updDirection = refractionsDirection(refractionCos, direction(iRay, jRay), iIntersectPlane)
                        setDirection(iRay, jRay, updDirection)
                #Fresnel's law
                        incidenceCos = np.sqrt(1 - incidenceSin**2)
                        fresnelCoeff = 2*refractionIndexTo*incidenceCos / \
                            (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos)
                        updAmplitude = scaleAmplitude(iRay,jRay,fresnelCoeff)


                #Debug
                for indTrace in range(traces):
                    if iRay == iTrace[indTrace] and jRay == jTrace[indTrace]:
                        path[indTrace,nPath[indTrace],:] = updLocation
                        pathAmps[indTrace,nPath[indTrace]] = updAmplitude
                        pathTimes[indTrace] += time
                        nPath[indTrace] += 1

    #print("TRIANGLE AMPLITUDE NORM IS DISABLED!")
    triNorms = np.zeros((2*nTheta-1,2*nPhi-1))
    addToLost = []
    for iRay in range(2*nTheta-1):
        for jRay in range(2*nPhi-1):
            #print("Triangle amplitude update ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #Triangle amplitude update
            if not isLost(iRay, jRay) and not isUnderZSurface(iRay, jRay):
                #isNormExist, rayNorm = triangleAmplitudeNormReverse(iRay,jRay)
                isNormExist, rayNorm = distanceNorm(iRay,jRay)
                if isNormExist:
                   scaleAmplitude(iRay,jRay,1/rayNorm)
                   #print("triNorm:", rayNorm)
                   triNorms[iRay, jRay] = rayNorm
                else:
                   addToLost.append((iRay,jRay))  

    for ray in addToLost:
        rayLost(*ray)

    #get signal for receivers
    receiversTimes = np.zeros(receiversCoordinates.shape[0])
    receiversAmps  = np.zeros(receiversCoordinates.shape[0])
    receiversTensorAmps = np.zeros((receiversCoordinates.shape[0],3,6))
    distancesToRays = np.zeros(receiversCoordinates.shape[0])
    for i in range(receiversCoordinates.shape[0]):
        #print("Receiver ", i, " from ", receiversCoordinates.shape[0])
        curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(receiversCoordinates[i,:])
        receiversTimes[i] = curTime
        receiversAmps[i]  = curAmplitude
        distancesToRays[i] = distanceToRay

        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])
        receiversTensorAmps[i,0,0] =       6*xCos**3          - 3*xCos
        receiversTensorAmps[i,1,0] =       6*(xCos**2)*yCos   -   yCos
        receiversTensorAmps[i,2,0] =       6*(xCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,1] =       6*xCos*(yCos**2)   -   xCos
        receiversTensorAmps[i,1,1] =       6*(yCos**3)        - 3*yCos
        receiversTensorAmps[i,2,1] =       6*(yCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,2] =       6*xCos*(zCos**2)   -   xCos
        receiversTensorAmps[i,1,2] =       6*(yCos)*(zCos**2) -   yCos
        receiversTensorAmps[i,2,2] =       6*(zCos**3)        - 3*zCos

        receiversTensorAmps[i,0,3] =       6*(xCos**2)*(yCos) -   yCos
        receiversTensorAmps[i,1,3] =       6*(yCos**2)*(xCos) -   xCos
        receiversTensorAmps[i,2,3] =       6*xCos*yCos*(zCos)

        receiversTensorAmps[i,0,4] = (-1)*(6*(xCos**2)*(zCos) -   zCos)
        receiversTensorAmps[i,1,4] = (-1)*(6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,2,4] = (-1)*(6*(zCos**2)*xCos   -   xCos)

        receiversTensorAmps[i,0,5] = (-1)*(6*xCos*yCos*(zCos)         ) 
        receiversTensorAmps[i,1,5] = (-1)*(6*(yCos**2)*(zCos)  -  zCos)
        receiversTensorAmps[i,2,5] = (-1)*(6*(zCos**2)*yCos    -  yCos)

    gaussWavelet = lambda mean, var: lambda x:\
              -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
    gaussWavelet2 = lambda mean, var: lambda x:\
          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))
                sigma=1/fdPWave/5
                length = receiverSignals.shape[3]

                gwt = gaussWavelet2(waveComing/fd,sigma)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt
                #plt.plot(receiverSignals[iR, iCh, iTensor, :])
                #plt.plot(np.arange(0,gwt.shape[0])*(fd/gwt.shape[0]), np.abs(np.fft.fft(gwt)))
                #plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1

    print("should be second order")
    return receiverSignals, distancesToRays, rays, path, pathAmps, pathTimes,  nPath, initialArrangement

def rayTracer1(
        receiversCoordinates # (number of receivers, 2)
       ,fd
       ,fdPWave
       ,timeLength
       ,nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,startRaysLocation
       ,initialAmplitude
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,nTheta           # number of theta steps in quarter
       ,nPhi
       ,startAngle = np.pi/10
       ,iTrace   = 0
       ,jTrace   = 0
       ):
    print("Start Ray Tracer")

    ###Initial setup
    eps = 1e-9
    startSphereRadius = rCube/1000
    #rays = (theta, phi, time, x, y, z, dirX, dirY, dirZ, isLost, amplitude)
    rays = np.zeros((2*nTheta-1,2*nPhi-1,9))
    distances = np.zeros((2*nTheta-1,2*nPhi-1))
    nPars = rays.shape[2]

    ###Utils definition
    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]
    initialLocation = lambda iTheta, iPhi: initialArrangement2D[iTheta,iPhi,:]

    def addDistance(iTheta, iPhi, distance):
        distances[iTheta,iPhi] += distance
    distance = lambda iTheta, iPhi: distances[iTheta, iPhi] 

    def shiftAllLocations(v):
        rays[:,:,1:4] += np.array(v)

    def setDirection(iTheta, iPhi, direction):
        rays[iTheta,iPhi,4:7] = direction
    direction = lambda iTheta, iPhi: rays[iTheta,iPhi,4:7]
    initialDirection = lambda iTheta, iPhi: initialDirection2D[iTheta,iPhi,:]

    def scaleLocation(s):
        rays[:,:,1:4] *= s
    def setLocToDir():
        rays[:,:,4:7] = rays[:,:,1:4]

    def rayLost(iTheta, iPhi):
        rays[iTheta, iPhi, 7] = 1
    isLost = lambda iTheta, iPhi: rays[iTheta, iPhi, 7] == 1

    def cubeIndex(loc):
        x = int((loc[0] - mostNegativeCube[0])) // rCube
        y = int((loc[1] - mostNegativeCube[1])) // rCube
        z = int(-mostNegativeCube[2]-(loc[2] - mostNegativeCube[2])) // rCube
        #if x < 0 or y < 0 or z < 0:
        #    raise ValueError("mostNegativeCube: ", mostNegativeCube, " Location of power: ", loc )
        return x,y,z

    def setAmplitude(amp):
        rays[:,:,8] = amp
    def scaleAmplitude(iTheta, iPhi, scale):
        if scale < 0:
            raise ValueError("TRYING TO REVERSE AMPLITUDE!")
        rays[iTheta,iPhi,8] *= scale
        return rays[iTheta,iPhi,8]
    amplitude = lambda iTheta, iPhi: rays[iTheta, iPhi, 8]

    def addTime(iTheta, iPhi, t):
        rays[iTheta,iPhi,0] += t
        return rays[iTheta,iPhi,0]
    rayTime = lambda iTheta, iPhi: rays[iTheta,iPhi,0]

    times = lambda rays: rays[:,:,0]

    ###

    #Initialize rays
    setAmplitude(initialAmplitude)

    thetaSpace = np.linspace(startAngle,np.pi/2,nTheta)
    phiSpace   = np.linspace(startAngle,np.pi/2,nPhi  )

    for iTheta in range(nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta, iPhi, np.array([np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)]) )

    rays[nTheta:2*nTheta-1,:,:] = np.copy(rays[:nTheta-1,:,:])
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = np.copy(rays[:,:nPhi-1,:])
    rays[:,nPhi:2*nPhi-1,2] *= -1

    setLocToDir()

    scaleLocation(startSphereRadius)

    shiftAllLocations(startRaysLocation)

    initialArrangement   = np.copy(rays.reshape(-1,nPars)[:,1:4])
    initialArrangement2D = np.copy(rays[:,:,1:4])
    initialDirection2D = np.copy(rays[:,:,4:7])

    ###Trace debug setup
    iTrace = np.array(iTrace)
    jTrace = np.array(jTrace)
    traces = iTrace.shape[0]
    path     = np.zeros((traces,10000,3))
    pathAmps = np.zeros((traces,10000))
    pathTimes = np.zeros((traces))
    for i in range(traces):
        path[i,0,:] = location(iTrace[i],jTrace[i])
        pathAmps[i,0] = 1
    nPath = np.ones(traces, dtype=np.int)

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])
    def refractionsDirection(refractionCos, direction, iIntersectPlane):
        #According to Snell's law
        updDirection = np.zeros(3)
        if np.abs(direction[2] - 1) < eps and iIntersectPlane == 4:
            return direction
        sign = normals[iIntersectPlane][idxsD[iIntersectPlane]]
        refractionX = 0
        refractionY = 0
        refractionZ = 0
        signF = lambda x: x/np.abs(x)
        if iIntersectPlane < 2:
            refractionX = sign*refractionCos
            if np.abs(direction[1]) < eps:
                refractionY = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionX**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionX**2)
            else:
                refractionYZrel = direction[2]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionX**2) / (1 + refractionYZrel**2))
                refractionZ = refractionY*refractionYZrel
        elif iIntersectPlane < 4:
            refractionY = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionY**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionY**2)
            else:
                refractionZXrel = direction[0]/direction[2]
                refractionZ = signF(direction[2]) * np.sqrt((1 - refractionY**2) / (1 + refractionZXrel**2))
                refractionX = refractionZ * refractionZXrel
        else:
            refractionZ = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionZ**2)
            elif np.abs(direction[1]) < eps:
                refractionY = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionZ**2)
            else:
                refractionYXrel = direction[0]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionZ**2) / (1 + refractionYXrel**2))
                refractionX = refractionYXrel * refractionY

        updDirection = np.array([refractionX, refractionY, refractionZ])
        return updDirection/npl.norm(updDirection)


    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    def triangleAreaBy3Sides(a, b, c):
        halfPerimeter = (a+b+c)/2
        return np.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c))

    def triangleAreaBy3Points(A, B, C):
        a = npl.norm(A-B)
        b = npl.norm(C-B)
        c = npl.norm(C-A)
        return triangleAreaBy3Sides(a, b, c)

    def triangleAmplitudeNorm(iCurRay, jCurRay):
        quarter1 = (None, 1e111)
        quarter2 = (None, 1e111)
        quarter3 = (None, 1e111)
        quarter4 = (None, 1e111)

        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isUnderZSurface(iRay, jRay) and not isLost(iRay,jRay):
                    distV = location(iCurRay, jCurRay) - location(iRay, jRay)
                    distance = npl.norm(distV)
                    if distance > 1e-7:
                        if distV[0] >= 0 and distV[1]  < 0 and distance < quarter1[1]:
                            quarter1 = ((iRay, jRay), distance)
                        if distV[0] >= 0 and distV[1] >= 0 and distance < quarter2[1]:
                            quarter2 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1]  < 0 and distance < quarter3[1]:
                            quarter3 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1] >= 0 and distance < quarter4[1]:
                            quarter4 = ((iRay, jRay), distance)

        closestRays = list(filter(lambda x: x[0] != None, [quarter1, quarter2, quarter3, quarter4]))

        if len(closestRays) < 1:
            raise ValueError("There is no triangles are found!")

        finishAreas = []
        startAreas  = []
        for i in range(len(closestRays)):
            finishDist = npl.norm(location(*closestRays[i-1][0]) - location(*closestRays[i][0]))
            finishAreas.append(triangleAreaBy3Sides(closestRays[i-1][1], closestRays[i][1], finishDist))
            startAreas.append(
                triangleAreaBy3Points(
                    initialLocation(iCurRay,jCurRay), 
                    initialLocation(*closestRays[i-1][0]), 
                    initialLocation(*closestRays[i][0])
                    ))

    #Triangle norm where we take triangles on the start unit sphere and then try to find their images
    def triangleAmplitudeNormReverse(iCurRay, jCurRay):

        finishAreas = []
        startAreas  = []
        startAreasAppend = lambda iR0, jR0, iR1, jR1: \
            startAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(initialLocation(iR0,jR0) - initialLocation(iR1,jR0))
                 ,npl.norm(initialLocation(iR1,jR0) - initialLocation(iR0,jR1))
                 ,npl.norm(initialLocation(iR0,jR0) - initialLocation(iR0,jR1))
                 ))
        finishAreasAppend = lambda iR0, jR0, iR1, jR1: \
            finishAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(location(iR0,jR0) - location(iR1,jR0))
                 ,npl.norm(location(iR1,jR0) - location(iR0,jR1))
                 ,npl.norm(location(iR0,jR0) - location(iR0,jR1))
                 ))
        areasAppend = lambda iR, jR, iR1, jR1: \
             startAreasAppend(iR, jR, iR1, jR1) == \
             finishAreasAppend(iR, jR, iR1, jR1)
        
        isLost0 = lambda iR, jR: isLost(iR,jR) and (not isUnderZSurface(iR,jR))

        isNotLost = lambda iR, jR, iR0, jR0: \
                not (isLost0(iR,jR) or isLost0(iR0,jR0) or isLost0(iR,jR0) or isLost0(iR0,jR))

        if iCurRay+1 < 2*nTheta-1 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay-1)
        if iCurRay+1 < 2*nTheta-1 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay-1)

        if len(startAreas) < 3:
            return False, None
        #print("Areas:", np.array(finishAreas), " ", np.array(startAreas))
        
        return True, np.sqrt(np.median(np.array(finishAreas)) / np.median(np.array(startAreas)))

    def approxParamsByClosestRay(point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
        minD = dists[curMinimumIdx]
        minId = curMinimumIdx

        outTime = rayTime(*minId)
        outAmp  = amplitude(*minId)
        outDirection = initialDirection(*minId)

        return outTime, outAmp, outDirection, minD

    def distanceNorm(iCurRay, jCurRay):
        return True, distance(iCurRay, jCurRay)

    def approxParamsByClosestRays(n, point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        mins    = []
        minsD   = np.zeros(n)
        weights = np.zeros(n)
        for i in range(n):
            curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
            minsD[i] = dists[curMinimumIdx]
            mins.append(curMinimumIdx)
            dists[curMinimumIdx] = 1e111
            weights[i] = scs.multivariate_normal.pdf(point, location(*curMinimumIdx)[:2], np.eye(2)*minsD[0])


        outTime = 0
        outAmp  = 0
        outDirection = np.zeros(3)
        for i in range(n):
            outTime += (weights[i]/np.sum(weights)) * rayTime(*mins[i])
            outAmp  += (weights[i]/np.sum(weights)) * amplitude(*mins[i])
            outDirection += (weights[i]/np.sum(weights)) * direction(*mins[i])

        return outTime, outAmp, outDirection

    acousticImpedance = vp * density

    zSurface = mostNegativeCube[2] + nCubes[2]*rCube
    isUnderZSurface = lambda iRay, jRay: location(iRay, jRay)[2] < zSurface
    isInsideXGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[0] >= mostNegativeCube[0] and\
        location(iRay, jRay)[0] < mostNegativeCube[0] + nCubes[0]*rCube
    isInsideYGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[1] >= mostNegativeCube[1] and\
        location(iRay, jRay)[1] < mostNegativeCube[1] + nCubes[1]*rCube
    isInsideGrid = lambda iRay, jRay:\
        isUnderZSurface(iRay, jRay) and isInsideXGrid(iRay, jRay) and isInsideYGrid(iRay, jRay)
    #raise ValueError()
    for iRay in range(2*nTheta-1):
        for jRay in range(2*nPhi-1):
            while isInsideGrid(iRay, jRay) and not isLost(iRay,jRay):
                if np.dot(direction(iRay,jRay), normals[4]) <= 0:
                    rayLost(iRay, jRay)

                #Trace ray through heterogeneous environment
                planes = closestPlanes(location(iRay,jRay))
                distanceToPlane = 1e100
                iIntersectPlane = 0
                lengthNormalToPlane = 0
                for iNormal in range(normals.shape[0]):
                    if np.dot(direction(iRay,jRay), normals[iNormal]) > 0:
                        curLengthNormalToPlane = np.abs(location(iRay,jRay)[idxsD[iNormal]] - planes[iNormal])
                        cosRayPlane = np.dot(direction(iRay,jRay), normals[iNormal])
                        curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                        if curDistanceToPlane < distanceToPlane:
                            distanceToPlane = curDistanceToPlane
                            iIntersectPlane = iNormal
                            lengthNormalToPlane = curLengthNormalToPlane
                stepsToIntersect = np.abs(lengthNormalToPlane / direction(iRay,jRay)[idxsD[iIntersectPlane]])
                #print("stepsToIntersect=",stepsToIntersect)
                #print("direction(iRay, jRay)=",direction(iRay, jRay))
                updLocation  = location(iRay, jRay) + (stepsToIntersect + 1e-7)*direction(iRay, jRay)
                prevLocation = np.copy(location(iRay, jRay))
                setLocation(iRay, jRay, updLocation)
                addDistance(iRay, jRay, npl.norm(prevLocation - updLocation))

                #Time update
                time = npl.norm(updLocation - prevLocation)/vp[cubeIndex(prevLocation)]
                addTime(iRay,jRay, time)

                #Snell's law
                if isInsideGrid(iRay, jRay):
                    refractionIndexFrom = acousticImpedance[cubeIndex(prevLocation)]
                    refractionIndexTo   = acousticImpedance[cubeIndex(updLocation )]
                    incidenceSin  = np.sqrt(1 - np.dot(direction(iRay, jRay), normals[iIntersectPlane])**2)
                    if refractionIndexTo*incidenceSin > refractionIndexFrom:
                        rayLost(iRay, jRay)
                    else:
                        refractionSin = refractionIndexTo/refractionIndexFrom * incidenceSin
                        refractionCos = np.sqrt(1 - refractionSin**2)
                        updDirection = refractionsDirection(refractionCos, direction(iRay, jRay), iIntersectPlane)
                        setDirection(iRay, jRay, updDirection)
                #Fresnel's law
                        incidenceCos = np.sqrt(1 - incidenceSin**2)
                        fresnelCoeff = 2*refractionIndexTo*incidenceCos / \
                            (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos)
                        updAmplitude = scaleAmplitude(iRay,jRay,fresnelCoeff)


                #Debug
                for indTrace in range(traces):
                    if iRay == iTrace[indTrace] and jRay == jTrace[indTrace]:
                        path[indTrace,nPath[indTrace],:] = updLocation
                        pathAmps[indTrace,nPath[indTrace]] = updAmplitude
                        pathTimes[indTrace] += time
                        nPath[indTrace] += 1

    #print("TRIANGLE AMPLITUDE NORM IS DISABLED!")
    triNorms = np.zeros((2*nTheta-1,2*nPhi-1))
    addToLost = []
    for iRay in range(2*nTheta-1):
        for jRay in range(2*nPhi-1):
            #print("Triangle amplitude update ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #Triangle amplitude update
            if not isLost(iRay, jRay) and not isUnderZSurface(iRay, jRay):
                #isNormExist, rayNorm = triangleAmplitudeNormReverse(iRay,jRay)
                isNormExist, rayNorm = distanceNorm(iRay,jRay)
                if isNormExist:
                   scaleAmplitude(iRay,jRay,1/rayNorm)
                   #print("triNorm:", rayNorm)
                   triNorms[iRay, jRay] = rayNorm
                else:
                   addToLost.append((iRay,jRay))  

    for ray in addToLost:
        rayLost(*ray)

    #get signal for receivers
    receiversTimes = np.zeros(receiversCoordinates.shape[0])
    receiversAmps  = np.zeros(receiversCoordinates.shape[0])
    receiversTensorAmps = np.zeros((receiversCoordinates.shape[0],3,6))
    distancesToRays = np.zeros(receiversCoordinates.shape[0])
    for i in range(receiversCoordinates.shape[0]):
        #print("Receiver ", i, " from ", receiversCoordinates.shape[0])
        curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(receiversCoordinates[i,:])
        receiversTimes[i] = curTime
        receiversAmps[i]  = curAmplitude
        distancesToRays[i] = distanceToRay

        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])
        receiversTensorAmps[i,0,0] =  6*xCos**3          - 3*xCos
        receiversTensorAmps[i,1,0] =  6*(xCos**2)*yCos   -   yCos
        receiversTensorAmps[i,2,0] =  6*(xCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,1] =  6*xCos*(yCos**2)   -   xCos
        receiversTensorAmps[i,1,1] =  6*(yCos**3)        - 3*yCos
        receiversTensorAmps[i,2,1] =  6*(yCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,2] =  6*xCos*(zCos**2)   -   xCos
        receiversTensorAmps[i,1,2] =  6*(yCos)*(zCos**2) -   yCos
        receiversTensorAmps[i,2,2] =  6*(zCos**3)        - 3*zCos

        receiversTensorAmps[i,0,3] =  6*(xCos**2)*(yCos) -   yCos
        receiversTensorAmps[i,1,3] =  6*(yCos**2)*(xCos) -   xCos
        receiversTensorAmps[i,2,3] =  6*xCos*yCos*(zCos)

        receiversTensorAmps[i,0,4] = (6*(xCos**2)*(zCos) -   zCos)
        receiversTensorAmps[i,1,4] = (6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,2,4] = (6*(zCos**2)*xCos   -   xCos)

        receiversTensorAmps[i,0,5] = (6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,1,5] = (6*(yCos**2)*(zCos)  -  zCos)
        receiversTensorAmps[i,2,5] = (6*(zCos**2)*yCos    -  yCos)
    
    tensorRays = np.zeros((2*nTheta-1, 2*nPhi-1, 3, 6))
    for iRay in range(2*nTheta-1):
        print(iRay, 2*nTheta-1)
        for jRay in range(2*nPhi-1):
            if not isLost(iRay, jRay):
                #curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(location(iRay,jRay)[:2])
                #receiversTimes[i] = curTime
                #receiversAmps[i]  = curAmplitude
                #distancesToRays[i] = distanceToRay
                curDirection = direction(iRay,jRay)

                xCos = np.dot(curDirection, normals[1])
                yCos = np.dot(curDirection, normals[3])
                zCos = np.dot(curDirection, normals[4])
                tensorRays[iRay,jRay,0,0] =  6*xCos**3          - 3*xCos
                tensorRays[iRay,jRay,1,0] =  6*(xCos**2)*yCos   -   yCos
                tensorRays[iRay,jRay,2,0] =  6*(xCos**2)*zCos   -   zCos

                tensorRays[iRay,jRay,0,1] =  6*xCos*(yCos**2)   -   xCos
                tensorRays[iRay,jRay,1,1] =  6*(yCos**3)        - 3*yCos
                tensorRays[iRay,jRay,2,1] =  6*(yCos**2)*zCos   -   zCos

                tensorRays[iRay,jRay,0,2] =  6*xCos*(zCos**2)   -   xCos
                tensorRays[iRay,jRay,1,2] =  6*(yCos)*(zCos**2) -   yCos
                tensorRays[iRay,jRay,2,2] =  6*(zCos**3)        - 3*zCos

                tensorRays[iRay,jRay,0,3] =  6*(xCos**2)*(yCos) -   yCos
                tensorRays[iRay,jRay,1,3] =  6*(yCos**2)*(xCos) -   xCos
                tensorRays[iRay,jRay,2,3] =  6*xCos*yCos*(zCos)

                tensorRays[iRay,jRay,0,4] = (6*(xCos**2)*(zCos) -   zCos)
                tensorRays[iRay,jRay,1,4] = (6*xCos*yCos*(zCos)         )
                tensorRays[iRay,jRay,2,4] = (6*(zCos**2)*xCos   -   xCos)

                tensorRays[iRay,jRay,0,5] = (6*xCos*yCos*(zCos)         )
                tensorRays[iRay,jRay,1,5] = (6*(yCos**2)*(zCos)  -  zCos)
                tensorRays[iRay,jRay,2,5] = (6*(zCos**2)*yCos    -  yCos)

    gaussWavelet = lambda mean, var: lambda x:\
              -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
    gaussWavelet2 = lambda mean, var: lambda x:\
          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))
                sigma=1/fdPWave/5
                length = receiverSignals.shape[3]

                gwt = gaussWavelet2(waveComing/fd,sigma)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt
                #plt.plot(receiverSignals[iR, iCh, iTensor, :])
                #plt.plot(np.arange(0,gwt.shape[0])*(fd/gwt.shape[0]), np.abs(np.fft.fft(gwt)))
                #plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1

    print("should be second order")
    print(np.sum(rays[:,:,7]), (2*nTheta-1)*(2*nPhi-1))
    return receiverSignals, distancesToRays, rays, tensorRays, path, pathAmps, pathTimes,  nPath, initialArrangement


def rayTracer0(
        receiversCoordinates # (number of receivers, 2)
       ,fd
       ,fdPWave
       ,timeLength
       ,nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,startRaysLocation
       ,initialAmplitude
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,nTheta           # number of theta steps in quarter
       ,nPhi
       ,startAngle = np.pi/10
       ,iTrace   = 0
       ,jTrace   = 0
       ):
    print("Start Ray Tracer")

    ###Initial setup
    eps = 1e-9
    startSphereRadius = rCube/1000
    #rays = (theta, phi, time, x, y, z, dirX, dirY, dirZ, isLost, amplitude)
    rays = np.zeros((2*nTheta-1,2*nPhi-1,9))
    distances = np.zeros((2*nTheta-1,2*nPhi-1))
    nPars = rays.shape[2]

    ###Utils definition
    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]
    initialLocation = lambda iTheta, iPhi: initialArrangement2D[iTheta,iPhi,:]

    def addDistance(iTheta, iPhi, distance):
        distances[iTheta,iPhi] += distance
    distance = lambda iTheta, iPhi: distances[iTheta, iPhi] 

    def shiftAllLocations(v):
        rays[:,:,1:4] += np.array(v)

    def setDirection(iTheta, iPhi, direction):
        rays[iTheta,iPhi,4:7] = direction
    direction = lambda iTheta, iPhi: rays[iTheta,iPhi,4:7]
    initialDirection = lambda iTheta, iPhi: initialDirection2D[iTheta,iPhi,:]

    def scaleLocation(s):
        rays[:,:,1:4] *= s
    def setLocToDir():
        rays[:,:,4:7] = rays[:,:,1:4]

    def rayLost(iTheta, iPhi):
        rays[iTheta, iPhi, 7] = 1
    isLost = lambda iTheta, iPhi: rays[iTheta, iPhi, 7] == 1

    def cubeIndex(loc):
        x = int((loc[0] - mostNegativeCube[0])) // rCube
        y = int((loc[1] - mostNegativeCube[1])) // rCube
        z = int((loc[2] - mostNegativeCube[2])) // rCube
        #if x < 0 or y < 0 or z < 0:
        #    raise ValueError("mostNegativeCube: ", mostNegativeCube, " Location of power: ", loc )
        return x,y,z

    def setAmplitude(amp):
        rays[:,:,8] = amp
    def scaleAmplitude(iTheta, iPhi, scale):
        if scale < 0:
            raise ValueError("TRYING TO REVERSE AMPLITUDE!")
        rays[iTheta,iPhi,8] *= scale
        return rays[iTheta,iPhi,8]
    amplitude = lambda iTheta, iPhi: rays[iTheta, iPhi, 8]

    def addTime(iTheta, iPhi, t):
        rays[iTheta,iPhi,0] += t
        return rays[iTheta,iPhi,0]
    rayTime = lambda iTheta, iPhi: rays[iTheta,iPhi,0]

    times = lambda rays: rays[:,:,0]

    ###

    #Initialize rays
    setAmplitude(initialAmplitude)

    thetaSpace = np.linspace(startAngle,np.pi/2,nTheta)
    phiSpace   = np.linspace(startAngle,np.pi/2,nPhi  )

    for iTheta in range(nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta, iPhi, np.array([np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)]) )

    rays[nTheta:2*nTheta-1,:,:] = np.copy(rays[:nTheta-1,:,:])
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = np.copy(rays[:,:nPhi-1,:])
    rays[:,nPhi:2*nPhi-1,2] *= -1

    setLocToDir()

    scaleLocation(startSphereRadius)

    shiftAllLocations(startRaysLocation)

    initialArrangement   = np.copy(rays.reshape(-1,nPars)[:,1:4])
    initialArrangement2D = np.copy(rays[:,:,1:4])
    initialDirection2D = np.copy(rays[:,:,4:7])

    ###Trace debug setup
    iTrace = np.array(iTrace)
    jTrace = np.array(jTrace)
    traces = iTrace.shape[0]
    path     = np.zeros((traces,10000,3))
    pathAmps = np.zeros((traces,10000))
    pathTimes = np.zeros((traces))
    for i in range(traces):
        path[i,0,:] = location(iTrace[i],jTrace[i])
        pathAmps[i,0] = 1
    nPath = np.ones(traces, dtype=np.int)

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])
    def refractionsDirection(refractionCos, direction, iIntersectPlane):
        #According to Snell's law
        updDirection = np.zeros(3)
        if np.abs(direction[2] - 1) < eps and iIntersectPlane == 4:
            return direction
        sign = normals[iIntersectPlane][idxsD[iIntersectPlane]]
        refractionX = 0
        refractionY = 0
        refractionZ = 0
        signF = lambda x: x/np.abs(x)
        if iIntersectPlane < 2:
            refractionX = sign*refractionCos
            if np.abs(direction[1]) < eps:
                refractionY = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionX**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionX**2)
            else:
                refractionYZrel = direction[2]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionX**2) / (1 + refractionYZrel**2))
                refractionZ = refractionY*refractionYZrel
        elif iIntersectPlane < 4:
            refractionY = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionY**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionY**2)
            else:
                refractionZXrel = direction[0]/direction[2]
                refractionZ = signF(direction[2]) * np.sqrt((1 - refractionY**2) / (1 + refractionZXrel**2))
                refractionX = refractionZ * refractionZXrel
        else:
            refractionZ = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionZ**2)
            elif np.abs(direction[1]) < eps:
                refractionY = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionZ**2)
            else:
                refractionYXrel = direction[0]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionZ**2) / (1 + refractionYXrel**2))
                refractionX = refractionYXrel * refractionY

        updDirection = np.array([refractionX, refractionY, refractionZ])
        return updDirection/npl.norm(updDirection)


    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    def triangleAreaBy3Sides(a, b, c):
        halfPerimeter = (a+b+c)/2
        return np.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c))

    def triangleAreaBy3Points(A, B, C):
        a = npl.norm(A-B)
        b = npl.norm(C-B)
        c = npl.norm(C-A)
        return triangleAreaBy3Sides(a, b, c)

    def triangleAmplitudeNorm(iCurRay, jCurRay):
        quarter1 = (None, 1e111)
        quarter2 = (None, 1e111)
        quarter3 = (None, 1e111)
        quarter4 = (None, 1e111)

        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isUnderZSurface(iRay, jRay) and not isLost(iRay,jRay):
                    distV = location(iCurRay, jCurRay) - location(iRay, jRay)
                    distance = npl.norm(distV)
                    if distance > 1e-7:
                        if distV[0] >= 0 and distV[1]  < 0 and distance < quarter1[1]:
                            quarter1 = ((iRay, jRay), distance)
                        if distV[0] >= 0 and distV[1] >= 0 and distance < quarter2[1]:
                            quarter2 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1]  < 0 and distance < quarter3[1]:
                            quarter3 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1] >= 0 and distance < quarter4[1]:
                            quarter4 = ((iRay, jRay), distance)

        closestRays = list(filter(lambda x: x[0] != None, [quarter1, quarter2, quarter3, quarter4]))

        if len(closestRays) < 1:
            raise ValueError("There is no triangles are found!")

        finishAreas = []
        startAreas  = []
        for i in range(len(closestRays)):
            finishDist = npl.norm(location(*closestRays[i-1][0]) - location(*closestRays[i][0]))
            finishAreas.append(triangleAreaBy3Sides(closestRays[i-1][1], closestRays[i][1], finishDist))
            startAreas.append(
                triangleAreaBy3Points(
                    initialLocation(iCurRay,jCurRay), 
                    initialLocation(*closestRays[i-1][0]), 
                    initialLocation(*closestRays[i][0])
                    ))

    #Triangle norm where we take triangles on the start unit sphere and then try to find their images
    def triangleAmplitudeNormReverse(iCurRay, jCurRay):

        finishAreas = []
        startAreas  = []
        startAreasAppend = lambda iR0, jR0, iR1, jR1: \
            startAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(initialLocation(iR0,jR0) - initialLocation(iR1,jR0))
                 ,npl.norm(initialLocation(iR1,jR0) - initialLocation(iR0,jR1))
                 ,npl.norm(initialLocation(iR0,jR0) - initialLocation(iR0,jR1))
                 ))
        finishAreasAppend = lambda iR0, jR0, iR1, jR1: \
            finishAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(location(iR0,jR0) - location(iR1,jR0))
                 ,npl.norm(location(iR1,jR0) - location(iR0,jR1))
                 ,npl.norm(location(iR0,jR0) - location(iR0,jR1))
                 ))
        areasAppend = lambda iR, jR, iR1, jR1: \
             startAreasAppend(iR, jR, iR1, jR1) == \
             finishAreasAppend(iR, jR, iR1, jR1)
        
        isLost0 = lambda iR, jR: isLost(iR,jR) and (not isUnderZSurface(iR,jR))

        isNotLost = lambda iR, jR, iR0, jR0: \
                not (isLost0(iR,jR) or isLost0(iR0,jR0) or isLost0(iR,jR0) or isLost0(iR0,jR))

        if iCurRay+1 < 2*nTheta-1 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay-1)
        if iCurRay+1 < 2*nTheta-1 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay-1)

        if len(startAreas) < 3:
            return False, None
        #print("Areas:", np.array(finishAreas), " ", np.array(startAreas))
        
        return True, np.sqrt(np.median(np.array(finishAreas)) / np.median(np.array(startAreas)))

    def approxParamsByClosestRay(point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
        minD = dists[curMinimumIdx]
        minId = curMinimumIdx

        outTime = rayTime(*minId)
        outAmp  = amplitude(*minId)
        outDirection = initialDirection(*minId)

        return outTime, outAmp, outDirection, minD

    def distanceNorm(iCurRay, jCurRay):
        return True, distance(iCurRay, jCurRay)

    def approxParamsByClosestRays(n, point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        mins    = []
        minsD   = np.zeros(n)
        weights = np.zeros(n)
        for i in range(n):
            curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
            minsD[i] = dists[curMinimumIdx]
            mins.append(curMinimumIdx)
            dists[curMinimumIdx] = 1e111
            weights[i] = scs.multivariate_normal.pdf(point, location(*curMinimumIdx)[:2], np.eye(2)*minsD[0])


        outTime = 0
        outAmp  = 0
        outDirection = np.zeros(3)
        for i in range(n):
            outTime += (weights[i]/np.sum(weights)) * rayTime(*mins[i])
            outAmp  += (weights[i]/np.sum(weights)) * amplitude(*mins[i])
            outDirection += (weights[i]/np.sum(weights)) * direction(*mins[i])

        return outTime, outAmp, outDirection


    acousticImpedance = vp * density

    zSurface = mostNegativeCube[2] + nCubes[2]*rCube
    isUnderZSurface = lambda iRay, jRay: location(iRay, jRay)[2] < zSurface
    isInsideXGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[0] >= mostNegativeCube[0] and\
        location(iRay, jRay)[0] < mostNegativeCube[0] + nCubes[0]*rCube
    isInsideYGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[1] >= mostNegativeCube[1] and\
        location(iRay, jRay)[1] < mostNegativeCube[1] + nCubes[1]*rCube
    isInsideGrid = lambda iRay, jRay:\
        isUnderZSurface(iRay, jRay) and isInsideXGrid(iRay, jRay) and isInsideYGrid(iRay, jRay)
    #raise ValueError()
    for iRay in range(2*nTheta-1):
        #print("Ray ", iRay*(2*nPhi-1), " from ", (2*nTheta-1))
        for jRay in range(2*nPhi-1):
            #print(iRay, jRay, " Ray ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            
            while isInsideGrid(iRay, jRay) and not isLost(iRay,jRay):
                #print("direction(iRay, jRay)=",direction(iRay, jRay))

                if np.dot(direction(iRay,jRay), normals[4]) <= 0:
                    #print("RayLost: Full reflection!")
                    rayLost(iRay, jRay)

                #Trace ray through heterogeneous environment
                planes = closestPlanes(location(iRay,jRay))
                distanceToPlane = 1e100
                iIntersectPlane = 0
                lengthNormalToPlane = 0
                for iNormal in range(normals.shape[0]):
                    if np.dot(direction(iRay,jRay), normals[iNormal]) > 0:
                        curLengthNormalToPlane = np.abs(location(iRay,jRay)[idxsD[iNormal]] - planes[iNormal])
                        cosRayPlane = np.dot(direction(iRay,jRay), normals[iNormal])
                        curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                        if curDistanceToPlane < distanceToPlane:
                            distanceToPlane = curDistanceToPlane
                            iIntersectPlane = iNormal
                            lengthNormalToPlane = curLengthNormalToPlane
                stepsToIntersect = np.abs(lengthNormalToPlane / direction(iRay,jRay)[idxsD[iIntersectPlane]])
                #print("stepsToIntersect=",stepsToIntersect)
                #print("direction(iRay, jRay)=",direction(iRay, jRay))
                updLocation  = location(iRay, jRay) + (stepsToIntersect + 1e-7)*direction(iRay, jRay)
                prevLocation = np.copy(location(iRay, jRay))
                setLocation(iRay, jRay, updLocation)
                addDistance(iRay, jRay, npl.norm(prevLocation - updLocation))

                #Time update
                time = npl.norm(updLocation - prevLocation)/vp[cubeIndex(prevLocation)]
                addTime(iRay,jRay, time)

                #Snell's law
                if isInsideGrid(iRay, jRay):
                    refractionIndexFrom = acousticImpedance[cubeIndex(prevLocation)]
                    refractionIndexTo   = acousticImpedance[cubeIndex(updLocation )]
                    incidenceSin  = np.sqrt(1 - np.dot(direction(iRay, jRay), normals[iIntersectPlane])**2)
                    if refractionIndexTo*incidenceSin > refractionIndexFrom:
                        rayLost(iRay, jRay)
                    else:
                        refractionSin = refractionIndexTo/refractionIndexFrom * incidenceSin
                        refractionCos = np.sqrt(1 - refractionSin**2)
                        updDirection = refractionsDirection(refractionCos, direction(iRay, jRay), iIntersectPlane)
                        setDirection(iRay, jRay, updDirection)
                #Fresnel's law
                        incidenceCos = np.sqrt(1 - incidenceSin**2)
                        fresnelCoeff = 2*refractionIndexTo*incidenceCos / \
                            (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos)
                        updAmplitude = scaleAmplitude(iRay,jRay,fresnelCoeff)


                #Debug
                for indTrace in range(traces):
                    if iRay == iTrace[indTrace] and jRay == jTrace[indTrace]:
                        path[indTrace,nPath[indTrace],:] = updLocation
                        pathAmps[indTrace,nPath[indTrace]] = updAmplitude
                        pathTimes[indTrace] += time
                        nPath[indTrace] += 1

    #print("TRIANGLE AMPLITUDE NORM IS DISABLED!")
    triNorms = np.zeros((2*nTheta-1,2*nPhi-1))
    addToLost = []
    for iRay in range(2*nTheta-1):
        for jRay in range(2*nPhi-1):
            #print("Triangle amplitude update ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #Triangle amplitude update
            if not isLost(iRay, jRay) and not isUnderZSurface(iRay, jRay):
                #isNormExist, rayNorm = triangleAmplitudeNormReverse(iRay,jRay)
                isNormExist, rayNorm = distanceNorm(iRay,jRay)
                if isNormExist:
                   scaleAmplitude(iRay,jRay,1/rayNorm)
                   #print("triNorm:", rayNorm)
                   triNorms[iRay, jRay] = rayNorm
                else:
                   addToLost.append((iRay,jRay))  

    for ray in addToLost:
        rayLost(*ray)

    #get signal for receivers
    receiversTimes = np.zeros(receiversCoordinates.shape[0])
    receiversAmps  = np.zeros(receiversCoordinates.shape[0])
    receiversTensorAmps = np.zeros((receiversCoordinates.shape[0],3,6))
    distancesToRays = np.zeros(receiversCoordinates.shape[0])
    for i in range(receiversCoordinates.shape[0]):
        #print("Receiver ", i, " from ", receiversCoordinates.shape[0])
        curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(receiversCoordinates[i,:])
        receiversTimes[i] = curTime
        receiversAmps[i]  = curAmplitude
        distancesToRays[i] = distanceToRay

        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])
        receiversTensorAmps[i,0,0] =       6*xCos**3          - 3*xCos
        receiversTensorAmps[i,1,0] =       6*(xCos**2)*yCos   -   yCos
        receiversTensorAmps[i,2,0] =       6*(xCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,1] =       6*xCos*(yCos**2)   -   xCos
        receiversTensorAmps[i,1,1] =       6*(yCos**3)        - 3*yCos
        receiversTensorAmps[i,2,1] =       6*(yCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,2] =       6*xCos*(zCos**2)   -   xCos
        receiversTensorAmps[i,1,2] =       6*(yCos)*(zCos**2) -   yCos
        receiversTensorAmps[i,2,2] =       6*(zCos**3)        - 3*zCos

        receiversTensorAmps[i,0,3] =       6*(xCos**2)*(yCos) -   yCos
        receiversTensorAmps[i,1,3] =       6*(yCos**2)*(xCos) -   xCos
        receiversTensorAmps[i,2,3] =       6*xCos*yCos*(zCos)

        receiversTensorAmps[i,0,4] = (-1)*(6*(xCos**2)*(zCos) -   zCos)
        receiversTensorAmps[i,1,4] = (-1)*(6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,2,4] = (-1)*(6*(zCos**2)*xCos   -   xCos)

        receiversTensorAmps[i,0,5] = (-1)*(6*xCos*yCos*(zCos)         ) 
        receiversTensorAmps[i,1,5] = (-1)*(6*(yCos**2)*(zCos)  -  zCos)
        receiversTensorAmps[i,2,5] = (-1)*(6*(zCos**2)*yCos    -  yCos)

    gaussWavelet = lambda mean, var: lambda x:\
              -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
    gaussWavelet2 = lambda mean, var: lambda x:\
          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))
                sigma=1/fdPWave/5
                length = receiverSignals.shape[3]

                gwt = gaussWavelet2(waveComing/fd,sigma)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt
                #plt.plot(receiverSignals[iR, iCh, iTensor, :])
                #plt.plot(np.arange(0,gwt.shape[0])*(fd/gwt.shape[0]), np.abs(np.fft.fft(gwt)))
                #plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1

    print("should be second order")
    print(np.sum(rays[:,:,7]), (2*nTheta-1)*(2*nPhi-1))
    return receiverSignals, distancesToRays, rays, path, pathAmps, pathTimes,  nPath, initialArrangement

def rayTracerDirect(
        receiversCoordinates # (number of receivers, 2)
       ,fd
       ,fdPWave
       ,timeLength
       ,nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,startRaysLocation
       ,initialAmplitude
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,maxSteps
       ,tolerance
       ,iTrace   = 0
       ):
    print("Start Ray Tracer")

    ###Initial setup
    eps = 1e-9
    #rays = (i, time, x, y, z, dirX, dirY, dirZ, isLost, amplitude, distance, initDirX, initDirY, initDirZ)
    rays = np.zeros((receiversCoordinates.shape[0],13))
    print(rays.shape)
    nPars = rays.shape[1]

    ###Utils definition
    def setLocation(i, location):
        rays[i,1:4] = location
    location = lambda i: rays[i,1:4]

    def addDistance(i,distance):
        rays[i,9] += distance
    distance = lambda i: rays[i,9]
    distances = lambda: rays[:,9]

    def setDirection(i, direction):
        rays[i,4:7] = direction
    direction = lambda i: rays[i,4:7]

    def setInitDirection(i, direction):
        rays[i,10:13] = direction
        rays[i,4:7] = direction
    initDirection = lambda i: rays[i,10:13]

    def scaleLocation(s):
        rays[:,1:4] *= s

    def shiftAllLocations(v):
        rays[:,1:4] += np.array(v)

    def setLocToDir():
        rays[:,4:7] = rays[:,1:4]

    def rayLost(i):
        rays[i,7] = 1
    isLost = lambda i: rays[i, 7] == 1

    def cubeIndex(loc):
        x = int((loc[0] - mostNegativeCube[0])) // rCube
        y = int((loc[1] - mostNegativeCube[1])) // rCube
        z = int(-mostNegativeCube[2] - (loc[2] - mostNegativeCube[2])) // rCube
        #if x < 0 or y < 0 or z < 0:
        #    raise ValueError("mostNegativeCube: ", mostNegativeCube, " Location of power: ", loc )
        return x,y,z

    def setAmplitudes(amp):
        rays[:,8] = amp

    def setAmplitude(iRay, amp):
        rays[iRay,8] = amp

    def scaleAmplitude(i, scale):
        if scale < 0:
            raise ValueError("TRYING TO REVERSE AMPLITUDE!")
        rays[i,8] *= scale
        return rays[i,8]
    amplitude = lambda i: rays[i, 8]
    amplitudes = lambda: rays[:,8]

    def addTime(i, t):
        rays[i,0] += t
        return rays[i,0]
    rayTime = lambda i: rays[i,0]

    times = lambda: rays[:,0]

    ###Trace debug setup
    iTrace = np.array(iTrace)
    print(iTrace)
    traces = iTrace.shape[0]
    path     = np.zeros((traces,10000,3))
    pathAmps = np.zeros((traces,10000))
    pathTimes = np.zeros((traces))
    for i in range(traces):
        path[i,0,:] = location(iTrace[i])
        pathAmps[i,0] = 1
    nPath = np.ones(traces, dtype=np.int)

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])
    def refractionsDirection(refractionCos, direction, iIntersectPlane):
        #According to Snell's law
        updDirection = np.zeros(3)
        if np.abs(direction[2] - 1) < eps and iIntersectPlane == 4:
            return direction
        sign = normals[iIntersectPlane][idxsD[iIntersectPlane]]
        refractionX = 0
        refractionY = 0
        refractionZ = 0
        signF = lambda x: x/np.abs(x)
        if iIntersectPlane < 2:
            refractionX = sign*refractionCos
            if np.abs(direction[1]) < eps:
                refractionY = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionX**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionX**2)
            else:
                refractionYZrel = direction[2]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionX**2) / (1 + refractionYZrel**2))
                refractionZ = refractionY*refractionYZrel
        elif iIntersectPlane < 4:
            refractionY = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionZ = signF(direction[2]) * np.sqrt(1 - refractionY**2)
            elif np.abs(direction[2]) < eps:
                refractionZ = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionY**2)
            else:
                refractionZXrel = direction[0]/direction[2]
                refractionZ = signF(direction[2]) * np.sqrt((1 - refractionY**2) / (1 + refractionZXrel**2))
                refractionX = refractionZ * refractionZXrel
        else:
            refractionZ = sign*refractionCos
            if np.abs(direction[0]) < eps:
                refractionX = 0
                refractionY = signF(direction[1]) * np.sqrt(1 - refractionZ**2)
            elif np.abs(direction[1]) < eps:
                refractionY = 0
                refractionX = signF(direction[0]) * np.sqrt(1 - refractionZ**2)
            else:
                refractionYXrel = direction[0]/direction[1]
                refractionY = signF(direction[1]) * np.sqrt((1 - refractionZ**2) / (1 + refractionYXrel**2))
                refractionX = refractionYXrel * refractionY

        updDirection = np.array([refractionX, refractionY, refractionZ])
        return updDirection/npl.norm(updDirection)


    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    def triangleAreaBy3Sides(a, b, c):
        halfPerimeter = (a+b+c)/2
        return np.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c))

    def triangleAreaBy3Points(A, B, C):
        a = npl.norm(A-B)
        b = npl.norm(C-B)
        c = npl.norm(C-A)
        return triangleAreaBy3Sides(a, b, c)

    def triangleAmplitudeNorm(iCurRay, jCurRay):
        quarter1 = (None, 1e111)
        quarter2 = (None, 1e111)
        quarter3 = (None, 1e111)
        quarter4 = (None, 1e111)

        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isUnderZSurface(iRay, jRay) and not isLost(iRay,jRay):
                    distV = location(iCurRay, jCurRay) - location(iRay, jRay)
                    distance = npl.norm(distV)
                    if distance > 1e-7:
                        if distV[0] >= 0 and distV[1]  < 0 and distance < quarter1[1]:
                            quarter1 = ((iRay, jRay), distance)
                        if distV[0] >= 0 and distV[1] >= 0 and distance < quarter2[1]:
                            quarter2 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1]  < 0 and distance < quarter3[1]:
                            quarter3 = ((iRay, jRay), distance)
                        if distV[0]  < 0 and distV[1] >= 0 and distance < quarter4[1]:
                            quarter4 = ((iRay, jRay), distance)

        closestRays = list(filter(lambda x: x[0] != None, [quarter1, quarter2, quarter3, quarter4]))

        if len(closestRays) < 1:
            raise ValueError("There is no triangles are found!")

        finishAreas = []
        startAreas  = []
        for i in range(len(closestRays)):
            finishDist = npl.norm(location(*closestRays[i-1][0]) - location(*closestRays[i][0]))
            finishAreas.append(triangleAreaBy3Sides(closestRays[i-1][1], closestRays[i][1], finishDist))
            startAreas.append(
                triangleAreaBy3Points(
                    initialLocation(iCurRay,jCurRay), 
                    initialLocation(*closestRays[i-1][0]), 
                    initialLocation(*closestRays[i][0])
                    ))

    #Triangle norm where we take triangles on the start unit sphere and then try to find their images
    def triangleAmplitudeNormReverse(iCurRay, jCurRay):

        finishAreas = []
        startAreas  = []
        startAreasAppend = lambda iR0, jR0, iR1, jR1: \
            startAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(initialLocation(iR0,jR0) - initialLocation(iR1,jR0))
                 ,npl.norm(initialLocation(iR1,jR0) - initialLocation(iR0,jR1))
                 ,npl.norm(initialLocation(iR0,jR0) - initialLocation(iR0,jR1))
                 ))
        finishAreasAppend = lambda iR0, jR0, iR1, jR1: \
            finishAreas.append(
               triangleAreaBy3Sides(
                  npl.norm(location(iR0,jR0) - location(iR1,jR0))
                 ,npl.norm(location(iR1,jR0) - location(iR0,jR1))
                 ,npl.norm(location(iR0,jR0) - location(iR0,jR1))
                 ))
        areasAppend = lambda iR, jR, iR1, jR1: \
             startAreasAppend(iR, jR, iR1, jR1) == \
             finishAreasAppend(iR, jR, iR1, jR1)

        isLost0 = lambda iR, jR: isLost(iR,jR) and (not isUnderZSurface(iR,jR))

        isNotLost = lambda iR, jR, iR0, jR0: \
                not (isLost0(iR,jR) or isLost0(iR0,jR0) or isLost0(iR,jR0) or isLost0(iR0,jR))

        if iCurRay+1 < 2*nTheta-1 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay+1 < 2*nPhi-1 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay+1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay+1)
        if iCurRay-1 >= 0 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay-1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay-1, jCurRay-1)
        if iCurRay+1 < 2*nTheta-1 and jCurRay-1 >= 0 \
           and isNotLost(iCurRay, jCurRay, iCurRay+1, jCurRay-1):
             areasAppend(iCurRay, jCurRay, iCurRay+1, jCurRay-1)

        if len(startAreas) < 3:
            return False, None
        #print("Areas:", np.array(finishAreas), " ", np.array(startAreas))
        return True, np.sqrt(np.median(np.array(finishAreas)) / np.median(np.array(startAreas)))

    def approxParamsByClosestRay(point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
        minD = dists[curMinimumIdx]
        minId = curMinimumIdx

        outTime = rayTime(*minId)
        outAmp  = amplitude(*minId)
        outDirection = initialDirection(*minId)

        return outTime, outAmp, outDirection, minD

    def distanceNorm(i):
        return True, distance(i)

    def approxParamsByClosestRays(n, point):
        dists = np.zeros((2*nTheta-1, 2*nPhi-1))+1e111
        for iRay in range(2*nTheta-1):
            for jRay in range(2*nPhi-1):
                if not isLost(iRay, jRay) and not isUnderZSurface(iRay,jRay):
                    dists[iRay, jRay] = npl.norm(point - location(iRay,jRay)[:2])

        mins    = []
        minsD   = np.zeros(n)
        weights = np.zeros(n)
        for i in range(n):
            curMinimumIdx = np.unravel_index(np.argmin(dists), dists.shape)
            minsD[i] = dists[curMinimumIdx]
            mins.append(curMinimumIdx)
            dists[curMinimumIdx] = 1e111
            weights[i] = scs.multivariate_normal.pdf(point, location(*curMinimumIdx)[:2], np.eye(2)*minsD[0])


        outTime = 0
        outAmp  = 0
        outDirection = np.zeros(3)
        for i in range(n):
            outTime += (weights[i]/np.sum(weights)) * rayTime(*mins[i])
            outAmp  += (weights[i]/np.sum(weights)) * amplitude(*mins[i])
            outDirection += (weights[i]/np.sum(weights)) * direction(*mins[i])

        return outTime, outAmp, outDirection


    acousticImpedance = vp * density

    viewLengthX  = nCubes[0]
    viewLengthY  = nCubes[1]
    viewLength = 1000
    viewLengthZ = 800
    viewXY = np.zeros((viewLength,viewLength))
    viewXZ = np.zeros((viewLength,viewLengthZ))
    viewYZ = np.zeros((viewLength,viewLengthZ))
    constZ = startRaysLocation[2]
    constY = startRaysLocation[1]
    constX = startRaysLocation[0]
    x = 0
    y = 0
    z = -800
    for i in list(range(viewLength))*rCube:
        for j in list(range(viewLength))*rCube:
            viewXY[i,j] = acousticImpedance[cubeIndex(np.array([x+i,y+j,constZ]))]
            if j < viewLengthZ:
                viewXZ[i,j] = acousticImpedance[cubeIndex(np.array([x+i,constY,z+j]))]
                viewYZ[i,j] = acousticImpedance[cubeIndex(np.array([constX,y+i,z+j]))]

    print(nCubes[0]*25, nCubes[1]*25, "Q")
    labels = lambda nLabels, i: np.arange(startRaysLocation[i] - viewLength//2, startRaysLocation[i] + viewLength//2 + 0.001, viewLength/(nLabels-1), dtype=np.int64)
    labelsX = lambda nLabels: np.arange(0, viewLengthX+1e-7, viewLengthX/(nLabels-1), dtype=np.int64)
    labelsY = lambda nLabels: np.arange(0, viewLengthY+1e-7, viewLengthY/(nLabels-1), dtype=np.int64)
    labelsZ = lambda nLabels: np.arange(startRaysLocation[2], startRaysLocation[2] + viewLengthZ + 1, viewLengthZ/(nLabels-1), dtype=np.int64)

    #plt.imshow(viewXY, cmap='nipy_spectral', aspect='auto', origin='lower')
    #plt.colorbar()
    #xLocs, xLabels = plt.xticks()
    #yLocs, yLabels = plt.yticks()
    #xLocs = xLocs[1:]
    #yLocs = yLocs[1:]
    #plt.yticks(yLocs, labelsX(len(yLocs)))
    #plt.xticks(xLocs, labelsY(len(xLocs)))
    #plt.title("XY. Z = 800. Strike at " + str(startRaysLocation))
    #plt.show()

    #plt.imshow(viewXZ, cmap='nipy_spectral', aspect='auto', origin='lower')
    #xLocs, _ = plt.xticks()
    #yLocs, _ = plt.yticks()
    #xLocs = xLocs[1:]
    #yLocs = yLocs[1:]
    #plt.xticks(xLocs, labelsX(len(xLocs)))
    #plt.yticks(yLocs, labelsZ(len(yLocs)))
    #plt.title("XZ. Y = 3625. Strike at " + str(startRaysLocation))
    #plt.colorbar()
    #plt.show()

    #plt.imshow(viewYZ, cmap='nipy_spectral', aspect='auto', origin='lower')
    #xLocs, _ = plt.xticks()
    #yLocs, _ = plt.yticks()
    #xLocs = xLocs[1:]
    #yLocs = yLocs[1:]
    #plt.xticks(xLocs, labelsY(len(xLocs)))
    #plt.yticks(yLocs, labelsZ(len(yLocs)))
    #plt.title("YZ. X = 3125. Strike at " + str(startRaysLocation))
    #plt.colorbar()
    #plt.show()


    zSurface = mostNegativeCube[2] + nCubes[2]*rCube
    isUnderZSurface = lambda i: location(i)[2] < zSurface
    isInsideXGrid   = lambda i:\
        location(i)[0] >= mostNegativeCube[0] and\
        location(i)[0] < mostNegativeCube[0] + nCubes[0]*rCube
    isInsideYGrid   = lambda i:\
        location(i)[1] >= mostNegativeCube[1] and\
        location(i)[1] < mostNegativeCube[1] + nCubes[1]*rCube
    isInsideGrid = lambda i:\
        isUnderZSurface(i) and isInsideXGrid(i) and isInsideYGrid(i)
    isInsideXYGrid = lambda i:\
        isInsideXGrid(i) and isInsideYGrid(i)
    #print("direction(28, 72)=",direction(28, 72))
    #raise ValueError()
    def traceRay(iRay):
        #print("Ray ", iRay*(2*nPhi-1), " from ", (2*nTheta-1))
        #print(iRay, jRay, " Ray ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
        reasonOfEnd = 0# means succesful ray
                       # 1 means Full Reflection
                       # 2 means out of borders
        isFullReflection = False
        while isInsideGrid(iRay) and not isLost(iRay):
            #print("direction(iRay, jRay)=",direction(iRay, jRay))

            if np.dot(direction(iRay), normals[4]) <= 0:
                #print("RayLost: ray goes down!")
                reasonOfEnd = 2
                rayLost(iRay)

            #Trace ray through heterogeneous environment
            planes = closestPlanes(location(iRay))
            distanceToPlane = 1e100
            iIntersectPlane = 0
            lengthNormalToPlane = 0
            for iNormal in range(normals.shape[0]):
                if np.dot(direction(iRay), normals[iNormal]) > 0:
                    curLengthNormalToPlane = np.abs(location(iRay)[idxsD[iNormal]] - planes[iNormal])
                    cosRayPlane = np.dot(direction(iRay), normals[iNormal])
                    curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                    if curDistanceToPlane < distanceToPlane:
                        distanceToPlane = curDistanceToPlane
                        iIntersectPlane = iNormal
                        lengthNormalToPlane = curLengthNormalToPlane
            stepsToIntersect = np.abs(lengthNormalToPlane / direction(iRay)[idxsD[iIntersectPlane]])
            #print("stepsToIntersect=",stepsToIntersect)
            #print("direction(iRay, jRay)=",direction(iRay, jRay))
            updLocation  = location(iRay) + (stepsToIntersect + 1e-7)*direction(iRay)
            prevLocation = np.copy(location(iRay))
            setLocation(iRay, updLocation)
            addDistance(iRay, npl.norm(prevLocation - updLocation))

            #Time update
            time = npl.norm(updLocation - prevLocation)/vp[cubeIndex(prevLocation)]
            addTime(iRay, time)

            #Snell's law
            if isInsideGrid(iRay):
                refractionIndexFrom = acousticImpedance[cubeIndex(prevLocation)]
                refractionIndexTo   = acousticImpedance[cubeIndex(updLocation )]
                incidenceSin  = np.sqrt(1 - np.dot(direction(iRay), normals[iIntersectPlane])**2)
                if refractionIndexTo*incidenceSin > refractionIndexFrom:
                    rayLost(iRay)
                    isFullReflection = True
                    reasonOfEnd = 1
                else:
                    refractionSin = refractionIndexTo/refractionIndexFrom * incidenceSin
                    refractionCos = np.sqrt(1 - refractionSin**2)
                    updDirection = refractionsDirection(refractionCos, direction(iRay), iIntersectPlane)
                    setDirection(iRay, updDirection)
            #Fresnel's law
                    incidenceCos = np.sqrt(1 - incidenceSin**2)
                    fresnelCoeff = 2*refractionIndexTo*incidenceCos / \
                        (refractionIndexTo*incidenceCos + refractionIndexFrom*refractionCos)
                    updAmplitude = scaleAmplitude(iRay,fresnelCoeff)


            #Debug
        #for indTrace in range(traces):
        #    if iRay == iTrace[indTrace]:
        #        path[indTrace,nPath[indTrace],:] = updLocation
        #        pathAmps[indTrace,nPath[indTrace]] = updAmplitude
        #        pathTimes[indTrace] += time
        #        nPath[indTrace] += 1
        if isUnderZSurface(iRay):
            #print("underZSurface!")
            rayLost(iRay)

        if isFullReflection:
            return 1
        if not isInsideXYGrid(i):
            return 2
        return reasonOfEnd

    #Initialize rays
    setAmplitudes(initialAmplitude)
    shiftAllLocations(startRaysLocation)
    print("amp", amplitudes()) 

    #for iRay in range(receiversCoordinates.shape[0]):
    #    print("RAY",iRay)
    #    goal = np.zeros(3)
    #    goal[:2] = receiversCoordinates[iRay,:]
    #    print("goal:", goal)
    #    initialVector = goal - location(iRay)
    #    initialDirection = initialVector/npl.norm(initialVector)
    #    setInitDirection(iRay,initialDirection)
    #    print("START: ", receiversCoordinates[iRay,:], startRaysLocation, direction(iRay), location(iRay))

    #    nOfSteps = 0
    #    distanceToReceiver = 1e111
    #    bestRay = np.zeros((10))
    #    while tolerance < distanceToReceiver and maxSteps > nOfSteps:
    #        print(nOfSteps, tolerance, distanceToReceiver)
    #        traceRay(iRay)
    #        if isLost(iRay) and nOfSteps == 0:
    #            print("instant lost")
    #            rays[iRay,:] = 0
    #            setInitDirection(iRay, np.array([0,0,1]))
    #            setLocation(iRay, startRaysLocation)
    #        else:
    #            nOfSteps += 1
    #            curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
    #            print("curDistance", curDistanceToReceiver)
    #            if isLost(iRay) or curDistanceToReceiver >= distanceToReceiver:
    #                if isLost(iRay):
    #                    print("lost")
    #                else:
    #                    print("distance")
    #                randDist = np.random.rand(1)
    #                randAngle = 2*np.pi*np.random.rand(1)
    #                randShift = np.array([np.sin(randAngle), np.cos(randAngle), 0]) * randDist * distanceToReceiver
    #                newDirectionVector = (goal + randShift) - startRaysLocation
    #                rays[iRay,:] = 0
    #                setInitDirection(iRay, newDirectionVector/npl.norm(newDirectionVector))
    #                setLocation(iRay,startRaysLocation)
    #            else:
    #                print("success")
    #                distanceToReceiver = curDistanceToReceiver
    #                bestRay = np.copy(rays[iRay,:])
    #                newGoal = np.zeros(3)
    #                newGoal[:2] = goal[:2] - 0.5*(goal[:2] - location(iRay)[:2])
    #                newDirectionVector = newGoal - startRaysLocation
    #                print("newDirectionVector", isLost(iRay), (newGoal), startRaysLocation, location(iRay))
    #                rays[iRay,:] = 0
    #                newDirection = newDirectionVector/npl.norm(newDirectionVector)
    #                setInitDirection(iRay, newDirection)
    #                setLocation(iRay, startRaysLocation)

    print("shape!", receiversCoordinates.shape)
    pathGradientDescents = np.zeros((receiversCoordinates.shape[0],10000,3))
    pathGDIsLost = np.zeros((receiversCoordinates.shape[0],10000)) - 1
    ###LINESEARCH###
    for iRay in range(receiversCoordinates.shape[0]):
        print("RAY",iRay)
        scalingStep = 0.1
        lGoal = np.zeros(3)
        rGoal = np.zeros(3)
        lGoal[:2] = startRaysLocation[:2]
        rGoal[:2] = receiversCoordinates[iRay,:2] 
        rGoal += rGoal - lGoal
        goal = lambda l, r: r + 0.5*(l - r)
        initialVector = goal(lGoal, rGoal) - startRaysLocation
        initialDirection = initialVector/npl.norm(initialVector)
        setInitDirection(iRay,initialDirection)
        pathGradientDescents[iRay,0,:] = goal(lGoal, rGoal)

        nOfSteps = 0
        distanceToReceiver = 1e111
        bestRay = np.zeros((13))
        prevLGoal = lGoal
        print(nOfSteps, distanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
        history = ['L','L','L']
        historyGoals = []
        def historyUpdate(x):
            history[:2] = history[1:3]
            history[2] = x
        while tolerance < distanceToReceiver and maxSteps > nOfSteps:
            traceRay(iRay)
            pathGDIsLost[iRay,nOfSteps] = isLost(iRay)
            pathGradientDescents[iRay,nOfSteps,:] = goal(lGoal, rGoal)
            nOfSteps += 1
            curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
            if history == ["S","F","F"]:
                scalingStep /= 7
                lGoal, rGoal = historyGoals[-1]
            if (isLost(iRay) or curDistanceToReceiver > distanceToReceiver) and history != ["S","F","F"] :
                historyUpdate("F")
                #if isLost(iRay):
                #    print(nOfSteps, "L", curDistanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
                #else:
                #    print(nOfSteps, "F", curDistanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
                lGoal = prevLGoal
                rGoal = rGoal + scalingStep*(lGoal-rGoal)
                initialVector = goal(lGoal, rGoal) - startRaysLocation
                initialDirection = initialVector/npl.norm(initialVector)
                rays[iRay,:] = 0
                setAmplitude(iRay, initialAmplitude)
                setInitDirection(iRay, initialDirection)
                setLocation(iRay, startRaysLocation)
            else:
                historyUpdate("S")
                historyGoals.append((lGoal, rGoal))
                #print("curDistance", curDistanceToReceiver)
                #print("success")
                #print(nOfSteps, "success", curDistanceToReceiver, npl.norm(lGoal[:2] - receiversCoordinates[iRay,:]), npl.norm(rGoal[:2] - receiversCoordinates[iRay,:]))
                distanceToReceiver = curDistanceToReceiver
                bestRay = np.copy(rays[iRay,:])
                prevLGoal = lGoal
                lGoal = lGoal + scalingStep*(rGoal-lGoal)
                initialVector = goal(lGoal, rGoal) - startRaysLocation
                pathGradientDescents[iRay,nOfSteps,:] = goal(lGoal, rGoal)
                pathGDIsLost[iRay,nOfSteps] = isLost(iRay)
                initialDirection = initialVector/npl.norm(initialVector)
                rays[iRay,:] = 0
                setAmplitude(iRay, initialAmplitude)
                setInitDirection(iRay, initialDirection)
                setLocation(iRay, startRaysLocation)

        rays[iRay,:] = np.copy(bestRay)
        #print("bestRay:", bestRay)
        print("distanceToreceiver", distanceToReceiver)
        print(rayTime(iRay))

        if distanceToReceiver > 10:
            nSteps = 100
            dirVector = receiversCoordinates[iRay,:] - startRaysLocation[:2]
            dirVector = dirVector/nSteps 
            iStep = 0

            while iStep < 5*nSteps:
                iStep+=1
                goal = np.zeros(3)
                goal[:2] = startRaysLocation[:2] + dirVector*iStep
                initialVector = goal - startRaysLocation
                nOfSteps += 1
                initialDirection = initialVector/npl.norm(initialVector)
                rays[iRay,:] = 0
                setAmplitude(iRay, initialAmplitude)
                setInitDirection(iRay, initialDirection)
                setLocation(iRay, startRaysLocation)
                traceRay(iRay)
                pathGradientDescents[iRay,nOfSteps+1,:] = goal
                pathGDIsLost[iRay,nOfSteps+1] = isLost(iRay)
                curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
                if (not isLost(iRay)) and curDistanceToReceiver < distanceToReceiver:
                    distanceToReceiver = curDistanceToReceiver
                    bestRay = np.copy(rays[iRay,:])
            rays[iRay,:] = np.copy(bestRay)
            print("distanceToreceiver", distanceToReceiver)

            reason = 0
            while reason != 2:
                    iStep += 1 
                    goal = np.zeros(3)
                    goal[:2] = startRaysLocation[:2] + dirVector*iStep
                    goal[2] = startRaysLocation[2] - npl.norm(dirVector*iStep - dirVector*(iStep-1))/2
                    initialVector = goal - startRaysLocation
                    nOfSteps += 1
                    initialDirection = initialVector/npl.norm(initialVector)
                    rays[iRay,:] = 0
                    setAmplitude(iRay, initialAmplitude)
                    setInitDirection(iRay, initialDirection)
                    setLocation(iRay, startRaysLocation)
                    reason = traceRay(iRay)
                    pathGradientDescents[iRay,nOfSteps+1,:] = goal
                    pathGDIsLost[iRay,nOfSteps+1] = isLost(iRay)
                    curDistanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
                    if (not isLost(iRay)) and curDistanceToReceiver < distanceToReceiver:
                        distanceToReceiver = curDistanceToReceiver
                        bestRay = np.copy(rays[iRay,:])
        rays[iRay,:] = np.copy(bestRay)
        print("distanceToreceiver", distanceToReceiver)
        print(rayTime(iRay))



    #for iRay in range(receiversCoordinates.shape[0]):
    #    print("RAY",iRay)
    #    #if iRay > 1:
    #    #        raise ValueError()
    #    distanceToReceiver = 1e111
    #    bestRay = np.zeros((13))
    #    
    #    goal = np.zeros(3)

    #    #goal[:2] = startRaysLocation[:2]
    #    #setAmplitude(iRay, initialAmplitude)
    #    #setInitDirection(iRay, np.array([0,0,1]))
    #    #setLocation(iRay, startRaysLocation)
    #    #traceRay(iRay)
    #    #distanceToReceiver = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
    #    #bestRay = np.copy(rays[iRay,:])
    #    #pathGradientDescents[iRay,0,:] = goal
    #    #pathGDIsLost[iRay,0] = isLost(iRay)
    #    
    #    goal[:2] = receiversCoordinates[iRay,:]
    #    initialDirection = goal - startRaysLocation
    #    initialDirection = initialDirection/npl.norm(initialDirection)
    #    rays[iRay,:] = 0
    #    setAmplitude(iRay, initialAmplitude)
    #    setInitDirection(iRay, initialDirection)
    #    setLocation(iRay, startRaysLocation)
    #    traceRay(iRay)
    #    pathGradientDescents[iRay,0,:] = goal
    #    pathGDIsLost[iRay,0] = isLost(iRay)
    #    
    #    curDistance = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
    #    if (not isLost(iRay)) and (distanceToReceiver > curDistance):
    #        distanceToReceiver = curDistance
    #        bestRay = np.copy(iRay)
    #    maxSteps = 5000
    #    nOfSteps = 0
    #    #stepLength = [100,10,1,1000]
    #    stepLength = [100,10,1,1000]
    #    bestGoal = goal
    #    while nOfSteps < maxSteps and distanceToReceiver > 10:
    #        curStepLength = stepLength[nOfSteps%4]
    #        nOfSteps += 1
    #        randV = (np.random.rand(2) - 0.5)
    #        #print("randV", randV)
    #        randStep = randV / npl.norm(randV) * curStepLength * np.random.rand()
    #        #print("randStep",randStep, npl.norm(randStep))
    #        goal[:2] = bestGoal[:2] + randStep
    #        #print(nOfSteps)
    #        initialDirection = goal - startRaysLocation
    #        rays[iRay,:] = 0
    #        setAmplitude(iRay, initialAmplitude)
    #        initialDirection = initialDirection/npl.norm(initialDirection)
    #        setInitDirection(iRay, initialDirection)
    #        setLocation(iRay, startRaysLocation)
    #        traceRay(iRay)
    #        pathGradientDescents[iRay,nOfSteps,:] = goal
    #        pathGDIsLost[iRay,nOfSteps] = isLost(iRay)
    #        curDistance = npl.norm(location(iRay)[:2] - receiversCoordinates[iRay,:])
    #        #print("curDistance", curDistance)
    #        if (not isLost(iRay)) and (distanceToReceiver > curDistance):
    #            distanceToReceiver = curDistance
    #            bestRay = np.copy(iRay)
    #            bestGoal = goal
    #            print("SUCCESS---------------------------------------------------", nOfSteps, curStepLength, distanceToReceiver)
    #            
    #    print("distanceToreceiver", distanceToReceiver)
    #    print(rayTime(iRay))


    #initialArrangement   = np.copy(rays[:,1:4])
    #initialArrangement2D = np.copy(rays[:,1:4])
    #initialDirection2D   = np.copy(rays[:,4:7])

    #print("TRIANGLE AMPLITUDE NORM IS DISABLED!")
    triNorms = np.zeros((receiversCoordinates.shape[0]))
    addToLost = []
    def normByDistance(iRay):
            #print("Triangle amplitude update ", iRay*(2*nPhi-1) + jRay, " from ", (2*nTheta-1)*(2*nPhi-1))
            #Triangle amplitude update
            isNormExist, rayNorm = distanceNorm(iRay)
            if isNormExist:
               scaleAmplitude(iRay,1/rayNorm)
               #print("triNorm:", rayNorm)
               triNorms[iRay] = rayNorm

    for iRay in range(receiversCoordinates.shape[0]):
        normByDistance(iRay)

    #get signal for receivers
    receiversTimes = times()
    receiversAmps  = amplitudes()
    receiversTensorAmps = np.zeros((receiversCoordinates.shape[0],3,6))
    distancesToRays = np.zeros(receiversCoordinates.shape[0])
    for i in range(receiversCoordinates.shape[0]):
        #print("Receiver ", i, " from ", receiversCoordinates.shape[0])
        #curTime, curAmplitude, curDirection, distanceToRay = approxParamsByClosestRay(receiversCoordinates[i,:])
        #receiversTimes[i] = time(iRay)
        #receiversAmps[i]  = curAmplitude
        distancesToRays[i] = npl.norm(receiversCoordinates[i,:] - location(i)[:2])
        curDirection = initDirection(i)

        xCos = np.dot(curDirection, normals[1])
        yCos = np.dot(curDirection, normals[3])
        zCos = np.dot(curDirection, normals[4])
        receiversTensorAmps[i,0,0] =       6*xCos**3          - 3*xCos
        receiversTensorAmps[i,1,0] =       6*(xCos**2)*yCos   -   yCos
        receiversTensorAmps[i,2,0] =       6*(xCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,1] =       6*xCos*(yCos**2)   -   xCos
        receiversTensorAmps[i,1,1] =       6*(yCos**3)        - 3*yCos
        receiversTensorAmps[i,2,1] =       6*(yCos**2)*zCos   -   zCos

        receiversTensorAmps[i,0,2] =       6*xCos*(zCos**2)   -   xCos
        receiversTensorAmps[i,1,2] =       6*(yCos)*(zCos**2) -   yCos
        receiversTensorAmps[i,2,2] =       6*(zCos**3)        - 3*zCos

        receiversTensorAmps[i,0,3] =       6*(xCos**2)*(yCos) -   yCos
        receiversTensorAmps[i,1,3] =       6*(yCos**2)*(xCos) -   xCos
        receiversTensorAmps[i,2,3] =       6*xCos*yCos*(zCos)

        receiversTensorAmps[i,0,4] =      (6*(xCos**2)*(zCos) -   zCos)
        receiversTensorAmps[i,1,4] =      (6*xCos*yCos*(zCos)         )
        receiversTensorAmps[i,2,4] =      (6*(zCos**2)*xCos   -   xCos)

        receiversTensorAmps[i,0,5] =      (6*xCos*yCos*(zCos)         ) 
        receiversTensorAmps[i,1,5] =      (6*(yCos**2)*(zCos)  -  zCos)
        receiversTensorAmps[i,2,5] =      (6*(zCos**2)*yCos    -  yCos)

    gaussWavelet = lambda mean, var: lambda x:\
              -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
    gaussWavelet2 = lambda mean, var: lambda x:\
          (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))
                sigma=1/fdPWave/5
                length = receiverSignals.shape[3]

                gwt = gaussWavelet2(waveComing/fd,sigma)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt

                #plt.plot(receiverSignals[iR, iCh, iTensor, :])
                #plt.plot(np.arange(0,gwt.shape[0])*(fd/gwt.shape[0]), np.abs(np.fft.fft(gwt)))
                #plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1

    print("should be second order")
    return receiverSignals, distancesToRays, rays, path, pathAmps, pathTimes, nPath, pathGradientDescents, pathGDIsLost

def genEnv(nCubes, env='equal'):
    vp      = np.zeros((nCubes,nCubes,nCubes))
    density = np.zeros((nCubes,nCubes,nCubes))
    if env=='equal':
        vp      += 1000
        density += 1000
    else:
        for ind in range(nCubes**3):
            i = ind // nCubes**2
            j = (ind - i*nCubes**2) // nCubes
            k = ind %  nCubes

            vp[i,j,k] = 3600
            density[i,j,k] = 2150
            factors = primefactors(i+j+k)
            if len(factors) == 1:
                vp[i,j,k] *= factors[0]/3
    return vp, density

def simpleMedium(nCubes):
    vp      = np.zeros(nCubes) + 1000
    density = np.zeros(nCubes) + 1000
    #vp[:,:,round(nCubes*0.3):round(nCubes*0.7)] *= 1.7
    return vp, density

#spherePoints = fibonacciSphere(1000, False)
#print(spherePoints.shape)
#
#fig = plt.figure()
#ax = fig.add_subplot(111,projection='3d')
#ax.scatter(spherePoints[:,0], spherePoints[:,1], spherePoints[:,2])
#plt.show()


leftDownCube = np.array([-1000,-1000,0])
nCubes = (20,20,20)
rCube = 100
vp, density = simpleMedium(nCubes)

nRays = 37
initialAmplitude = 17
startRaysLocation = np.array([0,0,0])
iTrace = [0,0,1]
jTrace = [2,4,4]
iTrace = np.arange((2*nRays-1)**2)//(2*nRays-1)
jTrace = np.arange((2*nRays-1)**2)%(2*nRays-1)
startAngle = (1/5.7)*np.pi
receiversCoordinates = np.array([[100,0],[33,33],[77,-133]])

#rs, rays, path, pathAmps, pathTimes, nPath, initialArrangement = \
#    rayTracer(receiversCoordinates,200,8,10,nCubes,rCube,vp,density,startRaysLocation,initialAmplitude,
#                   leftDownCube,nRays,nRays,startAngle,iTrace,jTrace)

#plotPath(20,100,leftDownCube,path,nPath,c=['xz','yz','xy'])
#plotMediumPath(20, 100, vp, density, leftDownCube , path, nPath,
#        axes=['xz','yz','xy']
#       #,amps=pathAmps
#       #,times=pathTimes
#       )

#fig = plt.figure()
#ax = fig.add_subplot(111,projection='3d')
#ax.scatter(rays[:,1], rays[:,2], rays[:,3])
#plt.show()

def testInitialDistribution(initialArrangement):
    s = 3
    plt.scatter(initialArrangement[:,0], initialArrangement[:,1], s=s)
    plt.title("InitialArrangement XY")
    plt.show()
    plt.scatter(initialArrangement[:,0], initialArrangement[:,2], s=s)
    plt.title("InitialArrangement XZ")
    plt.show()
    plt.scatter(initialArrangement[:,1], initialArrangement[:,2], s=s)
    plt.title("InitialArrangement YZ")
    plt.show()

#testInitialDistribution(initialArrangement)
#plt.scatter(spherePoints[:,0], spherePoints[:,1], s=1)
#plt.show()
#plt.scatter(spherePoints[:,0], spherePoints[:,2], s=1)
#plt.show()
#plt.scatter(spherePoints[:,1], spherePoints[:,2], s=1)
#plt.show()

def nearestReceiver(location, xCoords, yCoords, zCoords):
    dist = lambda i: npl.norm(np.array([xCoords[i], yCoords[i]]) - location)
    return np.argmin(np.vectorize(dist)(np.arange(xCoords.shape[0])))

def testTensorsAmplitudeDist(iTensor, startAngle, nTheta, nPhi):
    rays = np.zeros((2*nTheta-1, 2*nPhi-1, 4))

    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]

    thetaSpace = np.linspace(startAngle,np.pi/2,nTheta)
    phiSpace   = np.linspace(startAngle,np.pi/2,nPhi  )

    for iTheta in range(nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta,iPhi, np.array([np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)]) )
            #print("setLocation:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])
            #print("theta: ", theta, " phi: ", phi, " loc:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])
            #print("theta: ", iTheta, " phi: ", iPhi, " loc:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])

    rays[nTheta:2*nTheta-1,:,:] = np.copy(rays[:nTheta-1,:,:])
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = np.copy(rays[:,:nPhi-1,:])
    rays[:,nPhi:2*nPhi-1,2] *= -1

    receiversTensorAmps = np.zeros((2*nTheta-1, 2*nPhi-1, 3, 6))
    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    for iTheta in range(2*nTheta-1):
        for iPhi in range(2*nPhi-1):
            curDirection = location(iTheta, iPhi)
            xCos = np.dot(curDirection, normals[1])
            yCos = np.dot(curDirection, normals[3])
            zCos = np.dot(curDirection, normals[4])

            receiversTensorAmps[iTheta,iPhi,0,0] = 6*xCos**3 - 3*xCos
            receiversTensorAmps[iTheta,iPhi,1,0] = 6*(xCos**2)*yCos - yCos
            receiversTensorAmps[iTheta,iPhi,2,0] = 6*(xCos**2)*zCos - zCos

            receiversTensorAmps[iTheta,iPhi,0,1] = 6*xCos*(yCos**2) - xCos
            receiversTensorAmps[iTheta,iPhi,1,1] = 6*(yCos**3)      - 3*yCos
            receiversTensorAmps[iTheta,iPhi,2,1] = 6*(yCos**2)*zCos - zCos

            receiversTensorAmps[iTheta,iPhi,0,2] = 6*xCos*(zCos**2)     - xCos
            receiversTensorAmps[iTheta,iPhi,1,2] = 6*(yCos)*(zCos**2)   - yCos
            receiversTensorAmps[iTheta,iPhi,2,2] = 6*(zCos**3)          - 3*zCos

            receiversTensorAmps[iTheta,iPhi,0,3] = 6*(xCos**2)*(yCos)     - yCos
            receiversTensorAmps[iTheta,iPhi,1,3] = 6*(yCos**2)*(xCos)   - xCos
            receiversTensorAmps[iTheta,iPhi,2,3] = 6*xCos*yCos*(zCos)

            receiversTensorAmps[iTheta,iPhi,0,4] = 6*(xCos**2)*(zCos)   - zCos
            receiversTensorAmps[iTheta,iPhi,1,4] = 6*xCos*yCos*(zCos)
            receiversTensorAmps[iTheta,iPhi,2,4] = 6*(zCos**2)*xCos     - xCos

            receiversTensorAmps[iTheta,iPhi,0,5] = 6*xCos*yCos*(zCos)
            receiversTensorAmps[iTheta,iPhi,1,5] = 6*(yCos**2)*(zCos)   - zCos
            receiversTensorAmps[iTheta,iPhi,2,5] = 6*(zCos**2)*yCos     - yCos

    receiversTensorAmps = receiversTensorAmps.reshape(-1, 3, 6)
    #plt.scatter(receiversTensorAmps[:,0,1], receiversTensorAmps[:,1,1])
    i = iTensor
    #ax = Axes3D(plt.figure())
    #ax.scatter(receiversTensorAmps[:,0,i], receiversTensorAmps[:,1,i], receiversTensorAmps[:,2,i])
    #ax.set_xlabel("x")
    #ax.set_ylabel("y")
    #ax.set_zlabel("z")
    tensorNames = ['xx', 'yy', 'zz', 'xy', 'xz', 'yz'] 
    sizeScatter = 7
    plt.scatter(receiversTensorAmps[:,0,i], receiversTensorAmps[:,1,i], s=sizeScatter)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(tensorNames[iTensor])
    #plt.show()
    plt.figure()
    plt.scatter(receiversTensorAmps[:,0,i], receiversTensorAmps[:,2,i], s=sizeScatter)
    plt.xlabel("x")
    plt.ylabel("z")
    plt.title(tensorNames[iTensor])
    plt.figure()
    plt.scatter(receiversTensorAmps[:,1,i], receiversTensorAmps[:,2,i], s=sizeScatter)
    plt.xlabel("y")
    plt.ylabel("z")
    plt.title(tensorNames[iTensor])
    plt.show()

    #plt.scatter(receiversTensorAmps[:,0,i], np.arange(np.shape(receiversTensorAmps[:,1,i])[0]), s=sizeScatter)
    #plt.xlabel("x")
    #plt.ylabel("y")
    #plt.show()

#testTensorsAmplitudeDist(0, np.pi*0, nRays, nRays)

def testTensorsAmplitudeDist0(iTensor, iChannel, startAngle, nTheta, nPhi):
    rays = np.zeros((2*nTheta-1, 2*nPhi-1, 4))

    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]

    thetaSpace = np.linspace(startAngle,np.pi/2,nTheta)
    phiSpace   = np.linspace(startAngle,np.pi/2,nPhi  )

    for iTheta in range(nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta,iPhi, np.array([np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)]) )
            #print("setLocation:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])
            #print("theta: ", theta, " phi: ", phi, " loc:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])
            #print("theta: ", iTheta, " phi: ", iPhi, " loc:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])

    rays[nTheta:2*nTheta-1,:,:] = np.copy(rays[:nTheta-1,:,:])
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = np.copy(rays[:,:nPhi-1,:])
    rays[:,nPhi:2*nPhi-1,2] *= -1

    receiversTensorAmps = np.zeros((2*nTheta-1, 2*nPhi-1, 3, 6))
    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    for iTheta in range(2*nTheta-1):
        for iPhi in range(2*nPhi-1):
            curDirection = location(iTheta, iPhi)
            xCos = np.dot(curDirection, normals[1])
            yCos = np.dot(curDirection, normals[3])
            zCos = np.dot(curDirection, normals[4])

            receiversTensorAmps[iTheta,iPhi,0,0] = 6*xCos**3 - 3*xCos
            receiversTensorAmps[iTheta,iPhi,1,0] = 6*(xCos**2)*yCos - yCos
            receiversTensorAmps[iTheta,iPhi,2,0] = 6*(xCos**2)*zCos - zCos

            receiversTensorAmps[iTheta,iPhi,0,1] = 6*xCos*(yCos**2) - xCos
            receiversTensorAmps[iTheta,iPhi,1,1] = 6*(yCos**3)      - 3*yCos
            receiversTensorAmps[iTheta,iPhi,2,1] = 6*(yCos**2)*zCos - zCos

            receiversTensorAmps[iTheta,iPhi,0,2] = 6*xCos*(zCos**2)     - xCos
            receiversTensorAmps[iTheta,iPhi,1,2] = 6*(yCos)*(zCos**2)   - yCos
            receiversTensorAmps[iTheta,iPhi,2,2] = 6*(zCos**3)          - 3*zCos

            receiversTensorAmps[iTheta,iPhi,0,3] = 6*(xCos**2)*(yCos)   - yCos
            receiversTensorAmps[iTheta,iPhi,1,3] = 6*(yCos**2)*(xCos)   - xCos
            receiversTensorAmps[iTheta,iPhi,2,3] = 6*xCos*yCos*(zCos)

            receiversTensorAmps[iTheta,iPhi,0,4] = 6*(xCos**2)*(zCos)   - zCos
            receiversTensorAmps[iTheta,iPhi,1,4] = 6*xCos*yCos*(zCos)
            receiversTensorAmps[iTheta,iPhi,2,4] = 6*(zCos**2)*xCos     - xCos

            receiversTensorAmps[iTheta,iPhi,0,5] = 6*xCos*yCos*(zCos)
            receiversTensorAmps[iTheta,iPhi,1,5] = 6*(yCos**2)*(zCos)   - zCos
            receiversTensorAmps[iTheta,iPhi,2,5] = 6*(zCos**2)*yCos     - yCos

    out = np.zeros((2*nTheta-1, 2*nPhi-1))
    theta = np.zeros((2*nTheta-1, 2*nPhi-1))
    phi = np.zeros((2*nTheta-1, 2*nPhi-1))
    for iTheta in range(2*nTheta-1):
        for iPhi in range(2*nPhi-1):
            out[iTheta, iPhi] = receiversTensorAmps[iTheta,iPhi, iChannel, iTensor]
            theta[iTheta, iPhi] = thetaSpace[iTheta]
            phi[iTheta, iPhi] = phiSpace[iPhi]

    plt.imshow(out, cmap='nipy_spectral')
    plt.colorbar()
    plt.show()

#testTensorsAmplitudeDist(0, nRays, nRays)
#testTensorsAmplitudeDist0(0, 0, 0, nRays, nRays)
