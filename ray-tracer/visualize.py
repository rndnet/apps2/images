import numpy as np
from matplotlib import cm
from matplotlib import colors
import matplotlib.pyplot as plt

#plt.colorbar(cm.ScalarMappable(norm=colors.Normalize(vmin=3,vmax=100), cmap='hot'))
#plt.show()

#Draw rays in defferent colors
def plotPath(nCube, rCube, mostNegativeCube, path, nPath, c='xz'):
    if c == 'xz' or 'xz' in c:
        fig, ax = plt.subplots()
        for i in range(nCube[0]+1):
            ax.plot(np.zeros(nCube[2]+1)+i*rCube+mostNegativeCube[0], np.arange(nCube[2]+1)*rCube+mostNegativeCube[2],"g")
        for i in range(nCube[2]+1):
            ax.plot(np.arange(nCube[0]+1) *rCube+mostNegativeCube[0], np.zeros(nCube[0]+1)+i*rCube+mostNegativeCube[2],"g")
        for iTrace in range(path.shape[0]):
            ax.plot(path[iTrace,:nPath[iTrace],0],path[iTrace,:nPath[iTrace],2])
        ax.set_xlabel("X")
        ax.set_ylabel("Z")
        plt.show()
    if c == 'yz' or 'yz' in c:
        fig, ax = plt.subplots()
        for i in range(nCube[1]+1):
            ax.plot(np.zeros(nCube[2]+1)+i*rCube+mostNegativeCube[1], np.arange(nCube[2]+1) *rCube+mostNegativeCube[2],"g")
        for i in range(nCube[2]+1):
            ax.plot(np.arange(nCube[1]++1) *rCube+mostNegativeCube[1], np.zeros(nCube[1]++1)+i*rCube+mostNegativeCube[2],"g")
        for iTrace in range(path.shape[0]):
            ax.plot(path[iTrace,:nPath[iTrace],1],path[iTrace,:nPath[iTrace],2])
        ax.set_xlabel("Y")
        ax.set_ylabel("Z")
        plt.show()
    if c == 'xy' or 'xy' in c:
        fig, ax = plt.subplots()
        for i in range(nCube[0]+1):
            ax.plot(np.zeros(nCube[1]++1)+i*rCube+mostNegativeCube[0], np.arange(nCube[1]+1) *rCube+mostNegativeCube[1],"g")
        for i in range(nCube[1]+1):
            ax.plot(np.arange(nCube[0]++1) *rCube+mostNegativeCube[0], np.zeros(nCube[0]+1)+i*rCube+mostNegativeCube[1],"g")
        for iTrace in range(path.shape[0]):
            ax.plot(path[iTrace,:nPath[iTrace],0],path[iTrace,:nPath[iTrace],1])
        ax.set_xlabel("X")
        ax.set_ylabel("Y")
        plt.show()

#plot rays with given parameters amps or times or medium
def plotMediumPath(nCube, rCube, vp, density, mostNegativeCube, path, nPath, 
    axes='xz', cmapRay='hot', cmapMedium='magma', cmapAmp='jet', cmapTime='jet', 
    amps=[], times=[]):

    cmMedium = cm.get_cmap(cmapMedium, 1000)
    cmAmp    = cm.get_cmap(cmapAmp, 1000)
    cmTime    = cm.get_cmap(cmapTime, 1000)

    refractionIndex = vp*density
    maxRI = np.amax(refractionIndex)
    minRI = np.amin(refractionIndex)
    normRefraction = lambda x: (x - minRI)/(maxRI - minRI)
    normAmps       = lambda x: (x - np.amin(amps))/((np.amax(amps) - np.amin(amps)))
    normTimes      = lambda x: (x - np.amin(times)) / (np.amax(times) - np.amin(times))

    #print(np.amin(amps))
    clampUpper = lambda x: x if x < nCube else nCube-1
    clampLower = lambda x: x if x >= 0 else 0
    clamp = lambda x: clampUpper(clampLower(x))
    getRefractionIndex = lambda i,j,k: refractionIndex[clamp(i), clamp(j), clamp(k)]
    ds = rCube/13
    if axes == 'xz' or 'xz' in axes:
        #fig, (ax, axc) = plt.subplots(nrows=2,ncols=1)
        fig, ax = plt.subplots(nrows=1,ncols=1)
        for i in range(nCube+1):
            ax.plot(np.zeros(nCube+1)+i*rCube+mostNegativeCube[0]-ds, np.arange(nCube+1) *rCube+mostNegativeCube[2],
                    c=cmMedium(normRefraction(getRefractionIndex(i-1,0,0))))
            ax.plot(np.zeros(nCube+1)+i*rCube+mostNegativeCube[0]+ds, np.arange(nCube+1) *rCube+mostNegativeCube[2],
                    c=cmMedium(normRefraction(getRefractionIndex(i,0,0))))

            ax.plot(np.arange(nCube+1) *rCube+mostNegativeCube[0], np.zeros(nCube+1)+i*rCube+mostNegativeCube[2]-ds,
                    c=cmMedium(normRefraction(getRefractionIndex(0,0,i-1))))
            ax.plot(np.arange(nCube+1) *rCube+mostNegativeCube[0], np.zeros(nCube+1)+i*rCube+mostNegativeCube[2]+ds,
                    c=cmMedium(normRefraction(getRefractionIndex(0,0,i))))

        if len(amps) != 0:
            for iTrace in range(path.shape[0]):
                for iPath in range(nPath[iTrace]-1):
                    ax.plot(path[iTrace,iPath:iPath+2,0], 
                            path[iTrace,iPath:iPath+2,2],
                            c=cmAmp(normAmps(amps[iTrace,iPath])))
            plt.colorbar(cm.ScalarMappable(norm=colors.Normalize(vmin=np.amin(amps),vmax=np.amax(amps)), cmap=cmAmp))
        elif len(times) != 0:
            for iTrace in range(path.shape[0]):
                ax.plot(path[iTrace,:nPath[iTrace],0],
                        path[iTrace,:nPath[iTrace],2],
                        c=cmTime(normTimes(times[iTrace])))
            plt.colorbar(cm.ScalarMappable(norm=colors.Normalize(vmin=np.amin(times),vmax=np.amax(times)), cmap=cmTime))
        else:
            for iTrace in range(path.shape[0]):
                ax.plot(path[iTrace,:nPath[iTrace],0],path[iTrace,:nPath[iTrace],2])

        ax.set_xlabel("X")
        ax.set_ylabel("Z")
        plt.show()

    if axes == 'yz' or 'yz' in axes:
        fig, ax = plt.subplots()
        for i in range(nCube+1):
            ax.plot(np.zeros(nCube+1)+i*rCube+mostNegativeCube[1]-ds, np.arange(nCube+1) *rCube+mostNegativeCube[2],
                    c=cmMedium(normRefraction(getRefractionIndex(0,i-1,0))))
            ax.plot(np.zeros(nCube+1)+i*rCube+mostNegativeCube[1]+ds, np.arange(nCube+1) *rCube+mostNegativeCube[2],
                    c=cmMedium(normRefraction(getRefractionIndex(0,i,0))))

            ax.plot(np.arange(nCube+1) *rCube+mostNegativeCube[1], np.zeros(nCube+1)+i*rCube+mostNegativeCube[2]-ds,
                    c=cmMedium(normRefraction(getRefractionIndex(0,0,i-1))))
            ax.plot(np.arange(nCube+1) *rCube+mostNegativeCube[1], np.zeros(nCube+1)+i*rCube+mostNegativeCube[2]+ds,
                    c=cmMedium(normRefraction(getRefractionIndex(0,0,i))))

        for iTrace in range(path.shape[0]):
            ax.plot(path[iTrace,:nPath[iTrace],1],path[iTrace,:nPath[iTrace],2])
        ax.set_xlabel("Y")
        ax.set_ylabel("Z")
        plt.show()
    if axes == 'xy' or 'xy' in axes:
        fig, ax = plt.subplots()
        for i in range(nCube+1):
            ax.plot(np.zeros(nCube+1)+i*rCube+mostNegativeCube[0]-ds, np.arange(nCube+1) *rCube+mostNegativeCube[1],
                    c=cmMedium(normRefraction(getRefractionIndex(i-1,0,0))))
            ax.plot(np.zeros(nCube+1)+i*rCube+mostNegativeCube[0]+ds, np.arange(nCube+1) *rCube+mostNegativeCube[1],
                    c=cmMedium(normRefraction(getRefractionIndex(i,0,0))))

            ax.plot(np.arange(nCube+1) *rCube+mostNegativeCube[0], np.zeros(nCube+1)+i*rCube+mostNegativeCube[1]-ds,
                    c=cmMedium(normRefraction(getRefractionIndex(0,i-1,0))))
            ax.plot(np.arange(nCube+1) *rCube+mostNegativeCube[0], np.zeros(nCube+1)+i*rCube+mostNegativeCube[1]+ds,
                    c=cmMedium(normRefraction(getRefractionIndex(0,i,0))))
        for iTrace in range(path.shape[0]):
            ax.plot(path[iTrace,:nPath[iTrace],0],path[iTrace,:nPath[iTrace],1])
        ax.set_xlabel("X")
        ax.set_ylabel("Y")
        plt.show()

