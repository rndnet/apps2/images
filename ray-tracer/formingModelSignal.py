import numpy as np
import matplotlib.pyplot as plt

receiversCoordinates = np.array([[0,0],[0,0],[0,0]])
receiversCoordinates = np.array([[0,0]])
fs = 200
pWaveFrequencyWide  = 3.33
pWaveFrequency      = 7
timeLength          = 5
receiversTimes      = np.array([timeLength//2])#,7,5])
receiversAmps       = np.array([3])#,7,5])
receiversTensorAmps = np.ones((7,7,7))#,7,5])
wavelet = 'gabor'
wavelet = 'gauss'

fs = int(fs)

gaussWavelet = lambda mean, var: lambda x:\
          -((x-mean)*np.e**(-(x-mean)**2/(2*var**2)))/(np.sqrt(2*np.pi)*var**3)
gaussWavelet2 = lambda mean, var: lambda x:\
      (np.e**((-(x-mean)**2)/(2*var**2))*(mean**2 - var**2 + x**2 - 2*mean*x))/(np.sqrt(2*np.pi)*var**5)
#https://en.wikipedia.org/wiki/Gabor_wavelet
gaborWavelet     = lambda x0, a, k0: lambda x: ((np.e**(-(x-x0)**2 / a**2) * np.e**(-(1j)*k0*(x-x0))))
absGaborWavelet  = lambda x0, a, k0: lambda x: np.abs((np.e**(-(x-x0)**2 / a**2) * np.e**(-(1j)*k0*(x-x0))))
realGaborWavelet = lambda x0, a, k0: lambda x: np.real((np.e**(-(x-x0)**2 / a**2) * np.e**(-(1j)*k0*(x-x0))))
imagGaborWavelet = lambda x0, a, k0: lambda x: np.imag((np.e**(-(x-x0)**2 / a**2) * np.e**(-(1j)*k0*(x-x0))))
fftGaborWavelet  = lambda x0, a, k0: lambda x: np.abs((np.e**(-(x-k0)**2 * a**2) * np.e**(-(1j)*x0*(x-k0))))

#fd=fs
#fig, axes = plt.subplots(2,1)
#for i in range(1):
#    x0, a, k0 = 2.5, 0.01, (50+(0.2))*2*np.pi
#    w    = realGaborWavelet(x0,a,k0)((np.arange(0,timeLength*fs)/fs))
#    a = np.zeros((8192*4))
#    a[4096*4:4096*4+w.shape[0]] = w
#    axes[0].plot(w)
#    axes[1].plot(np.linspace(0, fd//2, fd*timeLength//2), np.abs(np.fft.fft(w))[1:fd*timeLength//2+1])
#    #axes[0].plot(a)
#    #axes[1].plot(np.linspace(0, fd//2, 8192*4//2), np.abs(np.fft.fft(a))[1:8192*4//2+1])
#    #axes[0,1].plot(real)
#    #axes[1,1].plot(imag)
#plt.show()

def getCentersFrequency(wavelet, length, fs):
    def _getCentersFrequency(sigma):
        signal = wavelet(length,sigma)(np.arange(0,length*fs)/fs)
        fftGt = np.abs(np.fft.fft(signal))[1:length*fs//2+1]
        frequencies = np.arange(length*fs//2)/length 
        if frequencies[np.argmax(fftGt)] < 0:
            plt.plot(frequencies)
            plt.title('Error')
            plt.show()
        return frequencies[np.argmax(fftGt)]
    return _getCentersFrequency

def getGaborK(frequency):
    return (frequency+0.2)*2*np.pi

def getWaveletsCenterByFrequency(wavelet, timeLength, fs, frequency):
    logBase = 3
    frequencyLinspace = np.linspace(0, stop=300**(1/logBase), num=10000)
    frequencyLogspace = frequencyLinspace**logBase
    frequencyLinspace = frequencyLogspace

    frequencyErrors = np.abs(np.vectorize(getCentersFrequency(wavelet, timeLength, fs))(frequencyLinspace) - frequency)
    if np.amin(frequencyErrors) > 0.2:
        raise Exception('Can\'t got right frequency')
    return frequencyLinspace[np.argmin(frequencyErrors)]

def insertModelWave(
    receiversCoordinates
   ,wavelet
   ,fd
   ,pWaveFrequency
   ,pWaveFrequencyWide
   ,timeLength
   ,receiversTimes
   ,receiversTensorAmps
   ,receiversAmps
     ):         

    receiverSignals = np.zeros((receiversCoordinates.shape[0],3,6,fd*timeLength))

    length = receiverSignals.shape[3]

    if   wavelet == 'gauss':
        wavelet = gaussWavelet2
        optWavelet = lambda length, sigma: wavelet(length//2,sigma)
        sigma = getWaveletsCenterByFrequency(optWavelet, timeLength, fs, pWaveFrequency)
        waveletParams = (sigma,)
    elif wavelet == 'gabor':
        wavelet = realGaborWavelet
        optWavelet = lambda length, sigma: wavelet(length//2, 1, sigma)
        waveletParams = (1/pWaveFrequencyWide, getGaborK(pWaveFrequency))
    else:
        raise Exception('Unknown wavelet name')

    for iR in range(receiversCoordinates.shape[0]):
        for iCh in range(3):
            for iTensor in range(6):
                waveComing = int(round(receiversTimes[iR]*fd))

                gwt = wavelet(waveComing/fd,*waveletParams)(np.arange(0,length)/fd)
                receiverSignals[iR, iCh, iTensor, :] \
                      = receiversTensorAmps[iR,iCh,iTensor]*receiversAmps[iR]*gwt

        fig, axes = plt.subplots(2,1)
        axes[0].plot(receiverSignals[iR, iCh, iTensor, :])
        axes[1].plot(np.linspace(0, fd//2, fd*timeLength//2), np.abs(np.fft.fft(receiverSignals[iR, iCh, iTensor, :]))[1:fd*timeLength//2+1])
        plt.show()

    #Bring to correspondence full wave modelling
    receiverSignals[:,2,:,:] *= -1
    return receiverSignals


receiverSignals = insertModelWave(
    receiversCoordinates, wavelet, fs, pWaveFrequency, pWaveFrequencyWide, timeLength, receiversTimes, receiversTensorAmps, receiversAmps)    
#receiverSignals = receiverSignals.transpose(1,0,2,3).reshape(-1,receiverSignals.shape[3])
