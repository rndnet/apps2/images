import numpy as np
import numpy.linalg as npl
import math, random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def fibonacciSphere(samples=1,randomize=True):
    rnd = 1.
    if randomize:
        rnd = random.random() * samples

    points = []
    offset = 2./samples
    increment = math.pi * (3. - math.sqrt(5.));

    for i in range(samples):
        y = ((i * offset) - 1) + (offset / 2);
        r = math.sqrt(1 - pow(y,2))

        phi = ((i + rnd) % samples) * increment

        x = math.cos(phi) * r
        z = math.sin(phi) * r

        points.append([x,y,z])

    return np.array(points)

def rayTracer(
        nCubes           # (xn, yn, zn)
       ,rCube            # length of cube's side
       ,vp               # size(vp)      = (xn, yn, zn)
       ,density          # size(density) = (xn, yn, zn)
       ,mostNegativeCube # the most negative corner of the most negative cube by x,y,z
       ,nTheta           # number of theta steps in quarter
       ,nPhi
       ,maxAngle = np.pi/2
       ,iTrace   = 0
       ,jTrace   = 0
       ):
    print("QUALLLLLLLLLLLLLLLLLLLLL")

    startSphereRadius = rCube/1000

    rays = np.zeros((2*nTheta-1,2*nPhi-1,8)) # ray = (time,x,y,z,dirX,dirY,dirZ,isLost)

    def setLocation(iTheta, iPhi, location):
        rays[iTheta,iPhi,1:4] = location
    location = lambda iTheta, iPhi: rays[iTheta,iPhi,1:4]

    def setDirection(iTheta, iPhi, direction):
        rays[iTheta,iPhi,4:7] = direction
    direction = lambda iTheta, iPhi: rays[iTheta,iPhi,4:7]

    def scaleLocation(s):
        rays[:,:,1:4] *= s
    def setLocToDir():
        rays[:,:,4:7] = rays[:,:,1:4]

    def rayLost(iTheta, iPhi):
        rays[iTheta, iPhi, 7] = 1
    isLost = lambda iTheta, iPhi: rays[iTheta, iPhi, 7] == 1

    #Initialize rays
    thetaSpace = np.linspace(0,maxAngle,nTheta)
    phiSpace   = np.linspace(0,maxAngle,nPhi  )

    setLocation(0, 0, [np.cos(0), np.sin(0) * np.cos(0), np.sin(0) * np.sin(0)])
    for iTheta in range(1,nTheta):
        theta = thetaSpace[iTheta]
        for iPhi in range(nPhi):
            phi   = phiSpace[iPhi]
            r0 = np.sin(theta)
            setLocation(iTheta,iPhi, [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])
            #print("setLocation:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])
            print("theta: ", theta, " phi: ", phi, " loc:", [np.cos(theta), r0 * np.cos(phi), r0 * np.sin(phi)])

    rays[nTheta:2*nTheta-1,:,:] = rays[1:nTheta,:,:]
    rays[nTheta:2*nTheta-1,:,1] *= -1
    rays[:,nPhi:2*nPhi-1,:] = rays[:,:nPhi-1,:]
    rays[:,nPhi:2*nPhi-1,2] *= -1

    setLocToDir()
    scaleLocation(startSphereRadius)

    initialArrangement = np.copy(rays.reshape(-1,8)[:,1:4])

    path  = np.zeros((10000,3))
    path[0,:] = location(iTrace,jTrace)

    print("Traced location: ", location(iTrace,jTrace))
    nPath = 1

    normals = np.array([[-1,0,0], [1,0,0], [0,-1,0], [0,1,0], [0,0,1]])
    idxsD   = np.array([0       , 0      , 1       , 1      , 2      ])

    def closestPlanes(loc):
        dists = loc - mostNegativeCube
        z  = (dists[2] // rCube) + 1
        nx = (dists[0] // rCube)
        x  = (dists[0] // rCube) + 1
        ny = (dists[1] // rCube)
        y  = (dists[1] // rCube) + 1

        #print("loc:", loc)
        #print("dists:", dists)
        #print("z:",z)
        shifts = mostNegativeCube[idxsD]
        return np.array([nx, x, ny, y, z]) * rCube + shifts

    zSurface = mostNegativeCube[2] + nCubes*rCube
    isUnderZSurface = lambda iRay, jRay: location(iRay, jRay)[2] < zSurface
    isInsideXGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[0] >= mostNegativeCube[0] and\
        location(iRay, jRay)[0] <= mostNegativeCube[0] + nCubes*rCube
    isInsideYGrid   = lambda iRay, jRay:\
        location(iRay, jRay)[1] >= mostNegativeCube[1] and\
        location(iRay, jRay)[1] <= mostNegativeCube[1] + nCubes*rCube
    isInsideGrid = lambda iRay, jRay:\
        isUnderZSurface(iRay, jRay) and isInsideXGrid(iRay, jRay) and isInsideYGrid(iRay, jRay)
    for iRay in range(nTheta):
        for jRay in range(nPhi):
            #print("iRay, jRay:", iRay, " ", jRay, " ", location(iRay,jRay), direction(iRay,jRay))
            #while location(iRay,jRay)[2] < zSurface and not isLost(iRay,jRay):
            while isInsideGrid(iRay, jRay) and not isLost(iRay,jRay):

                if np.dot(direction(iRay,jRay), normals[4]) <= 0:
                    #print("RayLost: Full reflection!")
                    rayLost(iRay, jRay)

                planes = closestPlanes(location(iRay,jRay))
                distanceToPlane = 1e100
                iIntersectPlane = 0
                lengthNormalToPlane = 0
                for iNormal in range(normals.shape[0]):
                    if np.dot(direction(iRay,jRay), normals[iNormal]) > 0:
                        curLengthNormalToPlane = np.abs(location(iRay,jRay)[idxsD[iNormal]] - planes[iNormal])
                        #print("0:", location(iRay,jRay)[idxsD[iNormal]], " ", planes[iNormal])
                        cosRayPlane = np.dot(direction(iRay,jRay), normals[iNormal])
                        curDistanceToPlane = curLengthNormalToPlane/cosRayPlane
                        if curDistanceToPlane < distanceToPlane:
                            distanceToPlane = curDistanceToPlane
                            iIntersectPlane = iNormal
                            lengthNormalToPlane = curLengthNormalToPlane
                stepsToIntersect = curLengthNormalToPlane / direction(iRay,jRay)[idxsD[iIntersectPlane]]
                updLocation = location(iRay, jRay) + stepsToIntersect*direction(iRay, jRay)
                setLocation(iRay, jRay, updLocation)

                if iRay == iTrace and jRay == jTrace:
                    path[nPath,:] = updLocation
                    nPath += 1
                #print("updLocation: ", updLocation)
    path = path[:nPath,:]
    return rays, path, initialArrangement

def plotPath(nCube, rCube, mostNegativeCube, path, c='xz'):
    fig, ax = plt.subplots()
    if c == 'xz':
        for i in range(nCube):
            ax.plot(np.zeros(nCube)+i*rCube+mostNegativeCube[0], np.arange(nCube) *rCube+mostNegativeCube[2],"g")
            ax.plot(np.arange(nCube) *rCube+mostNegativeCube[0], np.zeros(nCube)+i*rCube+mostNegativeCube[2],"g")
        ax.plot(path[:,0],path[:,2])
    plt.show()

#spherePoints = fibonacciSphere(1000, False)
#print(spherePoints.shape)
#
#fig = plt.figure()
#ax = fig.add_subplot(111,projection='3d')
#ax.scatter(spherePoints[:,0], spherePoints[:,1], spherePoints[:,2])
#plt.show()

leftDownCube = np.array([-1000,-1000,0])
rays, path, initialArrangement = rayTracer(20,100,0,0,leftDownCube,5,5,np.pi/2, 0, 1)
plotPath(20,100,leftDownCube,path)

#fig = plt.figure()
#ax = fig.add_subplot(111,projection='3d')
#ax.scatter(rays[:,1], rays[:,2], rays[:,3])
#plt.show()

print(rays.shape)
rays = rays.reshape(-1,8)
print(rays.shape)
s = 3
plt.scatter(initialArrangement[:,0], initialArrangement[:,1], s=s)
plt.title("InitialArrangement XY")
plt.show()
plt.scatter(initialArrangement[:,0], initialArrangement[:,2], s=s)
plt.title("InitialArrangement XZ")
plt.show()
plt.scatter(initialArrangement[:,1], initialArrangement[:,2], s=s)
plt.title("InitialArrangement YZ")
plt.show()

#plt.scatter(spherePoints[:,0], spherePoints[:,1], s=1)
#plt.show()
#plt.scatter(spherePoints[:,0], spherePoints[:,2], s=1)
#plt.show()
#plt.scatter(spherePoints[:,1], spherePoints[:,2], s=1)
#plt.show()


 
